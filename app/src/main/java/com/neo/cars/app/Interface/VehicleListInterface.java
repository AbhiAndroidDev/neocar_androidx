package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleDetailsModel;

import java.util.ArrayList;

public interface VehicleListInterface {

    void VehicleList(String strPageLimit, String strTotalRecord, ArrayList<VehicleDetailsModel> arr_vehicleDetailsModel);
}

package com.neo.cars.app.dialog;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.neo.cars.app.R;

import java.util.Calendar;

/**
 * Created by kamail on 5/4/18.
 */

public class YearMonthPickerDialog implements Dialog.OnClickListener {
    private static final int MIN_YEAR = 1970;
    private static int MAX_YEAR = 2099;
    private Calendar calendar;
    private OnDateSetListener mOnDateSetListener;

    private final Context mContext;

    private AlertDialog.Builder mDialogBuilder;

    private AlertDialog mDialog;

    private int mYear;

    public YearMonthPickerDialog(Context context, Calendar calendar, OnDateSetListener onDateSetListener) {
        this(context, onDateSetListener, -1, -1, calendar);
    }

    public YearMonthPickerDialog(Context context, OnDateSetListener onDateSetListener, int theme, int titleTextColor, Calendar calendar) {
        mContext = context;
        mOnDateSetListener = onDateSetListener;
        this.calendar = calendar;

        build();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                if (mOnDateSetListener != null)
                    mOnDateSetListener.onYearMonthSet(mYear);
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                dialog.cancel();
                break;
        }
    }

    private void build() {

        mDialogBuilder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);

        final LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View titleView = layoutInflater.inflate(R.layout.view_dialog_title, null, false);
        final View contentView = layoutInflater.inflate(R.layout.view_month_year_picker, null, false);

        //final NumberPickerWithColor yearPicker = (NumberPickerWithColor) contentView.findViewById(R.id.year_picker);
        final NumberPicker yearPicker = contentView.findViewById(R.id.year_picker);
        yearPicker.setWrapSelectorWheel(false);

        final TextView yearValue = titleView.findViewById(R.id.year_name);

        mDialogBuilder.setCustomTitle(titleView);
        mDialogBuilder.setView(contentView);
        yearPicker.setMinValue(MIN_YEAR);
        Calendar now = Calendar.getInstance();   // Gets the current date and time
        MAX_YEAR = now.get(Calendar.YEAR);;
        yearPicker.setMaxValue(MAX_YEAR);
        setCurrentDate(yearPicker, yearValue);
        setListeners(yearPicker, yearValue);
        mDialogBuilder.setPositiveButton("OK", this);
        mDialogBuilder.setNegativeButton("CANCEL", this);

        mDialog = mDialogBuilder.create();
    }

    private void setCurrentDate(NumberPicker yearPicker, TextView yearValue) {
        mYear = calendar.get(Calendar.YEAR);
        yearValue.setText(Integer.toString(mYear));
        yearPicker.setValue(mYear);
    }


    private void setListeners(final NumberPicker yearPicker, final TextView yearValue) {
        yearPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mYear = newVal;
                yearValue.setText(""+newVal);
            }
        });
    }

    public void show() {
        mDialog.show();
    }

    public interface OnDateSetListener {
        void onYearMonthSet(int year);
    }
}

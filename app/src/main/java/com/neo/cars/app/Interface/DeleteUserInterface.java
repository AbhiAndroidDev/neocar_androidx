package com.neo.cars.app.Interface;

/**
 * Created by parna on 31/8/18.
 */

public interface DeleteUserInterface {

    void responseOnDelete(String msg);
}

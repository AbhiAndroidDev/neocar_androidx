package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.UserVoucher_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.UserVoucherModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 23/5/18.
 */

public class VoucherRedeem_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private ArrayList<UserVoucherModel> arr_UserVoucherModel=new ArrayList<>();
    UserVoucher_Interface uservoucher_Interface;
    //JSONArray details, jsonArrayUserVoucher;
    JSONObject jsonObjDetails ;
    JSONArray jsonArrayUserVoucherRedeem ;

    public void userVoucherRedeem (Activity context ,UserVoucher_Interface mUserVoucher_Interface){

        mcontext = context;
        uservoucher_Interface=mUserVoucher_Interface;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.user_voucher,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",UserLoginDetails.getId());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);



    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            Msg = jobj_main.optJSONObject("user_voucher").optString("message");
            Status= jobj_main.optJSONObject("user_voucher").optString("status");
            jsonObjDetails = jobj_main.optJSONObject("user_voucher").optJSONObject("details");

            jsonArrayUserVoucherRedeem = jsonObjDetails.optJSONArray("user_voucher_redeem");

            if (Status.equals(StaticClass.SuccessResult)){
                arr_UserVoucherModel=new ArrayList<>();
                for (int i=0; i<jsonArrayUserVoucherRedeem.length(); i++){
                    UserVoucherModel uvm=new UserVoucherModel();

                    uvm.setVoucher_id(jsonArrayUserVoucherRedeem.optJSONObject(i).optString("voucher_id"));
                    uvm.setVoucher_code(jsonArrayUserVoucherRedeem.optJSONObject(i).optString("voucher_code"));
                    uvm.setVoucher_desc(jsonArrayUserVoucherRedeem.optJSONObject(i).optString("voucher_desc"));
                    uvm.setDiscount_value(jsonArrayUserVoucherRedeem.optJSONObject(i).optString("discount_value"));
                    uvm.setEnd_date(jsonArrayUserVoucherRedeem.optJSONObject(i).optString("end_date"));

                    arr_UserVoucherModel.add(uvm);
                }

            }else if (Status.equals(StaticClass.ErrorResult)){
                //new CustomToast(mcontext, Msg);
                jsonObjDetails = jobj_main.optJSONObject("user_voucher").optJSONObject("details");
                JSONArray jarrError = jsonObjDetails.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }



        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {
            uservoucher_Interface.OnUserVoucher(arr_UserVoucherModel);
        }

    }


}

package com.neo.cars.app;

import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.fragment.CompanyMyBookingPager;

public class CompanyMyBookingActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;
    private ConnectionDetector cd;
    private Context context;
    private SharedPrefUserDetails sharedPrefUser;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private TabLayout tabs;
    private ViewPager viewpager;
    private SharedPrefUserDetails sharedPref;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();
    boolean upcommingFlag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_my_booking);

        new AnalyticsClass(CompanyMyBookingActivity.this);

        Bundle intentss = getIntent().getExtras();
        if (intentss != null) {
            upcommingFlag = getIntent().getExtras().getBoolean("upcommingFlag");
        }

        Initialize();
        Listener();
    }

    private void Initialize() {
        context = this;
        cd = new ConnectionDetector(this);
        UserLoginDetails=new UserLoginDetailsModel();
        sharedPref = new SharedPrefUserDetails(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        sharedPrefUser = new SharedPrefUserDetails(CompanyMyBookingActivity.this);
        sharedPrefUser.putBottomView(StaticClass.Menu_MyBookings_company);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);

        tv_toolbar_title.setText(getResources().getString(R.string.tvMyVehicleBooking));

        ibNavigateMenu =  findViewById(R.id.ibNavigateMenu);
        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlBackLayout.setVisibility(View.VISIBLE);

        viewpager = findViewById(R.id.viewpager);


        tabs = findViewById(R.id.tabs);
        //Adding the tabs using addTab() method
        tabs.addTab(tabs.newTab().setText("Ongoing"));
        tabs.addTab(tabs.newTab().setText("Upcoming"));
        tabs.addTab(tabs.newTab().setText("Past"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        //Creating our pager adapter
        final CompanyMyBookingPager adapter = new CompanyMyBookingPager(getSupportFragmentManager(), tabs.getTabCount(),upcommingFlag);

        //Adding adapter to pager
        viewpager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabs.setOnTabSelectedListener(CompanyMyBookingActivity.this);

        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        if(upcommingFlag) {
            viewpager.setCurrentItem(1);
        }else{
            viewpager.setCurrentItem(0);
        }
    }


    private void Listener() {
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyMyBookingActivity.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.BottomProfileCompany){
            finish();
        }

        sharedPref.putBottomViewCompany(StaticClass.Menu_MyBookings_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanyMyBookingActivity.this, StaticClass.Menu_MyBookings_company);


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyMyBookingActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

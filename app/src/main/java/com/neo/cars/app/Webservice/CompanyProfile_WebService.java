package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 10/10/18.
 */

public class CompanyProfile_WebService {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private UserDocumentModel userDocumentModel;
    private ArrayList<UserLoginDetailsModel> arraylistUserModel;
    private String st_mobile="",otp="";
    private int transitionflag = StaticClass.transitionflagNext;

    public void companyProfile(Activity context){
        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.view_company_profile						,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", UserLoginDetails.getId());
                // params.put("user_id","75");  // temporary checking for uneditable edit box field alert dialog

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);

    }

    private void showProgressDialog() {

        if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".HomeProfileActivity")) {
            try {
                pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
                pdCusomeDialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void hideProgressDialog() {
        if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".HomeProfileActivity")) {
            try {
                if (pdCusomeDialog.isShowing()) {
                    pdCusomeDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("view_company_profile").optString("user_deleted");
            Msg = jobj_main.optJSONObject("view_company_profile").optString("message");
            Status= jobj_main.optJSONObject("view_company_profile").optString("status");
            JSONObject details=jobj_main.optJSONObject("view_company_profile").optJSONObject("details");

            arraylistUserModel = new ArrayList<UserLoginDetailsModel>();

            if (Status.equals(StaticClass.SuccessResult)) {
                UserLoginDetails.setId(details.optString("id"));
                UserLoginDetails.setEmail(details.optString("email"));
                UserLoginDetails.setCompany_mobile_number(details.optString("mobile_no"));
                UserLoginDetails.setUser_type(details.optString("user_type"));
                UserLoginDetails.setIs_active(details.optString("is_active"));
                UserLoginDetails.setProfile_name(details.optString("profile_name"));
                UserLoginDetails.setProfile_pic(details.optString("profile_pic"));
                UserLoginDetails.setReg_type(details.optString("reg_type"));
                UserLoginDetails.setReferral_code(details.optString("referral_code"));
                UserLoginDetails.setIs_notification_active(details.optString("is_notification_active"));
                UserLoginDetails.setIs_profile_complete(details.optString("is_profile_complete"));
//                UserLoginDetails.setMobile_verified(details.optString("mobile_verified"));
                UserLoginDetails.setEmail_verified(details.optString("email_verified"));
                UserLoginDetails.setProfile_incomplete_message(details.optString("profile_incomplete_message"));
                UserLoginDetails.setCompany_name(details.optString("company_name"));
                UserLoginDetails.setCompany_phone_number(details.optString("phone_number"));
                UserLoginDetails.setCompany_address(details.optString("registered_address"));
                UserLoginDetails.setCompany_gst_number(details.optString("gst_number"));
                UserLoginDetails.setCompany_pan_number(details.optString("pan_number"));
                UserLoginDetails.setPrimary_contact_name(details.optString("primary_contact_name"));
                UserLoginDetails.setPrimary_contact_mobile(details.optString("primary_contact_mobile"));
                UserLoginDetails.setPrimary_mobile_verified(details.optString("primary_mobile_verified"));
                UserLoginDetails.setPrimary_contact_id(details.optString("primary_contact_id"));
                UserLoginDetails.setPrimary_id_back(details.optString("primary_id_back"));
                UserLoginDetails.setPrimary_id_status(details.optString("primary_id_status"));
                UserLoginDetails.setSecondary_contact_name(details.optString("secondary_contact_name"));
                UserLoginDetails.setSecondary_contact_mobile(details.optString("secondary_contact_mobile"));
                UserLoginDetails.setSecondary_mobile_verified(details.optString("secondary_mobile_verified"));
                UserLoginDetails.setSecondary_contact_id(details.optString("secondary_contact_id"));
                UserLoginDetails.setSecondary_id_back(details.optString("secondary_id_back"));
                UserLoginDetails.setSecondary_id_status(details.optString("secondary_id_status"));
                UserLoginDetails.setCompany_pan_card(details.optString("company_pan_card"));
                UserLoginDetails.setBank_account_number(details.optString("bank_account_number"));
                UserLoginDetails.setIfsc_code(details.optString("ifsc_code"));
                UserLoginDetails.setCancel_check(details.optString("cancel_check"));

                arraylistUserModel.add(UserLoginDetails);

                JSONArray jsonArrayDoc = details.optJSONArray("document");
                Log.d("d", "JSONArray document:"+jsonArrayDoc);

            }
            else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("view_company_profile").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);

            }
        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

            new CustomToast(mcontext, Msg);

        }
        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                ((Profile_Interface)mcontext).MyProfile(UserLoginDetails);
            }
        }
    }
}

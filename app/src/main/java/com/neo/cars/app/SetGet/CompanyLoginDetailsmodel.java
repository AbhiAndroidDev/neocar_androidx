package com.neo.cars.app.SetGet;

/**
 * Created by parna on 5/10/18.
 */

public class CompanyLoginDetailsmodel {

    String id;
    String email;
    String mobile_no;
    String user_type;
    String is_active;
    String profile_name;
    String profile_pic;
    String reg_type;
    String referral_code;
    String is_notification_active;
    String is_profile_complete;
    String profile_incomplete_message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getReg_type() {
        return reg_type;
    }

    public void setReg_type(String reg_type) {
        this.reg_type = reg_type;
    }

    public String getReferral_code() {
        return referral_code;
    }

    public void setReferral_code(String referral_code) {
        this.referral_code = referral_code;
    }

    public String getIs_notification_active() {
        return is_notification_active;
    }

    public void setIs_notification_active(String is_notification_active) {
        this.is_notification_active = is_notification_active;
    }

    public String getIs_profile_complete() {
        return is_profile_complete;
    }

    public void setIs_profile_complete(String is_profile_complete) {
        this.is_profile_complete = is_profile_complete;
    }

    public String getProfile_incomplete_message() {
        return profile_incomplete_message;
    }

    public void setProfile_incomplete_message(String profile_incomplete_message) {
        this.profile_incomplete_message = profile_incomplete_message;
    }
}

package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleTypeModel;

/**
 * Created by parna on 18/5/18.
 */

public interface BeforeProcessBooking_Interface {

    public void beforeProcessBooking(VehicleTypeModel vehicleTypeModel);
}

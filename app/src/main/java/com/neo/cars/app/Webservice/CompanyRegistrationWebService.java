package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.MobileOtpVerificationActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.VolleyParser.CustomMultiPartRequest;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 5/10/18.
 */

public class CompanyRegistrationWebService {

    private Activity mcontext;
    private String Status = "0", Msg = "", unique_id="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private JSONObject details;

    public void Registration(Activity context, String strNameOfCompany, String strRegisteredAddress, String strRegEmailCompany, String strRegPhoneCompany, String strRegMobileCompany, String strCompanyGSTNumber,
                             String strCompanyPANNumber, String strPrimaryContactPName, String strprimayPhoneCompany, String strSecondaryContactPName,
                             String strSecondaryPhoneCompany, String strbankAccountNumber, String strIfscCode, File profile_pic, File primary_contact_id,
                             File primary_contact_id_back, File secondary_contact_id, File secondary_contact_id_back, File company_pan_card, File cancel_check){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        unique_id = Settings.Secure.getString(mcontext.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("d", "Device id::"+unique_id);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        MultipartEntity entity = new MultipartEntity();

        try{
            //Image part here in file form
            if (profile_pic != null) {
                entity.addPart("profile_pic", new FileBody(profile_pic));
            }
            if (primary_contact_id != null) {
                entity.addPart("primary_contact_id", new FileBody(primary_contact_id));
            }

            if(primary_contact_id_back != null) {
                entity.addPart("primary_id_back", new FileBody(primary_contact_id_back));
            }
            if (secondary_contact_id != null) {
                entity.addPart("secondary_contact_id", new FileBody(secondary_contact_id));
            }

            if (secondary_contact_id_back != null){
                Log.d("d", "fileSecondaryContactId image:::"+secondary_contact_id_back);
                entity.addPart("secondary_id_back", new FileBody(secondary_contact_id_back));
            }

            if (company_pan_card != null) {
                entity.addPart("company_pan_card", new FileBody(company_pan_card));
            }
            if (cancel_check != null) {
                entity.addPart("cancel_check", new FileBody(cancel_check));
            }

            entity.addPart("company_name", new StringBody(strNameOfCompany));
            entity.addPart("registered_address", new StringBody(strRegisteredAddress));
            entity.addPart("email", new StringBody(strRegEmailCompany));
          //  entity.addPart("password", new StringBody(strRegPassCompany));
           // entity.addPart("password_confirmation", new StringBody(strRegConfPassCompany));
            entity.addPart("device_type", new StringBody("A"));
            entity.addPart("device_id", new StringBody(unique_id));
            entity.addPart("phone_number", new StringBody(strRegPhoneCompany));
            entity.addPart("mobile_no", new StringBody(strRegMobileCompany));
            entity.addPart("gst_number", new StringBody(strCompanyGSTNumber));
            entity.addPart("pan_number", new StringBody(strCompanyPANNumber));
            entity.addPart("primary_contact_name", new StringBody(strPrimaryContactPName));
            entity.addPart("primary_contact_mobile", new StringBody(strprimayPhoneCompany));
            entity.addPart("secondary_contact_name", new StringBody(strSecondaryContactPName));
            entity.addPart("secondary_contact_mobile", new StringBody(strSecondaryPhoneCompany));
            entity.addPart("bank_account_number", new StringBody(strbankAccountNumber));
            entity.addPart("ifsc_code", new StringBody(strIfscCode));


        }catch (Exception e){
            e.printStackTrace();
        }

        RequestQueue rq = Volley.newRequestQueue(mcontext);
        CustomMultiPartRequest customMultiPartRequest = new CustomMultiPartRequest(Urlstring.companyregistration,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        Log.d("Error.Response", error.toString());
                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                Log.d("Response", response);
                Apiparsedata(response);
            }
        }, entity) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("xsrf-token",sharedPref.getKEY_Access_Token());
                System.out.println("params******getHeaders***" + params);

                return params;
            }
        };
        customMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        customMultiPartRequest.setShouldCache(false);
        // add the request object to the queue to be executed
        rq.add(customMultiPartRequest);

    }


    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);


            Msg = jobj_main.optJSONObject("company_registration").optString("message");
            Status= jobj_main.optJSONObject("company_registration").optString("status");

            details=jobj_main.optJSONObject("company_registration").optJSONObject("details");

            if (Status.equals(StaticClass.SuccessResult)) {

                UserLoginDetails.setId(details.optString("id"));
                sharedPref.setUserId(details.optString("id"));
                UserLoginDetails.setEmail(details.optString("email"));
                UserLoginDetails.setMobile(details.optString("mobile_no"));
                UserLoginDetails.setUser_type(details.optString("user_type"));
                sharedPref.setUserType(details.optString("user_type"));
                UserLoginDetails.setIs_active(details.optString("is_active"));
                UserLoginDetails.setProfile_name(details.optString("profile_name"));
                UserLoginDetails.setProfile_pic(details.optString("profile_pic"));
                UserLoginDetails.setReg_type(details.optString("reg_type"));
                UserLoginDetails.setReferral_code(details.optString("referral_code"));
                UserLoginDetails.setIs_notification_active(details.optString("is_notification_active"));
                UserLoginDetails.setIs_profile_complete(details.optString("is_profile_complete"));
                UserLoginDetails.setProfile_incomplete_message(details.optString("profile_incomplete_message"));

                String strParentDetails = gson.toJson(UserLoginDetails);
                sharedPref.putObjectToPreferenceUserDetails(strParentDetails);
                sharedPref.setUserloginStatus();

            }
            else if (Status.equals(StaticClass.ErrorResult)){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("company_registration").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    new CustomToast(mcontext, Msg);
                }


            }

        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {

//            final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
//                    Msg ,
//                    mcontext.getResources().getString(R.string.btn_ok),
//                    "");
//            alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    alertDialogYESNO.dismiss();
//
                    mcontext.startActivity(new Intent(mcontext, MobileOtpVerificationActivity.class));
                    mcontext.finish();
//
//                }
//            });
//
//            alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialogYESNO.dismiss();
//                }
//            });
//
//            alertDialogYESNO.show();

        } else {
        }
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

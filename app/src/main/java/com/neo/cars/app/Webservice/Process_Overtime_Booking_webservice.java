package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.ThankYouActivity;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomDialogText;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 3/10/18.
 */

public class Process_Overtime_Booking_webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="", vehicle_city_name = "";
    private SharedPrefUserDetails sharedPref;
    private CustomDialogText pdCusomeDialog;
    private Gson gson;
    private VehicleTypeModel vehicleTypeModel;
    private UserLoginDetailsModel UserLoginDetails;
    private JSONObject details;
    private JSONObject jsonObjDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void Process_Overtime_Booking(Activity context, final String booking_id, final String duration, final String transaction_id,
                                               final String transaction_info, final String night_charge, final String net_payable_amount, final String expected_route, final String reason_for_extension,
                                               final String expected_releasing_time, final String addl_info, final String no_of_baggage, final String addl_passenger, final String _vehicle_city_name) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        this.vehicle_city_name = _vehicle_city_name;


        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        vehicleTypeModel = new VehicleTypeModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();


        StringRequest stringProcessBookingRequest = new StringRequest(Request.Method.POST, Urlstring.process_overtime_booking,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response Booking hours", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("booking_id", booking_id);
                params.put("user_id", UserLoginDetails.getId());
                params.put("duration", duration);
                params.put("transaction_id", transaction_id);
                params.put("transaction_info", transaction_info);
                params.put("night_charge", night_charge);
                params.put("net_payable_amount", net_payable_amount);

                if(expected_route != null){
                    params.put("expected_route", expected_route);
                }

                if(reason_for_extension != null){
                    params.put("reason_for_extension", reason_for_extension);
                }

                if(expected_releasing_time != null){
                    params.put("expected_releasing_time", expected_releasing_time);
                }

                if(addl_info != null){
                    params.put("addl_info", addl_info);
                }

                if(no_of_baggage != null){
                    params.put("no_of_baggage", no_of_baggage);
                }

                if(addl_passenger != null){
                    params.put("addl_passenger", addl_passenger);
                }

                new PrintClass("process booking params******getParams***"+params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        stringProcessBookingRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(stringProcessBookingRequest);
    }


    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        Bundle bnd=new Bundle();
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("process_overtime_booking").optString("user_deleted");
//            Msg = jobj_main.optJSONObject("process_overtime_booking").optString("message");
            Status = jobj_main.optJSONObject("process_overtime_booking").optString("status");

            jsonObjDetails = jobj_main.optJSONObject("process_overtime_booking").optJSONObject("details");
            if(Status.equals(StaticClass.SuccessResult)){

                bnd.putString("booking_date",jsonObjDetails.optString("booking_date"));
                bnd.putString("start_time",jsonObjDetails.optString("start_time"));
                bnd.putString("end_time",jsonObjDetails.optString("end_time"));
                bnd.putString("duration",jsonObjDetails.optString("duration"));
                bnd.putString("pickup_location",jsonObjDetails.optString("pickup_location"));
                bnd.putString("net_payable_amount",jsonObjDetails.optString("net_payable_amount"));
                bnd.putString("vehicle_city_name", vehicle_city_name);
//                bnd.putSerializable("vehicle_city_name", jsonObjDetails.optString("vehicle_city_name"));



            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("process_overtime_booking").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }

        }catch (Exception e){
            e.printStackTrace();
        }



        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else{
            if (Status.equals(StaticClass.SuccessResult)) {
                Intent thankyouIntent = new Intent(mcontext, ThankYouActivity.class);
                thankyouIntent.putExtras(bnd);
                mcontext.startActivity(thankyouIntent);
                mcontext.finish();
            } else {
                new CustomToast(mcontext, Msg);
            }
        }
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialogText(mcontext,mcontext.getResources().getString(R.string.BookingWaitMsg));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

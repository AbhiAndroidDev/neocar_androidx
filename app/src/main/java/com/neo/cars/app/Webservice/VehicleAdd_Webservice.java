package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.VehicleSave_interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.VolleyParser.CustomMultiPartRequest;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 28/3/18.
 */

public class VehicleAdd_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private VehicleTypeModel vehicleTypeModel;
    private JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private String userType = "";


    public void VehicleAdd(Activity context, String strVehicleTypeId, String strVehicleBrandId, String strVehicleModelId,
                           String strAc, String strYrOfManufacture, String strTotalKm, String strStateId, String strCityId, String strMaxPassenger, String strMaxLuggage,
                           String strHourlyRate, String strOvertimeAvailableCheck, String strHourlyOvertimeRate,
                           String strSpecialInfo, String strLicensePlateNo, File fileLicensePlateImage,
                           File fileTaxTokenImage, File filePcuPaperImage, String strAddress, String strOtherLoc, File fileGalleryOne,
                           File fileGalleryTwo, File fileGalleryThree,
                           File fileRegionalTransportPermit, File fileRegistrationCertificateFront, File fileRegistrationCertificateBack,  File fileFitnessCertificate, File fileInsuranceCertificate,
                           String strSetGallerySelected, String userType,
                           String toll_parking_charge, String strNightChargesCheck, String strNightChargesRate){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);


        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        vehicleTypeModel = new VehicleTypeModel();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        this.userType = userType;

        showProgressDialog();
        MultipartEntity entity = new MultipartEntity();
        try{

            entity.addPart("user_id", new StringBody(UserLoginDetails.getId()));

            entity.addPart("vehicle_type_id", new StringBody(strVehicleTypeId));
            Log.d("d", "vehicle type id: "+strVehicleTypeId);

            entity.addPart("brand", new StringBody(strVehicleBrandId));
            Log.d("d", "brand id: "+strVehicleBrandId);

            entity.addPart("model", new StringBody(strVehicleModelId));
            Log.d("d", "model id: "+strVehicleModelId);

            entity.addPart("ac_available", new StringBody(strAc)); // ac is "N" as there is no such field yet.

            entity.addPart("year_of_manufacture", new StringBody(strYrOfManufacture));
            Log.d("d", "yr of manufacture: "+strYrOfManufacture);

            entity.addPart("total_no_km", new StringBody(strTotalKm));
            Log.d("d", "total km: "+strTotalKm);

            entity.addPart("state", new StringBody(strStateId));
            Log.d("d", "state id: "+strStateId);

            entity.addPart("city", new StringBody(strCityId));
            Log.d("d", "city id: "+strCityId);

            entity.addPart("max_passenger", new StringBody(strMaxPassenger));
            Log.d("d", "max passenger: "+strMaxPassenger);

            entity.addPart("max_luggage", new StringBody(strMaxLuggage));
            Log.d("d", "max luggage: "+strMaxLuggage);

            entity.addPart("hourly_rate", new StringBody(strHourlyRate));
            Log.d("d", "hourly rate: "+strHourlyRate);

            entity.addPart("overtime_available", new StringBody(strOvertimeAvailableCheck));
            Log.d("d", "overtime_available: "+strOvertimeAvailableCheck);

            if("Y".equals(strOvertimeAvailableCheck)){
                entity.addPart("hourly_overtime_rate", new StringBody(strHourlyOvertimeRate));
                Log.d("d", "hourly overtime rate: "+strHourlyOvertimeRate);
            }

            if(strSpecialInfo != null){
                entity.addPart("special_info", new StringBody(strSpecialInfo));
                Log.d("d", "special info : "+strSpecialInfo);
            }

            entity.addPart("license_plate_no", new StringBody(strLicensePlateNo));
            Log.d("d", "license_plate_no : "+strLicensePlateNo);

            if(fileLicensePlateImage != null){
                entity.addPart("license_plate_image", new FileBody(fileLicensePlateImage));
                Log.d("d", "license_plate_image : "+fileLicensePlateImage);
            }

            if(fileTaxTokenImage != null){
                entity.addPart("tax_token_image", new FileBody(fileTaxTokenImage));
                Log.d("d", "tax_token_image : "+fileTaxTokenImage);
            }

            if(filePcuPaperImage != null){
                entity.addPart("pcu_paper_image", new FileBody(filePcuPaperImage));
                Log.d("d", "pcu_paper_image : "+filePcuPaperImage);
            }

            if(fileRegionalTransportPermit != null){
                entity.addPart("valid_permit", new FileBody(fileRegionalTransportPermit));
                Log.d("d", "valid_permit : "+fileRegionalTransportPermit);
            }


            if(fileRegistrationCertificateFront != null){
                entity.addPart("registration_certificate", new FileBody(fileRegistrationCertificateFront));
                Log.d("d", "registration_certificate : "+fileRegistrationCertificateFront);
            }

            if(fileRegistrationCertificateBack != null){
                entity.addPart("registration_certificate_back", new FileBody(fileRegistrationCertificateBack));
                Log.d("d", "registration_certificate : "+fileRegistrationCertificateBack);
            }

            if(fileFitnessCertificate != null){
                entity.addPart("fitness_certificate", new FileBody(fileFitnessCertificate));
                Log.d("d", "fitness_certificate : "+fileFitnessCertificate);
            }


            if(fileInsuranceCertificate != null){
                entity.addPart("insurance_certificate", new FileBody(fileInsuranceCertificate));
                Log.d("d", "insurance_certificate : "+fileInsuranceCertificate);
            }



            //PICK UP LOCATION TO BE ADDED
            if (strAddress!= null){
                entity.addPart("pickup_location", new StringBody(strAddress));
                Log.d("d", "pickup_location: "+strAddress);
            }

            entity.addPart("other_pickup_location", new StringBody(strOtherLoc));
            Log.d("d", "other_pickup_location : "+strOtherLoc);

            if(fileGalleryOne != null){
                entity.addPart("gallery_image_1", new FileBody(fileGalleryOne));
                Log.d("d", "gallery_image_1 : " + fileGalleryOne);
            }

            if(fileGalleryTwo != null){
                entity.addPart("gallery_image_2", new FileBody(fileGalleryTwo));
                Log.d("d", "gallery_image_2 : " + fileGalleryTwo);
            }

            if(fileGalleryThree != null){
                entity.addPart("gallery_image_3", new FileBody(fileGalleryThree));
                Log.d("d", "gallery_image_3 : " + fileGalleryThree);
            }

            entity.addPart("default_image_position", new StringBody(strSetGallerySelected));
            Log.d("d", "default_image_position : " + strSetGallerySelected);


            entity.addPart("toll_parking_charge", new StringBody(toll_parking_charge));
            Log.d("d", "toll_parking_charge : " + toll_parking_charge);

            entity.addPart("night_charge_available", new StringBody(strNightChargesCheck));
            Log.d("d", "night_charge_available : " + strNightChargesCheck);

            if (!TextUtils.isEmpty(strNightChargesRate)) {

                entity.addPart("night_rate", new StringBody(strNightChargesRate));
                Log.d("d", "night_rate : " + strNightChargesRate);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        RequestQueue rq = Volley.newRequestQueue(mcontext);
        CustomMultiPartRequest customMultiPartRequest = new CustomMultiPartRequest(Urlstring.vehicle_add,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        Log.d("Error.Response", error.toString());

                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                System.out.println("**response Vehicle Add**"+response);
                Apiparsedata(response);

            }
        }, entity){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("xsrf-token",sharedPref.getKEY_Access_Token());
                System.out.println("params******getHeaders***" + params);

                return params;
            }
        };

        customMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        customMultiPartRequest.setShouldCache(false);
        // add the request object to the queue to be executed
        rq.add(customMultiPartRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_add").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_add").optString("message");
            Status= jobj_main.optJSONObject("vehicle_add").optString("status");
            details = jobj_main.optJSONObject("vehicle_add").optJSONObject("details");
            vehicleTypeModel.setVehicle_id(details.optString("id"));

        }catch (Exception e){
            e.printStackTrace();
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {

            if (Status.equals(StaticClass.SuccessResult)) {

                String strVehicleId = details.optString("id");
                ((VehicleSave_interface)mcontext).vehicleSaveOnClick(Status, Msg, strVehicleId, userType);

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                        Msg ,
                        mcontext.getResources().getString(R.string.btn_ok),
                        "");
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialogYESNO.dismiss();
                        mcontext.finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();


            }else if (Status.equals(StaticClass.ErrorResult)){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("vehicle_add").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    try {
                        Msg = jarrError.getString(0);
                        new CustomToast(mcontext, Msg);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.ExploreVehicleList_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.ExploreModel;
import com.neo.cars.app.SetGet.ExploreVehicleListModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 10/4/18.
 */

public class ExploreVehicleList_Webservice {


    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="", strTotalRecord="", strPageLimit="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private ArrayList<ExploreModel> arr_exploreModel ;
    private ArrayList<ExploreVehicleListModel> arr_exploreVehicleListModel ;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void exploreVehicleList(Activity context, final String strMinPrice, final String strMaxPrice, final String strStartDate, final String strEndDate,
                                   final String strDuration, final String strVehicleTypeId, final String strVehicleBrandId, final String strNoOfPassenger,
                                   final String strNoOfBaggage, final String strNeedOvertime, final String strNeedAc, final String strCityId, final String strStateId,
                                   final String strPickUpTime, final String strModelId, final int pageCount){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest exploreVehicleListREquest = new StringRequest(Request.Method.POST, Urlstring.vehicle_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response***", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String userId = UserLoginDetails.getId();

                if (userId != null) {
                    params.put("user_id", UserLoginDetails.getId());
                }

                if(strMinPrice != null){
                    params.put("min_price", strMinPrice);
                }else {
                    params.put("min_price", "");
                }

                if (strMaxPrice != null){
                    params.put("max_price", strMaxPrice);
                }else {
                    params.put("max_price", "");
                }

                if(strStartDate != null){
                    params.put("start_date", strStartDate);
                }
                else {
                    params.put("start_date", "");
                }

                if(strEndDate != null){
                    params.put("end_date", strEndDate);
                }else{
                    params.put("end_date", "");
                }

                if(strDuration != null){
                    params.put("duration", strDuration);
                }else{
                    params.put("duration", "");
                }

                if(strVehicleTypeId != null){
                    params.put("vehicle_type_id", strVehicleTypeId);
                }else{
                    params.put("vehicle_type_id", "");
                }

                if(strVehicleBrandId != null){
                    params.put("make_id", strVehicleBrandId);
                }else{
                    params.put("make_id", "");
                }

                if(!strNoOfPassenger .equals("0")){
                    params.put("no_of_passenger", strNoOfPassenger);
                }else{
                    params.put("no_of_passenger", "");
                }

                if(!strNoOfBaggage.equals("0")){
                    params.put("no_of_baggage", strNoOfBaggage);
                }else{
                    params.put("no_of_baggage", "");
                }

                if(strNeedOvertime != null){
                    params.put("need_overtime", strNeedOvertime);
                }else{
                    params.put("need_overtime", "");
                }

                if(strNeedAc != null){
                    params.put("need_ac", strNeedAc);
                }else{
                    params.put("need_ac", "");
                }

                if(strCityId != null){
                    params.put("city_id", strCityId);
                }else{
                    params.put("city_id", "");
                }

                if(strStateId != null){
                    params.put("state_id", strStateId);
                }else{
                    params.put("state_id", "");
                }

                if(strPickUpTime != null){
                    params.put("pickup_time", strPickUpTime);
                }else{
                    params.put("pickup_time", "");
                }

                if(strModelId != null){
                    params.put("model_id", strModelId);
                }else{
                    params.put("model_id", "");
                }

                if(strCityId == null || strCityId.equalsIgnoreCase("")){
                    params.put("page_no", Integer.toString(pageCount));
                }else{
                    params.put("city_page_no", Integer.toString(pageCount));
                }


                new PrintClass("explore vehicle list params******getParams***"+params);
                return params;
            }



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        exploreVehicleListREquest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(exploreVehicleListREquest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_list").optString("message");
            Status= jobj_main.optJSONObject("vehicle_list").optString("status");
            strTotalRecord = jobj_main.optJSONObject("vehicle_list").optString("total_record");
            strPageLimit = jobj_main.optJSONObject("vehicle_list").optString("page_limit");

            JSONArray details=jobj_main.optJSONObject("vehicle_list").optJSONArray("details");

            if (Status.equals(StaticClass.SuccessResult)){
                arr_exploreModel=new ArrayList<ExploreModel>();
                for (int i=0; i<details.length(); i++){

                    JSONObject jobjDetails = details.getJSONObject(i);
                    ExploreModel exploremodel = new ExploreModel();
                    exploremodel.setState_id(jobjDetails.optString("state_id"));
                    exploremodel.setState_name(jobjDetails.optString("state_name"));
                    exploremodel.setCity_name(jobjDetails.optString("city_name"));
                    exploremodel.setCity_id(jobjDetails.optString("city_id"));
                    exploremodel.setHas_more_vehicle(jobjDetails.optString("has_more_vehicle"));

                    JSONArray jsonArrayVehicle = jobjDetails.optJSONArray("vehicle");

                   // System.out.println("**arr_exploreVehicleListModel**"+jsonArrayVehicle.length());

                    arr_exploreVehicleListModel=new ArrayList<ExploreVehicleListModel>();
                    for (int j=0; j<jsonArrayVehicle.length(); j++){
                        ExploreVehicleListModel exploreVehicleListModel = new ExploreVehicleListModel();
                        JSONObject jsonObjvehicleList = jsonArrayVehicle.getJSONObject(j);
                        exploreVehicleListModel.setId(jsonObjvehicleList.optString("id"));
                        exploreVehicleListModel.setVehicle_type_id(jsonObjvehicleList.optString("vehicle_type_id"));
                        exploreVehicleListModel.setVehicle_image(jsonObjvehicleList.optString("vehicle_image"));
                        exploreVehicleListModel.setDescription(jsonObjvehicleList.optString("description"));
                        arr_exploreVehicleListModel.add(exploreVehicleListModel);
                    }
                    exploremodel.setArr_exploreVehicleListModel(arr_exploreVehicleListModel);

                    arr_exploreModel.add(exploremodel);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)){
                ((ExploreVehicleList_Interface)mcontext).ExploreVehicleListInterface(strTotalRecord, strPageLimit,  arr_exploreModel);
            }

        }
    }
}

package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.DriverAdd_interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.Details;
import com.neo.cars.app.SetGet.Document;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.VolleyParser.CustomMultiPartRequest;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by parna on 9/11/18.
 */

public class CompanyDriverAdd_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", unique_id="", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private DriverDataModel driverDataModel;
    private UserDocumentModel userDocumentModel;
    private JSONObject details;
    private ArrayList<DriverDataModel> arrlistDriverModel;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void DriverAdd(Activity context, String strDriverFirstName, String strDriverLastName, String strDriverSex,
                          String strDriverAddress, String strDriverNationality, String strDriverEmploymentSince, String strDriverFatherName,
                          String strDriverMobNo , String strDriverMaritalStatus, String strDriverSpouseName, String strDriverChildrenName,
                          String strDriverAddlInfo, File fileDriverPic, String strDriverDob, File fileAdhaarcard, File fileAdhaarcardBack,
                          File fileDrivingLicensePic, File fileDrivingLicensePicBack, String strdriverId){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        driverDataModel = new DriverDataModel();
        userDocumentModel = new UserDocumentModel();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        MultipartEntity entity = new MultipartEntity();

        try{

            entity.addPart("user_id", new StringBody(sharedPref.getUserid()));

            entity.addPart("first_name", new StringBody(strDriverFirstName));
            Log.d("d", "Driver first name: "+strDriverFirstName);

            entity.addPart("last_name", new StringBody(strDriverLastName));
            Log.d("d", "Driver last name: "+strDriverFirstName);

            entity.addPart("sex", new StringBody(strDriverSex));
            Log.d("d", "Driver sex: "+strDriverSex);

            entity.addPart("address", new StringBody(strDriverAddress));
            Log.d("d", "Driver address: "+strDriverAddress);

            entity.addPart("nationality", new StringBody(strDriverNationality));
            Log.d("d", "Driver nationality: "+strDriverNationality);

            entity.addPart("in_employment_since", new StringBody(strDriverEmploymentSince));
            Log.d("d", "Driver employment since: "+strDriverEmploymentSince);

            entity.addPart("fathers_name", new StringBody(strDriverFatherName));
            Log.d("d", "Driver fathers name: "+strDriverFatherName);

            entity.addPart("contact_no", new StringBody(strDriverMobNo));
            Log.d("d", "Driver contact no: "+strDriverMobNo);

            entity.addPart("marital_status", new StringBody(strDriverMaritalStatus));
            Log.d("d", "Driver marital status: "+strDriverMaritalStatus);

            if (strDriverMaritalStatus.equals("Y")){

                if (!TextUtils.isEmpty(strDriverSpouseName)) {
                    entity.addPart("name_of_spouse", new StringBody(strDriverSpouseName));
                    Log.d("d", "Driver spouse: " + strDriverSpouseName);
                }

                if (strDriverChildrenName != null){
                    entity.addPart("name_of_childrens", new StringBody(strDriverChildrenName));
                    Log.d("d", "Driver children name: "+strDriverChildrenName);
                }
            }

            entity.addPart("additional_info", new StringBody(strDriverAddlInfo));
            Log.d("d", "Driver addl info: "+strDriverAddlInfo);

            if (fileDriverPic != null){
                entity.addPart("recent_photograph", new FileBody(fileDriverPic));
                Log.d("d", "Driver recent_photograph: " + fileDriverPic);
            }

            entity.addPart("dob", new StringBody(strDriverDob));
            Log.d("d", "Driver dob: "+strDriverDob);

            entity.addPart("driver_id", new StringBody(strdriverId));
            Log.d("d", "driver_id: "+strdriverId);


            if (fileAdhaarcard != null && !fileAdhaarcard.equals("")){
                entity.addPart("aadhar_document", new FileBody(fileAdhaarcard));
                Log.d("d", "Driver aadhar_document: "+fileAdhaarcard);
            }

            if (fileAdhaarcardBack != null && !fileAdhaarcardBack.equals("")){
                entity.addPart("aadhar_document_back", new FileBody(fileAdhaarcardBack));
                Log.d("d", "Driver aadhar_document: "+fileAdhaarcardBack);
            }

            if (fileDrivingLicensePic != null && !fileDrivingLicensePic.equals("")){
                entity.addPart("driving_licence", new FileBody(fileDrivingLicensePic));
                Log.d("d", "Driver driving_licence: "+fileDrivingLicensePic);
            }

            if (fileDrivingLicensePicBack != null && !fileDrivingLicensePicBack.equals("")){
                entity.addPart("driving_licence_back", new FileBody(fileDrivingLicensePicBack));
                Log.d("d", "Driver driving_licence: "+fileDrivingLicensePicBack);
            }


        }catch (Exception e){
            e.printStackTrace();

        }

        RequestQueue rq = Volley.newRequestQueue(mcontext);
        CustomMultiPartRequest customMultiPartRequest = new CustomMultiPartRequest(Urlstring.company_driver_add_update,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        Log.d("Error.Response", error.toString());
                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                Log.d("Response", response);
                Apiparsedata(response);
            }
        }, entity) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("xsrf-token",sharedPref.getKEY_Access_Token());
                System.out.println("params******getHeaders***" + params);

                return params;
            }
        };
        customMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        customMultiPartRequest.setShouldCache(false);
        // add the request object to the queue to be executed
        rq.add(customMultiPartRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("company_driver_add_update").optString("user_deleted");
            Msg = jobj_main.optJSONObject("company_driver_add_update").optString("message");
            Status= jobj_main.optJSONObject("company_driver_add_update").optString("status");

            details=jobj_main.optJSONObject("company_driver_add_update").optJSONObject("details");

            if (Status.equalsIgnoreCase(StaticClass.SuccessResult)) {

                Details driverDetails = new Details();
                driverDetails.setId(Integer.parseInt(details.optString("id")));
                driverDetails.setFirstName(details.optString("first_name"));
                driverDetails.setLastName(details.optString("last_name"));
                driverDetails.setDob(details.optString("dob"));
                driverDetails.setGender(details.optString("gender"));
                driverDetails.setAddress(details.optString("address"));
                driverDetails.setNationality(details.optString("nationality"));
                driverDetails.setInEmploymentSince(details.optString("in_employment_since"));
                driverDetails.setFathersName(details.optString("fathers_name"));
                driverDetails.setContactNo(details.optString("contact_no"));
                driverDetails.setMaritalStatus(details.optString("marital_status"));
                driverDetails.setNameOfSpouse(details.optString("name_of_spouse"));
                driverDetails.setRecentPhotograph(details.optString("recent_photograph"));
                driverDetails.setNameOfChildrens(details.optString("name_of_childrens"));
                driverDetails.setAdditionalInfo(details.optString("additional_info"));

                JSONArray jsonArray = details.optJSONArray("document");

                List<Document> documentList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    Document document = new Document();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    document.setId(Integer.parseInt(jsonObject.optString("id")));
                    document.setDocumentType(jsonObject.optString("document_type"));
                    document.setDocumentFile(jsonObject.optString("document_file"));
                    document.setDocumentNumber(jsonObject.optString("document_number"));
                    document.setDocumentExpiryDate(jsonObject.optString("document_expiry_date"));
                    document.setIsApproved(jsonObject.optString("is_approved"));

                    documentList.add(document);
                }

                driverDetails.setDocument(documentList);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if(Status.equals(StaticClass.SuccessResult)){

                ((DriverAdd_interface)mcontext).driverAddInterface(Status, Msg);

//                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
//                        Msg ,
//                        mcontext.getResources().getString(R.string.btn_ok),
//                        "");
//                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        alertDialogYESNO.dismiss();
//                        mcontext.finish();
//
//                    }
//                });
//
//                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialogYESNO.dismiss();
//                    }
//                });
//
//                alertDialogYESNO.show();

            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("company_driver_add_update").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    try {
                        Msg = jarrError.getString(0);
                        new CustomToast(mcontext, Msg);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

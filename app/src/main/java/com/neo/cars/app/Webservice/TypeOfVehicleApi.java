package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.BookingHours_Interface;
import com.neo.cars.app.Interface.TypeOfVehicleInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.BookingHoursModel;
import com.neo.cars.app.SetGet.TypeOfVehicleModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TypeOfVehicleApi {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private BookingHoursModel bookinghourModel;
    private JSONObject jsonObjectVehicleType;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    private ArrayList<TypeOfVehicleModel> arrlistVehicleType;

    public void typeOfVehicle (Activity context){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistVehicleType = new ArrayList<TypeOfVehicleModel>();
        gson = new Gson();
        bookinghourModel = new BookingHoursModel();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest bookingHoursRequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_types_filter,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response TypeVehicle", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

//                if(strAvailableFor != null){
//                    params.put("available_for", "N"); // N for new booking
//                }
//
//                params.put("user_id", UserLoginDetails.getId());
//
//                new PrintClass("booking hours params******getParams***"+params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        bookingHoursRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(bookingHoursRequest);
    }


    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{

            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_types_filter").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_types_filter").optString("message");
            Status = jobj_main.optJSONObject("vehicle_types_filter").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("vehicle_types_filter").optJSONArray("details");
            for (int i=0; i<jsonArrayDetails.length(); i++){
                jsonObjectVehicleType = jsonArrayDetails.optJSONObject(i);

                TypeOfVehicleModel typeOfVehicleModel = new TypeOfVehicleModel();
                typeOfVehicleModel.setId(jsonObjectVehicleType.optString("id"));
                typeOfVehicleModel.setName(jsonObjectVehicleType.optString("name"));

                arrlistVehicleType.add(typeOfVehicleModel);
            }
            Log.d("d", "Booking hours size: "+ arrlistVehicleType.size());
            Log.e("d", "Booking hours: " + arrlistVehicleType);


        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }


//        if ("Y".equalsIgnoreCase(strUserDeleted)){
//            StaticClass.isLoginFalg=true;
//            transitionflag = StaticClass.transitionflagBack;
//            mcontext.finish();
//
//        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                ((TypeOfVehicleInterface)mcontext).TypeOfVehicle(arrlistVehicleType);

            }

//        }

    }
}

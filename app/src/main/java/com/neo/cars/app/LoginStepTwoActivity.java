package com.neo.cars.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;

/**
 * Created by parna on 2/3/18.
 */

public class LoginStepTwoActivity extends AppCompatActivity {

    private Button btnLogin,btnloginwithMobile;
    private ImageButton ibBackbutton;
    private TextView tvByLoggingIntoText_termsservices;
    private int transitionflag = StaticClass.transitionflagNext;
    private SharedPrefUserDetails prefs;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_step_two);

        new AnalyticsClass(LoginStepTwoActivity.this);

        Initialize();
        Listener();

    }

    private void Initialize(){
        prefs = new SharedPrefUserDetails(this);
        btnLogin = findViewById(R.id.btnLogin);
        btnloginwithMobile= findViewById(R.id.btnloginwithMobile);
        ibBackbutton= findViewById(R.id.ibBackbutton);
        tvByLoggingIntoText_termsservices=findViewById(R.id.tvByLoggingIntoText_termsservices);

    }

    private void Listener(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagNext;
                Intent intent=new Intent(LoginStepTwoActivity.this,Login_Parent_Activity.class);
                intent.putExtra("logintype",1);
                startActivity(intent);
                finish();
            }
        });


        btnloginwithMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagNext;
                StaticClass.Is_MobileNo = false;
                Intent intent=new Intent(LoginStepTwoActivity.this,Login_Parent_Activity.class);
                intent.putExtra("logintype",2);
                startActivity(intent);
//                finish();
            }
        });

        ibBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagBack;
                Intent i = new Intent(LoginStepTwoActivity.this, LoginStepOneActivity.class);
                startActivity(i);
                finish();
            }
        });

        tvByLoggingIntoText_termsservices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent termsIntent = new Intent(LoginStepTwoActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        new OnPauseSlider(LoginStepTwoActivity.this, transitionflag);
    }


    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        Intent i = new Intent(LoginStepTwoActivity.this, LoginStepOneActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        if (prefs.getLoginStatus()){
            finish();
        }
    }
}

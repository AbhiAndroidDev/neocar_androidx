package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.neo.cars.app.Interface.CityList_Interface;
import com.neo.cars.app.SetGet.CityListModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.CityList_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class CityList extends AppCompatActivity implements CityList_Interface {

    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;

    private EditText ET_ADDRESS_AUTOCOMPLETE;
    private LinearLayout LIST_ADDRESSES,linear_search;

    private LinearLayout LIST_FAVOURITE;
    private Timer timer;
    boolean timerflag;
    private ArrayList<CityListModel> arr_listState,final_arr_listState;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private String strStateId;
    private ConnectionDetector cd;
    private String FilterString="";
    private CustomDialog pdCustomDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_list);

        strStateId=getIntent().getExtras().getString("strStateId");
        new AnalyticsClass(CityList.this);
        Initialize();
        Listener();
        if(cd.isConnectingToInternet()){
            new CityList_Webservice().cityListWebservice(CityList.this, strStateId);

        }else{
            transitionflag = StaticClass.transitionflagBack;
            startActivity(new Intent(CityList.this, NetworkNotAvailable.class));
        }
    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.CityList));

        cd = new ConnectionDetector(this);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        LIST_ADDRESSES=findViewById(R.id.LIST_ADDRESSES);
        linear_search=findViewById(R.id.linear_search);
        linear_search.setVisibility(View.VISIBLE);

        ET_ADDRESS_AUTOCOMPLETE=findViewById(R.id.ET_ADDRESS_AUTOCOMPLETE);
        ET_ADDRESS_AUTOCOMPLETE.setMaxWidth(ET_ADDRESS_AUTOCOMPLETE.getWidth());

        ET_ADDRESS_AUTOCOMPLETE.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {

                LIST_ADDRESSES.removeAllViews();
                timerflag=false;
                try {
                    timer.cancel();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                System.out.println("*****addTextChangedListener*****");
                try {
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if(timerflag){
                                try {
                                    timer.cancel();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                                runOnUiThread(new Runnable(){

                                    @Override
                                    public void run() {
                                        FilterString=s.toString();
                                        newArrayList();
                                    }
                                });
                            }else{
                                timerflag=true;
                            }
                        }
                    },0, 1*500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void Listener(){
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                Intent resultintent=new Intent();
                resultintent.putExtra("CityName","");
                resultintent.putExtra("CityId","");
                setResult(StaticClass.CityLitRequestCode,resultintent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        Intent resultintent=new Intent();
        resultintent.putExtra("CityName","");
        resultintent.putExtra("CityId","");
        setResult(StaticClass.CityLitRequestCode,resultintent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CityList.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public void CityList(ArrayList<CityListModel> arrlistState) {

        arr_listState=new ArrayList<>();
        arr_listState=arrlistState;
        newArrayList();
    }

    private void newArrayList(){

        final_arr_listState=new ArrayList<>();
        LIST_ADDRESSES.removeAllViews();

        if(FilterString.equals("")){
            final_arr_listState=arr_listState;
        }else{
            for(int i=0;i<arr_listState.size();i++){
                if(arr_listState.get(i).getName().toLowerCase().contains(FilterString.toLowerCase())){
                    CityListModel cityListModel = new CityListModel();
                    cityListModel.setId(arr_listState.get(i).getId());
                    cityListModel.setName(arr_listState.get(i).getName());

                    final_arr_listState.add(cityListModel);
                }
            }
        }

        for(int i=0;i<final_arr_listState.size();i++)
        {
            LIST_ADDRESSES.addView(getView(i));
        }
    }

    public View getView(final int position){

        LayoutInflater inflater=getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_item, null,true);

      /*  Animation animation= AnimationUtils.loadAnimation(CityList.this,android.R.anim.slide_in_left);
        animation.setDuration(5);
        animation.setStartOffset(position*150);

        rowView.startAnimation(animation);*/

        TextView autotext=rowView.findViewById(R.id.autotext);

        autotext.setText(""+final_arr_listState.get(position).getName());

        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                TextView addr=v.findViewById(R.id.autotext);
                transitionflag = StaticClass.transitionflagBack;
                Intent resultintent=new Intent();
                resultintent.putExtra("CityName",final_arr_listState.get(position).getName());
                resultintent.putExtra("CityId",final_arr_listState.get(position).getId());
                setResult(Activity.RESULT_OK,resultintent);
                finish();
            }
        });

        return rowView;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CityList.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

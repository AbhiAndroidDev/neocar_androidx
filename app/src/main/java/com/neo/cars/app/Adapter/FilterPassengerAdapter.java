package com.neo.cars.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.neo.cars.app.Interface.TripCostSelectInterface;
import com.neo.cars.app.R;

import java.util.ArrayList;

public class FilterPassengerAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> tripCostList;
    private int selectedPosition = 0;
    private TripCostSelectInterface selectInterface;
    private LayoutInflater inflter;
    private TextView textView;

    public FilterPassengerAdapter(Context mContext, TripCostSelectInterface _selectInterface, ArrayList<String> arrListCost, int _selectedPosition) {
        this.mContext = mContext;
        this.tripCostList = arrListCost;
        this.selectedPosition = _selectedPosition;
        this.selectInterface = _selectInterface;

        inflter = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return tripCostList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = inflter.inflate(R.layout.item_passenger, null);

        textView = view.findViewById(R.id.tv_item_name);

//        final TextView textView;
//
//        if (convertView == null) {
//
//            textView = new TextView(mContext);
//            textView.setLayoutParams(new GridView.LayoutParams(170, 100));
//            textView.setPadding(15, 15, 15, 15);
//            textView.setBackgroundResource(R.drawable.base);
//            textView.setGravity(Gravity.CENTER);
//
//
//        }else{
//            textView = (TextView) convertView;
//        }

        textView.setText(tripCostList.get(position));

        if(selectedPosition == position){
            textView.setBackgroundResource(R.drawable.base_yellow);
        }
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setBackgroundResource(R.drawable.base_yellow);
                selectInterface.setTripCost(position, "filter_passenger", 0);
            }
        });

        return textView;
    }
}

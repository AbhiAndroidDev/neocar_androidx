package com.neo.cars.app.Utils;

import android.app.Activity;

import com.neo.cars.app.R;


public class OnPauseSlider {

    public OnPauseSlider(Activity activity, int transitionflag) {
        if (transitionflag == 2) {
            activity.overridePendingTransition(R.anim.right_slide_in_one,
                    R.anim.right_slide_out_one);
        } else if (transitionflag == 1) {
            activity.overridePendingTransition(R.anim.right_slide_in,
                    R.anim.right_slide_out);
        }
    }
}

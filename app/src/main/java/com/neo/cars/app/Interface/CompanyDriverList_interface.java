package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.CompanyDriverListModel;

import java.util.ArrayList;

/**
 * Created by parna on 9/11/18.
 */

public interface CompanyDriverList_interface {

    void companyDriverList(ArrayList<CompanyDriverListModel> arrlistDriver);
}

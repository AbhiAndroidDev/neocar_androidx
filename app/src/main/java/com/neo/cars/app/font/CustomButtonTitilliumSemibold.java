package com.neo.cars.app.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

/**
 * Created by parna on 28/2/18.
 */

public class CustomButtonTitilliumSemibold extends AppCompatButton {
    public CustomButtonTitilliumSemibold(Context context) {
        super(context);
    }

    public CustomButtonTitilliumSemibold(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomButtonTitilliumSemibold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if(!isInEditMode()){
            //------ Font------//
            Typeface typeface1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/TitilliumWeb-SemiBold.ttf");
            //Typeface typeface2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/SaintAndrewdesKiwis.ttf");

            setTypeface(typeface1,   Typeface.NORMAL);
        }

    }


}

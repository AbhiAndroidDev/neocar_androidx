package com.neo.cars.app.SetGet;

/**
 * Created by parna on 18/9/18.
 */

public class RatingReviewAllListModel {
    String review_id;
    String rating;
    String comment;
    String riview_by_id;
    String send_anonymously;
    String riview_by_name;
    String updated_at;

    public String getSend_anonymously() {
        return send_anonymously;
    }

    public void setSend_anonymously(String send_anonymously) {
        this.send_anonymously = send_anonymously;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRiview_by_id() {
        return riview_by_id;
    }

    public void setRiview_by_id(String riview_by_id) {
        this.riview_by_id = riview_by_id;
    }

    public String getRiview_by_name() {
        return riview_by_name;
    }

    public void setRiview_by_name(String riview_by_name) {
        this.riview_by_name = riview_by_name;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

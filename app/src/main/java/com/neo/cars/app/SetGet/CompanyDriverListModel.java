package com.neo.cars.app.SetGet;

/**
 * Created by parna on 9/11/18.
 */

public class CompanyDriverListModel {

    String driver_id;
    String name;
    String age;
    String contactno;
    String recent_photograph;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contact_no) {
        this.contactno = contact_no;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRecent_photograph() {
        return recent_photograph;
    }

    public void setRecent_photograph(String recent_photograph) {
        this.recent_photograph = recent_photograph;
    }
}

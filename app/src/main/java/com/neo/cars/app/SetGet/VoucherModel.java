package com.neo.cars.app.SetGet;

/**
 * Created by parna on 22/5/18.
 */

public class VoucherModel {

    String total_amount;
    String total_discount;
    String net_payable_amount;
    String night_charge;


    public String getNight_charge() {
        return night_charge;
    }

    public void setNight_charge(String night_charge) {
        this.night_charge = night_charge;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(String total_discount) {
        this.total_discount = total_discount;
    }

    public String getNet_payable_amount() {
        return net_payable_amount;
    }

    public void setNet_payable_amount(String net_payable_amount) {
        this.net_payable_amount = net_payable_amount;
    }
}

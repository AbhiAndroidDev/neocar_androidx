package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.BookingInformationModel;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.InboxModal;
import com.neo.cars.app.SetGet.VehicleInformationModel;

import java.util.ArrayList;

/**
 * Created by joydeep on 7/5/18.
 */

public interface MyBookingDetails_Interface {
    public void OnMyBookingDetails(BookingInformationModel bookingInformationModel,
                                   DriverDataModel driverdataModel,
                                   VehicleInformationModel vehicleInformationModel, String strShowDriver, String strShowDriverMsg);
}

package com.neo.cars.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.neo.cars.app.Interface.VehicleUnAvailability_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleUnAvailabilityModel;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Webservice.GetVehicleUnAvailableDateWebService;
import com.neo.cars.app.calendar.CalendarAdapterVehicle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by mithilesh on 1/11/18.
 */

public class VehicleAvailabilityInfofragment extends Fragment implements VehicleUnAvailability_Interface {

    private View view;
    private Context mContext;
    private String strvehicleId="", strYearMonth = "";
    private ConnectionDetector cd;
    public GregorianCalendar month, itemmonth;// calendar instances.
    public CalendarAdapterVehicle adapter;// adapter instance
    private ArrayList<String> eventList = new ArrayList<>();

    private GridView gridview;
    private TextView title;
    private ImageView previous,next;
    private int rightClickCount = 0;
    private LinearLayout calendar_ll;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {

            view = inflater.inflate(R.layout.calendar_availability_info, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            strvehicleId = getArguments().getString("vehicleId");
            Log.d("d", "Get strvehicleId: "+strvehicleId);
        }

        Initialize(view);
        Listener();
        strYearMonth = getCurrentDate2();
        callWebService(strYearMonth);

        return view;
    }

    private void Initialize(View mView) {
        mContext = getActivity();
        cd = new ConnectionDetector(getActivity());
        mContext = getContext();

        calendar_ll = mView.findViewById(R.id.calendar_ll);
        gridview = mView.findViewById(R.id.calendar_grid);
        title = mView.findViewById(R.id.calendar_date_display);
        previous = mView.findViewById(R.id.calendar_prev_button);
        next = mView.findViewById(R.id.calendar_next_button);

        setCalendarView();
    }

    private void setCalendarView() {

        month = (GregorianCalendar) GregorianCalendar.getInstance();
        itemmonth = (GregorianCalendar) month.clone();
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

        adapter = new CalendarAdapterVehicle(getActivity(), month, eventList, strvehicleId);
        gridview.setAdapter(adapter);

        adapter.refreshDays();
        adapter.notifyDataSetChanged();

        previous.setBackgroundResource(R.drawable.ic_arrow_left_calender_dselect);

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightClickCount--;
                disablePreviousButton(false);
                disableNextButton(false);
                setPreviousMonth();
                refreshCalendar();
                if (rightClickCount == 0){
                    disablePreviousButton(true);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightClickCount++;
                if (rightClickCount >= 5){
                    disableNextButton(true);
                }
                disablePreviousButton(false);
                setNextMonth();
                refreshCalendar();
            }
        });

        if (rightClickCount == 0){
            disablePreviousButton(true);
        }
    }

    protected void setNextMonth() {

        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) + 1),
                    month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) + 1);
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM", Locale.US);
        strYearMonth = df.format(month.getTime());
        Log.d("strYearMonth", strYearMonth);

        callWebService(strYearMonth);
    }

    protected void setPreviousMonth() {
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) - 1),
                    month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1);
        }
    }

    public void disablePreviousButton(boolean isConditionMatched){

        if(isConditionMatched){
            previous.setClickable(false);
            previous.setEnabled(false);
            previous.setBackgroundResource(R.drawable.ic_arrow_left_calender_dselect);
//            previous.setVisibility(View.INVISIBLE);
        }else {
            previous.setClickable(true);
            previous.setEnabled(true);
            previous.setBackgroundResource(R.drawable.ic_arrow_right_calender_select_1);
//            previous.setVisibility(View.VISIBLE);
        }
    }

    //created by miths to disable/hide next button when 4th month from current month shows

    public void disableNextButton(boolean isConditionMatched){

        if(isConditionMatched){
            next.setClickable(false);
            next.setEnabled(false);
            next.setBackgroundResource(R.drawable.ic_arrow_right_calender_dselect);
//            next.setVisibility(View.INVISIBLE);
        }else {
            next.setClickable(true);
            next.setEnabled(true);
            next.setVisibility(View.VISIBLE);
            next.setBackgroundResource(R.drawable.ic_arrow_right_calender_select);
        }
    }

    public void refreshCalendar() {
        adapter = new CalendarAdapterVehicle(getActivity(), month, eventList, strvehicleId);
        gridview.setAdapter(adapter);
        adapter.refreshDays();
        adapter.notifyDataSetChanged();
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
    }

    private void callWebService(String strYearMonth) {

        if (NetWorkStatus.isNetworkAvailable(getActivity())) {
            new GetVehicleUnAvailableDateWebService().getUnAvailableDate(getContext(), getActivity(), strvehicleId, strYearMonth, VehicleAvailabilityInfofragment.this);
        } else {
            Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    private void Listener() {
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public String getCurrentDate2() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
        //strYearMonth used to send as params in webservice
        strYearMonth = df.format(c);
        return strYearMonth;
    }

    @Override
    public void unAvailableDate(ArrayList<VehicleUnAvailabilityModel> arrListVehicleUnAvailabilityModel) {

        for (int i = 0; i < arrListVehicleUnAvailabilityModel.size(); i++){
            String strUnAvailableDate = arrListVehicleUnAvailabilityModel.get(i).getUnavailable_date();
            setUnAvailableDateOnCalendar(strUnAvailableDate);
        }
        adapter = new CalendarAdapterVehicle(getActivity(), month, eventList, strvehicleId);
        gridview.setAdapter(adapter);
    }

    public void setUnAvailableDateOnCalendar(String strUnAvailableDate){

        Log.d("strUnAvailableDate", strUnAvailableDate);
        eventList.add(strUnAvailableDate);
    }
}

package com.neo.cars.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.Interface.WishlistInterface;
import com.neo.cars.app.SetGet.WishlistModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.WishlistListing_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWeblight;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Wishlist extends AppCompatActivity implements WishlistInterface , SwipeRefreshLayout.OnRefreshListener{


    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;

    private CustomTextviewTitilliumWeblight tvNoWishlistFound;
    private GridView wishlist_gride;
    private MyAdapter ad;

    private ArrayList<WishlistModel> arr_InboxModal =new ArrayList<>();
    private RelativeLayout rlBackLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);

        new AnalyticsClass(Wishlist.this);

        Initialize();

    }

    private void Initialize() {

        swipeRefreshLayout = findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(Wishlist.this);
        wishlist_gride=findViewById(R.id.wishlist_gride) ;

        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvWishlistHeader));
        rlBackLayout = findViewById(R.id.rlBackLayout);

        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlBackLayout.setVisibility(View.GONE);

        tvNoWishlistFound = findViewById(R.id.tvNoWishlistFound);

        bottomview.BottomView(Wishlist.this, StaticClass.Menu_Wishlist);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }


    private void  refreshList(){

        if (NetWorkStatus.isNetworkAvailable(Wishlist.this)) {
            new WishlistListing_Webservice().WishlistListing(Wishlist.this);
        } else {
            Intent i = new Intent(Wishlist.this, NetworkNotAvailable.class);
            startActivity(i);
        }
        swipeRefreshLayout.setRefreshing(false);

    }



    @Override
    public void OnWishlist(ArrayList<WishlistModel> ar_InboxModal) {

        if(ar_InboxModal.size() > 0){
            arr_InboxModal=ar_InboxModal;
            ad = new MyAdapter(Wishlist.this,R.layout.whishlist_inflate,arr_InboxModal);
            wishlist_gride.setAdapter(ad);
            ad.notifyDataSetChanged();
            wishlist_gride.setTextFilterEnabled(true);

        }else{
            wishlist_gride.setVisibility(View.GONE);
            tvNoWishlistFound.setVisibility(View.VISIBLE);
        }

    }


    public class MyAdapter extends ArrayAdapter<WishlistModel> {
        ArrayList<WishlistModel> my_al;
        public MyAdapter(Context context, int row, ArrayList<WishlistModel> al) {
            super(context, row, al);
            my_al=al;
        }

        @SuppressLint("InflateParams")
        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                LayoutInflater inflater=getLayoutInflater();
                convertView=inflater.inflate(R.layout.wishlist_inflate_new,parent,false);

                ImageView wishlist_image=convertView.findViewById(R.id.wishlist_image);
                TextView tv_brandname=convertView.findViewById(R.id.tv_brandname);
                TextView tv_vehiclemake=convertView.findViewById(R.id.tv_vehiclemake);
                TextView tv_Total_no_of_km=convertView.findViewById(R.id.tv_Total_no_of_km);
                TextView tv_Tmax_passenger=convertView.findViewById(R.id.tv_Tmax_passenger);
                RelativeLayout rlParentRelative=convertView.findViewById(R.id.rlParentRelative);


                Picasso.get().load(my_al.get(position).getImage_url()).into(wishlist_image);

                tv_brandname.setText(my_al.get(position).getBrand());
                tv_vehiclemake.setText(my_al.get(position).getMake());
                tv_Total_no_of_km.setText(my_al.get(position).getTotal_no_km().replace(".00","")+"Km");
                tv_Tmax_passenger.setText(my_al.get(position).getMax_passenger());



                        rlParentRelative.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent wishlistdetailsintent = new Intent(Wishlist.this, BookACarActivity.class);
                                wishlistdetailsintent.putExtra("VehicleId", my_al.get(position).getVehicle_id());
                                wishlistdetailsintent.putExtra("IsComingFrom", StaticClass.Wishlist);
                                wishlistdetailsintent.putExtra("WishlistId", my_al.get(position).getWishlist_id());
                                Log.d("d", "Vehicle id::" + my_al.get(position).getVehicle_id());
                                Log.d("d", "WishlistId::" + my_al.get(position).getWishlist_id());
                                startActivity(wishlistdetailsintent);

                            }
                        });


            } catch (Exception e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(Wishlist.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }


    @Override
    protected void onResume() {
        super.onResume();

        StaticClass.BottomWishlist=false;
        StaticClass.BottomExplore = false;

        refreshList();

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }
}
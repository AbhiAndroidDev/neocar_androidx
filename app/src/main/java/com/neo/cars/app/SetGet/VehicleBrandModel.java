package com.neo.cars.app.SetGet;

/**
 * Created by parna on 31/8/18.
 */

public class VehicleBrandModel {
    String id;
    String code;
    String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

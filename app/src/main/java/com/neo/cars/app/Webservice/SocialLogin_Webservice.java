package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.CompanyHomeProfileActivity;
import com.neo.cars.app.HomeProfileActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SearchActivity;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 19/3/18.
 */

public class SocialLogin_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", unique_id="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;


    String st_login_type="",st_social_id="",St_emailId="",St_Mobile="", St_Name="", St_Photo="";



    public void SocialLogin(Activity context, final String login_type,String social_id,
                            String emailId,String Mobile, String strName, String strPhoto) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        st_login_type=login_type;
        st_social_id=social_id;
        St_emailId=emailId;
        St_Mobile=Mobile;
        St_Name = strName;
        St_Photo = strPhoto;

        sharedPref = new SharedPrefUserDetails(mcontext);

        unique_id = Settings.Secure.getString(mcontext.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("d", "Device id::"+unique_id);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.social_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login_type",st_login_type);
                params.put("email",St_emailId);
                params.put("mobile_no",St_Mobile);
                params.put("device_type", "A");
                params.put("device_id", unique_id);
                params.put("name", St_Name);
//                params.put("social_image_url", St_Photo);

                Log.d("d", "Social login nhame: "+St_Name);
                Log.d("d", "Social login photo: "+St_Photo);

                if(st_login_type.equals(StaticClass.FacebookLogin)){
                    params.put("facebook_id",st_social_id);
                }else  if(st_login_type.equals(StaticClass.GoogleLogin)){
                    params.put("gplus_id",st_social_id);
                }else  if(st_login_type.equals(StaticClass.TwitterLogin)){
                    params.put("twitter_id",st_social_id);
                }

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);

    }




    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);


            Msg = jobj_main.optJSONObject("social_login").optString("message");
            Status= jobj_main.optJSONObject("social_login").optString("status");

            JSONObject details=jobj_main.optJSONObject("social_login").optJSONObject("details");

            if (Status.equals(StaticClass.SuccessResult)) {
                UserLoginDetails.setId(details.optString("id"));
                sharedPref.setUserId(details.optString("id"));
                UserLoginDetails.setEmail(details.optString("email"));
                UserLoginDetails.setMobile(details.optString("mobile_no"));
                UserLoginDetails.setIs_active(details.optString("is_active"));
                //UserLoginDetails.setAd_username(details.optString("profile_name"));
                UserLoginDetails.setProfile_name(details.optString("profile_name"));
                UserLoginDetails.setProfile_pic(details.optString("profile_pic"));

                UserLoginDetails.setProfile_pic(details.optString("profile_pic"));
                UserLoginDetails.setReg_type(details.optString("reg_type"));
                UserLoginDetails.setReferral_code(details.optString("referral_code"));
                UserLoginDetails.setIs_notification_active(details.optString("is_notification_active"));
                UserLoginDetails.setIs_profile_complete(details.optString("is_profile_complete"));
                sharedPref.setUserType(details.optString("user_type"));

                String strParentDetails = gson.toJson(UserLoginDetails);
                sharedPref.putObjectToPreferenceUserDetails(strParentDetails);
                sharedPref.setUserloginStatus();

            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("social_login").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);

            }

        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

       // new CustomToast(mcontext, Msg);

        if (Status.equals(StaticClass.SuccessResult)) {
            if (sharedPref.getUserType().equalsIgnoreCase("U")) {
//                Intent invitefriendsIntent = new Intent(mcontext, HomeProfileActivity.class);
                Intent invitefriendsIntent = new Intent(mcontext, SearchActivity.class);
                mcontext.startActivity(invitefriendsIntent);
                mcontext.finish();
            }else if (sharedPref.getUserType().equalsIgnoreCase("C")){
                Intent invitefriendsIntent = new Intent(mcontext, CompanyHomeProfileActivity.class);
                mcontext.startActivity(invitefriendsIntent);
                mcontext.finish();
            }
        }
    }
}

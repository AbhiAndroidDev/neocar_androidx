package com.neo.cars.app.Utils;

/**
 * Created by Avijit on 14-03-2016.
 */
public class MessageText {

    public static String SENDER_ID="567048002762";
    public static int PLAY_SERVICES_RESOLUTION_REQUEST 	= 9000;

    public static String Network_not_availabl="Network unavailable. Please try again later.";
    public static String Person_information_is_null="Your G+ profile could not be found";
    public static String lat_long="Please check if your GPS is enabled on your device and restart the application.";
    public static String GPs_are_not_enable="Gps is not enabled.";

    public static final String TryAfterSomeTime = "Server error. Please try again later.";
    public static String Enter_all_added_field="Add location in the blank field.";
    public static String FromDate_is_after_ToDate="From Date is after To Date.";
    public static String Enter_name_mobno = "Enter name and valid mobile number in the blank field.";
//    public static String IamusingNeocars = " NeoCars is India’s first and only car hiring service that enables you to enjoy " +
//            "the many luxuries of a personal car without actually having to own one yourself.\n https://play.google.com/store/apps/details?id=";
    public static String IamusingNeocars = " NeoCars is India’s first and only car hiring service that enables you to enjoy " +
            "the many luxuries of a personal car without actually having to own one yourself.\n https://neocars.app.link";

    public static String Pleaeuseaformatasfollows = "Please use a format as follows:\n" +
            "MP 09 AB 1234\n" +
            " or MP 09 A 1234\n" +
            "or MP 09 1234" +
            "\n" +
            "Put blank spaces as shown in the format."+"\n"+
            "This information cannot be edited/changed later. Kindly make sure you have entered the number correctly.";

    public static String PickUpLocationInfoText = "Airport is mandatory option.\n" +
            "It is suggested to add 3 more popular locations around your city such as railway stations, popular landmarks, etc to increase chances of receiving bookings.";

    public static String OtherLocationWhileBookingText = "If you check this box, the user booking your vehicle will get the option to enter a custom address for pick up. Additional charges apply.";

    public static String tvParkingChargesHeader = "Included - Parking/Toll Charges is included in the vehicle cost. Rider to NOT be charged for the same.\n"+"\n"+
            "At Actuals - Parking/Toll Charges to be paid by the Rider, as and when incurred.";

    public static String tvinfoRegionalPermitHeader = "Valid ‘Permit’ granted by the Regional or State Transport\n" +
            "Authority to your vehicle allowing it to be used as a Commercial\n" +
            "Passenger Vehicle and/or Tourist Vehicle. Includes the AITP permit.";

    public static String tvinfoRegistrationCertificate = "Kindly upload a clear scan copy/photo of the FRONT SIDE of the 'Vehicle Registration Card' issued by the RTO.";
    public static String tvinfoRegistrationCertificateBack = "Kindly upload a clear scan copy/photo of the BACK SIDE of the 'Vehicle Registration Card' issued by the RTO.";

    public static String tvinfoFitnessCertificate = "Valid ‘Fitness Certificate’ of your vehicle issued by the\n" +
            "RTO.";
    public static String tvinfoInsuranceCertificate = "Valid ‘Insurance Certificate’ for your vehicle issued by a\n" +
            "competent issuing authority.";
    public static String tvRatingInfo = "This is a cumulative average rating of the scores displayed below that you have received from the vehicle owners with whom you have completed rides successfully.";

    public static String tvVehicleRatingInfo = "This is the average star rating based on the overall star ratings received for this vehicle by other users who have successfully completed bookings with it in the past.";
}

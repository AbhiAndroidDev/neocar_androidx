package com.neo.cars.app.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationCheck {

    private Matcher Emailmatcher;

    public boolean email_validation(String EmailString) {
        Pattern pattern;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        Emailmatcher = pattern.matcher(EmailString);
        return Emailmatcher.matches();
    }

    public boolean phone_validation(String phone) {
        String PHONE_PATTERN = "^[+]?[0-9]{10,13}$";
        return phone.matches(PHONE_PATTERN);
    }
}

package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.HomeProfileActivity;
import com.neo.cars.app.LoginStepOneActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 20/3/18.
 */

public class UpdatePassword_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    String st_oldpass="", st_newpass="", st_confpass="";
    private int transitionflag = StaticClass.transitionflagNext;

    public void UpdatePassword (Activity context, String strOldPass, String strNewPass, String strConfNewPass){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        st_oldpass = strOldPass;
        st_newpass = strNewPass;
        st_confpass = strConfNewPass;
        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest updatePasswordRequest = new StringRequest(Request.Method.POST, Urlstring.update_password,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    hideProgressDialog();
                    new CustomToast(mcontext, Msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }){

        @Override
        protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<String, String>();
            params.put("user_id",UserLoginDetails.getId());
            params.put("old_password",st_oldpass);
            params.put("password", st_newpass);
            params.put("password_confirmation", st_confpass);

            new PrintClass("params******getParams***"+params);
            return params;
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("xsrf-token",sharedPref.getKEY_Access_Token());

            new PrintClass("params******Header***"+params);
            return params;
        }
    };

        updatePasswordRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(updatePasswordRequest);
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("reset_password").optString("user_deleted");
            Msg = jobj_main.optJSONObject("reset_password").optString("message");
            Status= jobj_main.optJSONObject("reset_password").optString("status");

            JSONObject details=jobj_main.optJSONObject("reset_password").optJSONObject("details");

        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        new CustomToast(mcontext, Msg);



        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
//                Intent invitefriendsIntent = new Intent(mcontext, LoginStepOneActivity.class);
//                mcontext.startActivity(invitefriendsIntent);
//                mcontext.finish();
            }
        }
    }

}

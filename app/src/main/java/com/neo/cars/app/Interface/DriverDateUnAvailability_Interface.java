package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.DriverUnAvailabilityModel;

import java.util.ArrayList;

public interface DriverDateUnAvailability_Interface {

    void driverUnAvailableDate(ArrayList<DriverUnAvailabilityModel> arrListDriverUnAvailabilityModel);
}

package com.neo.cars.app.SetGet;

/**
 * Created by parna on 27/9/18.
 */

public class OvertimeAvailabilityVehicleModel {
    String id;
    String vehicle_type_id;
    String make;
    String model;
    String year;
    String km_travelled;
    String ac_available;
    String hourly_rate;
    String overtime_available;
    String hourly_overtime_rate;
    String max_luggage;
    String max_passenger;
    String max_addl_passenger;
    String description;
    String vehicle_image;

    public String getMax_addl_passenger() {
        return max_addl_passenger;
    }

    public void setMax_addl_passenger(String max_addl_passenger) {
        this.max_addl_passenger = max_addl_passenger;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getKm_travelled() {
        return km_travelled;
    }

    public void setKm_travelled(String km_travelled) {
        this.km_travelled = km_travelled;
    }

    public String getAc_available() {
        return ac_available;
    }

    public void setAc_available(String ac_available) {
        this.ac_available = ac_available;
    }

    public String getHourly_rate() {
        return hourly_rate;
    }

    public void setHourly_rate(String hourly_rate) {
        this.hourly_rate = hourly_rate;
    }

    public String getOvertime_available() {
        return overtime_available;
    }

    public void setOvertime_available(String overtime_available) {
        this.overtime_available = overtime_available;
    }

    public String getHourly_overtime_rate() {
        return hourly_overtime_rate;
    }

    public void setHourly_overtime_rate(String hourly_overtime_rate) {
        this.hourly_overtime_rate = hourly_overtime_rate;
    }

    public String getMax_luggage() {
        return max_luggage;
    }

    public void setMax_luggage(String max_luggage) {
        this.max_luggage = max_luggage;
    }

    public String getMax_passenger() {
        return max_passenger;
    }

    public void setMax_passenger(String max_passenger) {
        this.max_passenger = max_passenger;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVehicle_image() {
        return vehicle_image;
    }

    public void setVehicle_image(String vehicle_image) {
        this.vehicle_image = vehicle_image;
    }
}

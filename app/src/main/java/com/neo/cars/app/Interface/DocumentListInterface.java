package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.DocumentListModel;

import java.util.ArrayList;

/**
 * Created by parna on 26/3/18.
 */

public interface DocumentListInterface {

    void DocumentList(ArrayList<DocumentListModel> arrlistDoc);

}

package com.neo.cars.app;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.material.tabs.TabLayout;
import com.neo.cars.app.Adapter.ViewCouponAdapter;
import com.neo.cars.app.SetGet.ManageCouponModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.fragment.CouponPager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parna on 27/2/18.
 */

public class ManageCouponsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    private TabLayout tabs;
    private ViewPager viewpager;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile;
    private ImageButton ibNavigateMenu;
    private RecyclerView rcvCoupon;
    private ViewCouponAdapter viewCouponAdapter;
    private RelativeLayout rlBackLayout;

    private int transitionflag = StaticClass.transitionflagNext;
    private List<ManageCouponModel> manageCouponModelList = new ArrayList<>();
    private BottomView bottomview = new BottomView();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_coupons);

        new AnalyticsClass(ManageCouponsActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvManageCouponsHeader));
        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        rlBackLayout = findViewById(R.id.rlBackLayout);
        viewpager = findViewById(R.id.viewpager);
        tabs = findViewById(R.id.tabs);

        //Adding the tabs using addTab() method
        tabs.addTab(tabs.newTab().setText("View"));
        tabs.addTab(tabs.newTab().setText("Redeem"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        //Creating our pager adapter
        final CouponPager adapter = new CouponPager(getSupportFragmentManager(), tabs.getTabCount());
        //Adding adapter to pager
        viewpager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        tabs.setOnTabSelectedListener(this);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        bottomview.BottomView(ManageCouponsActivity.this,StaticClass.Menu_profile);
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(ManageCouponsActivity.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) ManageCouponsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

package com.neo.cars.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleSearchModel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.List;

/**
 * Created by parna on 12/4/18.
 */

public class ExploreCityListAdapter extends RecyclerView.Adapter<ExploreCityListAdapter.MyViewHolder> {

    private List<VehicleSearchModel> exploreCityList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout llCityHolder;
        ImageView ivLocation;
        CustomTextviewTitilliumWebRegular tvCityname;


        public MyViewHolder(View view) {
            super(view);
            llCityHolder = view.findViewById(R.id.llCityHolder);
            ivLocation = view.findViewById(R.id.ivLocation);
            tvCityname = view.findViewById(R.id.tvCityname);

        }
    }

    public ExploreCityListAdapter(Context mContext, List<VehicleSearchModel> mExploreCityModelList) {
        this.mContext = mContext;
        exploreCityList = mExploreCityModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_city_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ExploreCityListAdapter.MyViewHolder holder, final int position) {
        holder.tvCityname.setText(exploreCityList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return exploreCityList.size();
    }
}

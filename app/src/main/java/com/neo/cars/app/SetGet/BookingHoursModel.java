package com.neo.cars.app.SetGet;

/**
 * Created by parna on 7/5/18.
 */

public class BookingHoursModel {

    String label;
    String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

package com.neo.cars.app;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

/**
 * Created by parna on 27/2/18.
 */


public class HelpActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile, tvVersion;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;
    private CustomTextviewTitilliumWebRegular tvHowToUse, tvFAQ, tvContactUs, tvAboutUs;

    private int transitionflag = StaticClass.transitionflagNext;
    private BottomView bottomview = new BottomView();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        new AnalyticsClass(HelpActivity.this);

        Initialize();
        Listener();

    }

    private void Initialize(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvHelpHeader));
        rlBackLayout = findViewById(R.id.rlBackLayout);
        tvHowToUse = findViewById(R.id.tvHowToUse);
        tvFAQ = findViewById(R.id.tvFAQ);
        tvContactUs = findViewById(R.id.tvContactUs);
        tvAboutUs = findViewById(R.id.tvAboutUs);
        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        tvVersion = findViewById(R.id.tvVersion);

        bottomview.BottomView(HelpActivity.this,StaticClass.Menu_profile);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionName = pInfo.versionName;

            tvVersion.setText("Version "+versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        tvHowToUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent howtouseIntent = new Intent(HelpActivity.this, CmsPageActivity.class);
                howtouseIntent.putExtra("cms", StaticClass.HowToUse);
                startActivity(howtouseIntent);

            }
        });

        tvFAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent faqIntent = new Intent(HelpActivity.this, CmsPageActivity.class);
                faqIntent.putExtra("cms", StaticClass.FAQ);
                startActivity(faqIntent);

            }
        });

        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactusIntent = new Intent(HelpActivity.this, CmsPageActivity.class);
                contactusIntent.putExtra("cms", StaticClass.ContactUs);
                startActivity(contactusIntent);

            }
        });

        tvAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactusIntent = new Intent(HelpActivity.this, CmsPageActivity.class);
                contactusIntent.putExtra("cms", StaticClass.AboutUs);
                startActivity(contactusIntent);

            }
        });
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(HelpActivity.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();
       // StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }
}

package com.neo.cars.app;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.Interface.DialogListener;
import com.neo.cars.app.Interface.VehicleSave_interface;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.fragment.AddDriverInfofragment;
import com.neo.cars.app.fragment.AddVehicleInfoFragment;

/**
 * Created by parna on 16/3/18.
 */

public class AddVehicleDriverInfoActivity extends AppCompatActivity implements View.OnClickListener, VehicleSave_interface ,DialogListener {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private String strVehicleId = "";
    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;

    private LinearLayout vehicle_info_ll, driver_info_ll;
    private View vehicleview, driverview;
    private CustomTextviewTitilliumBold tv_vehicle_info, tv_driver_info;
    private String screenName = "driveInfo";
    private boolean vehicleNoClicked = false;
    private DialogListener dialogInterface;

    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_driver);

        new AnalyticsClass(AddVehicleDriverInfoActivity.this);

        Initialize();
        addFragment();
        Listener();

        setDialogInterface(this);
    }

    private void addFragment() {
        if (findViewById(R.id.fragment_container) != null) {

            AddVehicleInfoFragment addVehicleInfoFragment = new AddVehicleInfoFragment();
            addVehicleInfoFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, addVehicleInfoFragment).commit();
        }
    }

    private void Initialize() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvMyVehicleHeader));

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        vehicle_info_ll = findViewById(R.id.vehicle_info_ll);
        driver_info_ll = findViewById(R.id.driver_info_ll);

        vehicleview = findViewById(R.id.vehicle_info_view);
        driverview = findViewById(R.id.driver_info_view);

        tv_vehicle_info = findViewById(R.id.tv_vehicle_info);
        tv_driver_info = findViewById(R.id.tv_driver_info);

        vehicleview.setVisibility(View.VISIBLE);
        tv_vehicle_info.setTextColor(getResources().getColor(R.color.dark_jungle_green));

        bottomview.BottomView(AddVehicleDriverInfoActivity.this, StaticClass.Menu_profile);
    }

    private void Listener() {
        rlBackLayout.setOnClickListener(this);
        driver_info_ll.setOnClickListener(this);
        vehicle_info_ll.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.vehicle_info_ll:

                if (!screenName.equals("driveInfo")) {
                    setAlert2();
                }

                break;
            case R.id.driver_info_ll:

                if (!screenName.equals("vehicleInfo")) {
                    setAlert2();
                }
                break;
            case R.id.rlBackLayout:
                setAlert();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setAlert();
    }

    private void setAlert() {
        final CustomAlertDialogOKCancel alertDialogYESNO = new CustomAlertDialogOKCancel(AddVehicleDriverInfoActivity.this,
                AddVehicleDriverInfoActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                AddVehicleDriverInfoActivity.this.getResources().getString(R.string.yes),
                AddVehicleDriverInfoActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialogYESNO.dismiss();
            }
        });
        alertDialogYESNO.show();
    }

    public void setAlert2() {

        final CustomAlertDialogOKCancel alertDialogYESNO = new CustomAlertDialogOKCancel(AddVehicleDriverInfoActivity.this,
                AddVehicleDriverInfoActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                AddVehicleDriverInfoActivity.this.getResources().getString(R.string.yes),
                AddVehicleDriverInfoActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();

                if (screenName.equals("vehicleInfo")) {
                    dialogInterface.OnDialogInterface(vehicleview, screenName, new AddVehicleInfoFragment(), tv_vehicle_info, driverview, tv_driver_info);
                } else if (screenName.equals("driveInfo")) {
                    dialogInterface.OnDialogInterface(driverview, screenName, new AddDriverInfofragment(), tv_driver_info, vehicleview, tv_vehicle_info);
                }
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString("strVehicleId", strVehicleId);
        fragment.setArguments(args);
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public void vehicleSaveOnClick(String success, String msg, String strVehicleId, String userType) {
        getSupportFragmentManager().popBackStack();
        this.strVehicleId = strVehicleId;
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(AddVehicleDriverInfoActivity.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (StaticClass.BottomProfile) {
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) AddVehicleDriverInfoActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    public void setDialogInterface(DialogListener dialogListener) {
        this.dialogInterface = dialogListener;
    }

    @Override
    public void OnDialogInterface(View view, String screenName, Fragment fragment, TextView textView, View view2, TextView textView2) {
        view.setVisibility(View.VISIBLE);
        textView.setTextColor(getResources().getColor(R.color.dark_jungle_green));
        view2.setVisibility(View.INVISIBLE);
        textView2.setTextColor(getResources().getColor(R.color.dimgray));
        if (screenName.equals("vehicleInfo")) {
            this.screenName = "driveInfo";
        }
        if (screenName.equals("driveInfo")) {
            this.screenName = "vehicleInfo";
        }

        replaceFragment(fragment);
    }
}


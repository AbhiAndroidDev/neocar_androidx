package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.BookACarActivity;
import com.neo.cars.app.Interface.AddRemoveWishlist_Interface;
import com.neo.cars.app.Interface.WishAddDelete_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Wishlist;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 14/5/18.
 */

public class WishlistDelete_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private VehicleTypeModel vehicleTypeModel;
    private UserLoginDetailsModel UserLoginDetails;
    JSONObject details;
    private int transitionflag = StaticClass.transitionflagNext;

    public void wishlistDelete(Activity context, final String strUserId, final String strWishlistId){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        vehicleTypeModel = new VehicleTypeModel();

        showProgressDialog();

        StringRequest wishlistDeleteStringRequest = new StringRequest(Request.Method.POST, Urlstring.wishlist_delete,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d(":: Response details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", strUserId);
                params.put("wishlist_id", strWishlistId);

                new PrintClass("params******getParams***" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        wishlistDeleteStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(wishlistDeleteStringRequest);


    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("wishlist_delete").optString("user_deleted");
            Msg = jobj_main.optJSONObject("wishlist_delete").optString("message");
            Status= jobj_main.optJSONObject("wishlist_delete").optString("status");

            if (Status.equals(StaticClass.SuccessResult)){
                details = jobj_main.optJSONObject("wishlist_delete").optJSONObject("details");

            }else if (Status.equals(StaticClass.ErrorResult)){
                details = jobj_main.optJSONObject("wishlist_delete").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }
        }catch (Exception e){
            e.printStackTrace();
        }



        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                ((AddRemoveWishlist_Interface)mcontext).wishlistAddRemoveInterface(Msg,StaticClass.Explore_Whishlist_Delete);

         /*   if(strIsComingFrom.equalsIgnoreCase(StaticClass.ExploreCar)){
                Intent wishlistaddedIntent = new Intent(mcontext, BookACarActivity.class);
                mcontext.startActivity(wishlistaddedIntent);
                mcontext.finish();

                *//*Intent wishlistaddedIntent = new Intent(mcontext, Wishlist.class);
                mcontext.startActivity(wishlistaddedIntent);
                mcontext.finish();*//*

            }else if (strIsComingFrom.equalsIgnoreCase(StaticClass.Wishlist)){
                mcontext.finish();
            }*/
            } else {
                new CustomToast(mcontext, Msg);
            }
        }
    }

}

package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleUnAvailabilityDetailsModel;

public interface VehicleUnAvailableDetail_interface {

    void vehicleUnAvailableDetails(VehicleUnAvailabilityDetailsModel arrListVehicleUnAvailabilityModel, String status, String msg);
}

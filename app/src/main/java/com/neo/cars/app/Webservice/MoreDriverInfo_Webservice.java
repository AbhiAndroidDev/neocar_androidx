package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.MoreDriverInfo_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.PickUpLocationModel;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 25/4/18.
 */

public class MoreDriverInfo_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private DriverDataModel driverDataModel;
    private JSONObject details;
    private ArrayList<DriverDataModel> arrListDriverDataModel;
    private UserDocumentModel userDocumentModel;
    private MoreDriverInfo_Interface ImoreDriverInfo_Interface;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void driverInfo(Activity context, MoreDriverInfo_Interface mMoreDriverInfo_Interface,final String strDriverId){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        ImoreDriverInfo_Interface = mMoreDriverInfo_Interface;
        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        driverDataModel=new DriverDataModel();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest moreDriverInfoRequest = new StringRequest(Request.Method.POST, Urlstring.driver_details,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("::vehicle details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", UserLoginDetails.getId());
                params.put("driver_id", strDriverId);

                new PrintClass("params******getParams***" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };
        moreDriverInfoRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(moreDriverInfoRequest);
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("driver_details").optString("user_deleted");
            Msg = jobj_main.optJSONObject("driver_details").optString("message");
            Status= jobj_main.optJSONObject("driver_details").optString("status");

            details=jobj_main.optJSONObject("driver_details").optJSONObject("details");
            arrListDriverDataModel = new ArrayList<DriverDataModel>();

            if(Status.equals(StaticClass.SuccessResult)){
                driverDataModel.setId(details.optString("id"));
             //   driverDataModel.setName(details.optString("name"));
                driverDataModel.setFirst_name(details.optString("first_name"));
                driverDataModel.setLast_name(details.optString("last_name"));
                driverDataModel.setDob(details.optString("dob"));
                driverDataModel.setGender(details.optString("gender"));
                driverDataModel.setAddress(details.optString("address"));
                driverDataModel.setNationality(details.optString("nationality"));
                driverDataModel.setIn_employment_since(details.optString("in_employment_since"));
                driverDataModel.setFathers_name(details.optString("fathers_name"));
                driverDataModel.setContact_no(details.optString("contact_no"));
                driverDataModel.setMarital_status(details.optString("marital_status"));
                driverDataModel.setName_of_spouse(details.optString("name_of_spouse"));
                driverDataModel.setName_of_childrens(details.optString("name_of_childrens"));
                driverDataModel.setRecent_photograph(details.optString("recent_photograph"));
                driverDataModel.setAdditional_info(details.optString("additional_info"));

                arrListDriverDataModel.add(driverDataModel);

                JSONArray jsonArrayDocument = details.optJSONArray("document");
                if(jsonArrayDocument != null){
                    ArrayList<UserDocumentModel> arrlistUserDocumentModel = new ArrayList<UserDocumentModel>();
                    for(int i=0; i<jsonArrayDocument.length(); i++){
                        JSONObject jsonObjectUserDocument = jsonArrayDocument.getJSONObject(i);
                        userDocumentModel = new UserDocumentModel();

                        userDocumentModel.setId(jsonObjectUserDocument.optString("id"));
                        userDocumentModel.setDocument_type(jsonObjectUserDocument.optString("document_type"));
                        userDocumentModel.setDocument_side(jsonObjectUserDocument.optString("document_side"));
                        userDocumentModel.setDocument_file(jsonObjectUserDocument.optString("document_file"));
                        userDocumentModel.setDocument_number(jsonObjectUserDocument.optString("document_number"));
                        userDocumentModel.setDocument_expiry_date(jsonObjectUserDocument.optString("document_expiry_date"));
                        userDocumentModel.setIs_approved(jsonObjectUserDocument.optString("is_approved"));

                        arrlistUserDocumentModel.add(userDocumentModel);
                    }
                    driverDataModel.setArr_UserDocumentModel(arrlistUserDocumentModel);

                }

            }else if (Status.equals(StaticClass.ErrorResult)){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("driver_details").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                ImoreDriverInfo_Interface.MoreDriverInfo(driverDataModel);
            }
        }
    }
}

package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.BookingAvailabilityCheckInterface;
import com.neo.cars.app.Interface.BookingHours_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.BookingHoursModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BookingAvailablityCheckWebService {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private BookingHoursModel bookinghourModel;
    private JSONObject jsonObjectBookingHours;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    private ArrayList<BookingHoursModel> arrlistBookingHours;


    public void bookingAvailablityCheck(Activity context, final String strSelectedDate, final String strPickUpTime){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistBookingHours = new ArrayList<BookingHoursModel>();
        gson = new Gson();
        bookinghourModel = new BookingHoursModel();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest bookingHoursRequest = new StringRequest(Request.Method.POST, Urlstring.booking_availability_checking,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response Booking hours", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", UserLoginDetails.getId());
                params.put("booking_date", strSelectedDate);
                params.put("booking_time", strPickUpTime);

                new PrintClass("booking hours params******getParams***"+params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        bookingHoursRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(bookingHoursRequest);
    }


    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);

            Msg = jobj_main.optJSONObject("booking_availability_checking").optString("message");
            Status = jobj_main.optJSONObject("booking_availability_checking").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("booking_availability_checking").optJSONArray("details");


        } catch (Exception e) {
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

            //on Error Result Msg will be show

//        if (Status.equals(StaticClass.SuccessResult)) {
            ((BookingAvailabilityCheckInterface) mcontext).BookingAvailabilityCheck(Status, Msg);

//        }

    }
}

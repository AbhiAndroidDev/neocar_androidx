package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by parna on 10/5/18.
 */

public class ThankYouActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title,tvTripsTab;
    private CustomButtonTitilliumSemibold btnContinueExploring;
    private SharedPrefUserDetails sharedPref;

    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;
    private BottomView bottomview = new BottomView();

    private CustomTextviewTitilliumWebRegular tvDate,tvDuration,tvStartTime,tvPickUpLoc,tvTotalAmount, tvLocation, tvEndTime;
    private String booking_date = "",start_time ="", end_time = "", duration = "",pickup_location = "",total_amount = "", strVehiclecityName="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);
        sharedPref = new SharedPrefUserDetails(this);
        new AnalyticsClass(ThankYouActivity.this);

        StaticClass.IsPaymentDone = true;

        booking_date=getIntent().getExtras().getString("booking_date");
//        start_time=getIntent().getExtras().getString("start_time");
//        end_time=getIntent().getExtras().getString("end_time");
        start_time = sharedPref.getStartTime();
        end_time = sharedPref.getEndTime();
        Log.d("****start_time*****", start_time);
        duration=getIntent().getExtras().getString("duration");
        pickup_location=getIntent().getExtras().getString("pickup_location");
        total_amount=getIntent().getExtras().getString("net_payable_amount");
        strVehiclecityName = getIntent().getExtras().getString("vehicle_city_name");

        Initialize();
        Listener();
    }

    private void Initialize(){
        context = this;

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);
        tvTripsTab       = findViewById(R.id.tvTripsTab);
        tv_toolbar_title.setText(getResources().getString(R.string.tvDetails));
        rlBackLayout = findViewById(R.id.rlBackLayout);
        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        btnContinueExploring=findViewById(R.id.btnContinueExploring);

        tvDate=findViewById(R.id.tvDate);
        tvDuration=findViewById(R.id.tvDuration);
        tvStartTime=findViewById(R.id.tvStartTime);
        tvEndTime=findViewById(R.id.tvEndTime);
        tvPickUpLoc=findViewById(R.id.tvPickUpLoc);
        tvTotalAmount=findViewById(R.id.tvTotalAmount);
        tvLocation = findViewById(R.id.tvLocation);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date FromDate = null;
        try {
            FromDate = df.parse(booking_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = df.format(FromDate);

        tvDate.setText(formattedDate);
        tvDuration.setText(duration+" Hours");
        tvStartTime.setText(start_time);
        tvEndTime.setText(end_time);
        tvPickUpLoc.setText(pickup_location);
        tvTotalAmount.setText(getResources().getString(R.string.Rs) +" "+total_amount);
        tvLocation.setText(strVehiclecityName);

        String txt = "View complete details in the "+"<b>"+"TRIPS"+"</b>"+" tab";
        tvTripsTab.setText(Html.fromHtml(txt));

        bottomview.BottomView(ThankYouActivity.this,StaticClass.Menu_Explore);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        btnContinueExploring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
//                startActivity(new Intent(ThankYouActivity.this, FilterOnBookActivity.class));
                startActivity(new Intent(ThankYouActivity.this, SearchActivity.class));
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(ThankYouActivity.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // StaticClass.BottomExplore = false;
        if(StaticClass.BottomExplore){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) ThankYouActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

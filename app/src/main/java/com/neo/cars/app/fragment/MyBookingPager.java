package com.neo.cars.app.fragment;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * Created by parna on 8/3/18.
 */

public class MyBookingPager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    private int tabCount;


    //Constructor to the class
    public MyBookingPager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                OngoingBookingFragment tab1 = new OngoingBookingFragment();
                return tab1;

            case 1:
                UpcomingBookingFragment tab2 = new UpcomingBookingFragment();
                return tab2;

            case 2:
                PastBookingFragment tab3 = new PastBookingFragment();
                return tab3;

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }

}

package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.MobileOtpVerificationActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.VolleyParser.CustomMultiPartRequest;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 16/3/18.
 */

public class Registration_Webservice {
    private Activity mcontext;
    private String Status = "0", Msg = "", unique_id="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private JSONObject details;

    public void Registration(Activity context, String strRegFirstName, String strRegLastName,
                             String strRegEmail,
                             String strRegMob, File reply_file) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        unique_id = Settings.Secure.getString(mcontext.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("d", "Device id::"+unique_id);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        MultipartEntity entity = new MultipartEntity();
        try {
            //Image part here in file form
            if (reply_file != null) {
                entity.addPart("profile_pic", new FileBody(reply_file));
            }

            entity.addPart("first_name", new StringBody(strRegFirstName));
            entity.addPart("last_name", new StringBody(strRegLastName));

            entity.addPart("email", new StringBody(strRegEmail));
            entity.addPart("mobile_no", new StringBody(strRegMob));

            //entity.addPart("password", new StringBody(strRegPass));
           // entity.addPart("password_confirmation", new StringBody(strRegConfPass));
            //entity.addPart("dob", new StringBody(strRegDOB));
            entity.addPart("device_type", new StringBody("A"));
            entity.addPart("device_id", new StringBody(unique_id));

        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestQueue rq = Volley.newRequestQueue(mcontext);
        CustomMultiPartRequest customMultiPartRequest = new CustomMultiPartRequest(Urlstring.registration,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        Log.d("Error.Response", error.toString());
                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                Log.d("Response", response);
                Apiparsedata(response);
            }
        }, entity) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("xsrf-token",sharedPref.getKEY_Access_Token());
                System.out.println("params******getHeaders***" + params);

                return params;
            }
        };
        customMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        customMultiPartRequest.setShouldCache(false);
        // add the request object to the queue to be executed
        rq.add(customMultiPartRequest);
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);

            Msg = jobj_main.optJSONObject("registration").optString("message");
            Status= jobj_main.optJSONObject("registration").optString("status");

            details=jobj_main.optJSONObject("registration").optJSONObject("details");

            if (Status.equals(StaticClass.SuccessResult)) {

                UserLoginDetails.setId(details.optString("id"));
                sharedPref.setUserId(details.optString("id"));
                UserLoginDetails.setEmail(details.optString("email"));
                UserLoginDetails.setMobile(details.optString("mobile_no"));
                UserLoginDetails.setIs_active(details.optString("is_active"));
                UserLoginDetails.setProfile_name(details.optString("profile_name"));
                UserLoginDetails.setUser_type(details.optString("user_type"));
                sharedPref.setUserType(details.optString("user_type"));

                UserLoginDetails.setProfile_pic(details.optString("profile_pic"));
                UserLoginDetails.setReg_type(details.optString("reg_type"));
                UserLoginDetails.setReferral_code(details.optString("referral_code"));
                UserLoginDetails.setIs_notification_active(details.optString("is_notification_active"));
                UserLoginDetails.setIs_profile_complete(details.optString("is_profile_complete"));

                String strParentDetails = gson.toJson(UserLoginDetails);
                sharedPref.putObjectToPreferenceUserDetails(strParentDetails);

            }
            else if (Status.equals(StaticClass.ErrorResult)){
                details = jobj_main.optJSONObject("registration").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    new CustomToast(mcontext, Msg);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {

            mcontext.startActivity(new Intent(mcontext, MobileOtpVerificationActivity.class));

        } else {

        }
    }
}

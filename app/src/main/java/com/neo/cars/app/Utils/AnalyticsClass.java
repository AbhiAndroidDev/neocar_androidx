package com.neo.cars.app.Utils;

import android.app.Activity;
import android.os.Bundle;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
//import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.neo.cars.app.app.App;

/**
 * Created by joydeep on 18/5/17.
 */

public class AnalyticsClass {

    private FirebaseAnalytics mFirebaseAnalytics;

    public AnalyticsClass(Activity context){

//        Tracker t = ((App) context.getApplication()).getTracker(
//                App.TrackerName.APP_TRACKER);
//        // Set screen name.
//        t.setScreenName(context.getClass().getName());
//        // Send a screen view.
//        t.send(new HitBuilders.ScreenViewBuilder().build());
//
//        // Clear the screen name field when we're done.
//        t.setScreenName(null);


        //Added later to firebase Analytics

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        //Sets whether analytics collection is enabled for this app on this device.
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

        //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
        mFirebaseAnalytics.setMinimumSessionDuration(1000);

        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        mFirebaseAnalytics.setSessionTimeoutDuration(5000);
//
//        Bundle bundle = new Bundle();
//        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
//        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
//        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);



    }

}

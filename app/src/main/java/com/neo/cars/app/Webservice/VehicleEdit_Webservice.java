package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.VolleyParser.CustomMultiPartRequest;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 30/4/18.
 */

public class VehicleEdit_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserDocumentModel userDocumentModel;
    private VehicleTypeModel vehicleTypeModel;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    JSONObject details;

    public void VehicleEdit (Activity context, String strvehicleId, String strVehicleTypeId, String strVehicleBrandId, String strVehicleModelId,
                             String strAc_Available, String strYrOfManufacture, String strTotalKm,  String strStateId, String strCityId, String strMaxPassenger,
                             String strMaxLuggage, String strHourlyRate, String strOvertimeAvailableCheck, String strOvertimeHourlyRate,
                             String strSpecialInfo, String strLicenseNo, String strAddress, String strOtherLoc,  File fileTaxTokenImage,
                             File filePcuPaperImage, File fileGalleryOne, File fileGalleryTwo, File fileGalleryThree,
                             File fileRegionalTransportPermit, File fileRegistrationCertificateFront, File fileRegistrationCertificateBack, File fileFitnessCertificate, File fileInsuranceCertificate,
                             String gallery_image_id_1, String gallery_image_id_2,
                             String gallery_image_id_3, String strSetGallerySelected, String strDefaultImageId,
                             String toll_parking_charge, String strNightChargesCheck, String strNightChargesRate){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails = new UserLoginDetailsModel();
        vehicleTypeModel = new VehicleTypeModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();


        MultipartEntity entity = new MultipartEntity();
        try{

            entity.addPart("user_id", new StringBody(UserLoginDetails.getId()));

            entity.addPart("vehicle_id", new StringBody(strvehicleId));
            Log.d("d", "Vehicle id: "+strvehicleId);

            entity.addPart("vehicle_type_id", new StringBody(strVehicleTypeId));
            Log.d("d", "vehicle_type_id: "+strVehicleTypeId);

            entity.addPart("brand", new StringBody(strVehicleBrandId));
            Log.d("d", "brand: "+strVehicleBrandId);

            entity.addPart("model", new StringBody(strVehicleModelId));
            Log.d("d", "model: "+strVehicleModelId);

            entity.addPart("ac_available", new StringBody(strAc_Available));

            entity.addPart("year_of_manufacture", new StringBody(strYrOfManufacture));
            Log.d("d", "year_of_manufacture: "+strYrOfManufacture);

            entity.addPart("total_no_km", new StringBody(strTotalKm));
            Log.d("d", "total_no_km: "+strTotalKm);

            entity.addPart("state", new StringBody(strStateId));
            Log.d("d", "state id: "+strStateId);

            entity.addPart("city", new StringBody(strCityId));
            Log.d("d", "city id: "+strCityId);

            entity.addPart("max_passenger", new StringBody(strMaxPassenger));
            Log.d("d", "max_passenger: "+strMaxPassenger);

            entity.addPart("max_luggage", new StringBody(strMaxLuggage));
            Log.d("d", "max_luggage: "+strMaxLuggage);

            entity.addPart("hourly_rate", new StringBody(strHourlyRate));
            Log.d("d", "hourly_rate: "+strHourlyRate);

            entity.addPart("overtime_available", new StringBody(strOvertimeAvailableCheck));
            Log.d("d", "overtime_available: "+strOvertimeAvailableCheck);


            entity.addPart("toll_parking_charge", new StringBody(toll_parking_charge));
            Log.d("d", "toll_parking_charge : " + toll_parking_charge);

            entity.addPart("night_charge_available", new StringBody(strNightChargesCheck));
            Log.d("d", "night_charge_available : " + strNightChargesCheck);

            if (!TextUtils.isEmpty(strNightChargesRate)) {

                entity.addPart("night_rate", new StringBody(strNightChargesRate));
                Log.d("d", "night_rate : " + strNightChargesRate);
            }


            if("Y".equals(strOvertimeAvailableCheck)){
                entity.addPart("hourly_overtime_rate", new StringBody(strOvertimeHourlyRate));
                Log.d("d", "hourly_rate: "+strOvertimeHourlyRate);
            }

            if (strSpecialInfo != null){
                entity.addPart("special_info", new StringBody(strSpecialInfo));
                Log.d("d", "Special info: "+strSpecialInfo);
            }

            entity.addPart("license_plate_no", new StringBody(strLicenseNo));
            Log.d("d", "license_plate_no : "+strLicenseNo);

            if(strAddress != null){
                entity.addPart("pickup_location", new StringBody(strAddress));
                Log.d("d", "pickup_location : "+strAddress);
            }

            entity.addPart("other_pickup_location", new StringBody(strOtherLoc));
            Log.d("d", "other_pickup_location : "+strOtherLoc);

            if(fileTaxTokenImage != null){
                entity.addPart("tax_token_image", new FileBody(fileTaxTokenImage));
                Log.d("d", "tax_token_image : "+fileTaxTokenImage);
            }

            if(filePcuPaperImage != null){
                entity.addPart("pcu_paper_image", new FileBody(filePcuPaperImage));
                Log.d("d", "pcu_paper_image : "+filePcuPaperImage);
            }


            if(fileRegionalTransportPermit != null){
                entity.addPart("valid_permit", new FileBody(fileRegionalTransportPermit));
                Log.d("d", "valid_permit : "+fileRegionalTransportPermit);
            }


            if(fileRegistrationCertificateFront != null){
                entity.addPart("registration_certificate", new FileBody(fileRegistrationCertificateFront));
                Log.d("d", "registration_certificate : "+fileRegistrationCertificateFront);
            }

            if(fileRegistrationCertificateBack != null){
                entity.addPart("registration_certificate_back", new FileBody(fileRegistrationCertificateBack));
                Log.d("d", "registration_certificate : "+fileRegistrationCertificateBack);
            }

            if(fileFitnessCertificate != null){
                entity.addPart("fitness_certificate", new FileBody(fileFitnessCertificate));
                Log.d("d", "fitness_certificate : "+fileFitnessCertificate);
            }


            if(fileInsuranceCertificate != null){
                entity.addPart("insurance_certificate", new FileBody(fileInsuranceCertificate));
                Log.d("d", "insurance_certificate : "+fileInsuranceCertificate);
            }




            entity.addPart("user_id", new StringBody(UserLoginDetails.getId()));
            Log.d("d", "user id : "+UserLoginDetails.getId());

            if(fileGalleryOne != null){
                entity.addPart("gallery_image_1", new FileBody(fileGalleryOne));
                Log.d("d", "gallery_image_1 : " + fileGalleryOne);
            }

            if(fileGalleryTwo != null){
                entity.addPart("gallery_image_2", new FileBody(fileGalleryTwo));
                Log.d("d", "gallery_image_2 : " + fileGalleryTwo);
            }

            if(fileGalleryThree != null){
                entity.addPart("gallery_image_3", new FileBody(fileGalleryThree));
                Log.d("d", "gallery_image_3 : " + fileGalleryThree);
            }

            if (gallery_image_id_1 != null){
                entity.addPart("gallery_image_id_1", new StringBody(gallery_image_id_1));
                Log.d("d", "gallery_image_id_1 : " + fileGalleryThree);
            }

            if (gallery_image_id_2 != null){
                entity.addPart("gallery_image_id_2", new StringBody(gallery_image_id_2));
                Log.d("d", "gallery_image_id_2 : " + fileGalleryThree);
            }

            if (gallery_image_id_3 != null){
                entity.addPart("gallery_image_id_3", new StringBody(gallery_image_id_3));
                Log.d("d", "gallery_image_id_3 : " + fileGalleryThree);
            }

            if( strSetGallerySelected != null){
                entity.addPart("default_image_position", new StringBody(strSetGallerySelected));
                Log.d("d", "default_image_position : " + strSetGallerySelected);
            }

            if(strSetGallerySelected != null){
                entity.addPart("default_image_id", new StringBody(strDefaultImageId));
                Log.d("d", "default_image_id : " + strDefaultImageId);

            }

        }catch (Exception e){
            e.printStackTrace();
        }


        RequestQueue rq = Volley.newRequestQueue(mcontext);
        CustomMultiPartRequest customMultiPartRequest = new CustomMultiPartRequest(Urlstring.vehicle_edit,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        Log.d("Error.Response", error.toString());

                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                //Log.d("Response edit profile::", response);
                System.out.println("**response Edit Profile**"+response);
                Apiparsedata(response);

            }
        }, entity){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("xsrf-token",sharedPref.getKEY_Access_Token());
                System.out.println("params******getHeaders***" + params);

                return params;
            }
        };

        customMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        customMultiPartRequest.setShouldCache(false);
        // add the request object to the queue to be executed
        rq.add(customMultiPartRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try{

            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_edit").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_edit").optString("message");
            Status= jobj_main.optJSONObject("vehicle_edit").optString("status");

            details=null;


        }catch (Exception e){

        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if(Status.equals(StaticClass.SuccessResult)){

//                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
//                        Msg ,
//                        mcontext.getResources().getString(R.string.btn_ok),
//                        "");
//                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        alertDialogYESNO.dismiss();
                        mcontext.finish();
//
//                    }
//                });
//
//                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialogYESNO.dismiss();
//                    }
//                });
//
//                alertDialogYESNO.show();



            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("vehicle_edit").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    try {
                        Msg = jarrError.getString(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                new CustomToast(mcontext, Msg);

            }
        }
    }
}

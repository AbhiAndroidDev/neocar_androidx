package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.VehicleModel_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 2/4/18.
 */

public class VehicleModel_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private SharedPrefUserDetails sharedPref;
    private VehicleTypeModel vehicleMakeModel;
    ArrayList<VehicleTypeModel> arrlistVehicleModel;
    VehicleModel_Interface Ivehiclemodel_Interface;
    JSONObject jsonObjectVehiclemodel;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void vehicleModelWebservice(Activity context, VehicleModel_Interface mvehicleModel_Interface, final String strVehicleBrandId){
        mcontext = context;
        Ivehiclemodel_Interface = mvehicleModel_Interface;

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistVehicleModel = new ArrayList<>();
        gson = new Gson();
        vehicleMakeModel = new VehicleTypeModel();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest vehicleModelRequest = new StringRequest( Request.Method.POST, Urlstring.vehicle_model,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if(strVehicleBrandId != null){
                    params.put("make_id",strVehicleBrandId);
                }
                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("model params******getParams***"+params);
                return params;
            }



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        vehicleModelRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(vehicleModelRequest);

    }

    private void showProgressDialog() {
       /* try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void hideProgressDialog() {
       /* try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_model").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_model").optString("message");
            Status = jobj_main.optJSONObject("vehicle_model").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("vehicle_model").optJSONArray("details");
            for (int i = 0; i < jsonArrayDetails.length(); i++){
                jsonObjectVehiclemodel = jsonArrayDetails.optJSONObject(i);

                VehicleTypeModel vehicleTypeModel = new VehicleTypeModel();
                vehicleTypeModel.setId(jsonObjectVehiclemodel.optString("id"));
                vehicleTypeModel.setMake_id(jsonObjectVehiclemodel.optString("make_id"));
              //  vehicleTypeModel.setCode(jsonObjectVehiclemodel.optString("code"));
                vehicleTypeModel.setTitle(jsonObjectVehiclemodel.optString("title"));
                vehicleTypeModel.setIs_active(jsonObjectVehiclemodel.optString("is_active"));
                vehicleTypeModel.setType_name(jsonObjectVehiclemodel.optString("type_name"));
                vehicleTypeModel.setVehicle_type_id(jsonObjectVehiclemodel.optString("vehicle_type_id"));
                vehicleTypeModel.setSitting_capacity(jsonObjectVehiclemodel.optString("sitting_capacity"));
                vehicleTypeModel.setLuggae_capacity(jsonObjectVehiclemodel.optString("luggae_capacity"));

                //fetching AC price details
                JSONObject jsonObjectAC = jsonObjectVehiclemodel.optJSONObject("ac_price_details");
                vehicleTypeModel.setAc_ac_facility(jsonObjectAC.optString("ac_facility"));
                vehicleTypeModel.setAc_hourly_min_rate(jsonObjectAC.optString("hourly_min_rate"));
                vehicleTypeModel.setAc_hourly_max_rate(jsonObjectAC.optString("hourly_max_rate"));
                vehicleTypeModel.setAc_hourly_overtime_min_rate(jsonObjectAC.optString("hourly_overtime_min_rate"));
                vehicleTypeModel.setAc_hourly_overtime_max_rate(jsonObjectAC.optString("hourly_overtime_max_rate"));

                vehicleTypeModel.setAc_min_night_rate(jsonObjectAC.optString("min_night_rate"));
                vehicleTypeModel.setAc_max_night_rate(jsonObjectAC.optString("max_night_rate"));


                //fetching non-AC price details
                JSONObject jsonObjectNonAC = jsonObjectVehiclemodel.optJSONObject("non_ac_price_details");
                vehicleTypeModel.setNonac_ac_facility(jsonObjectNonAC.optString("non_ac_facility"));
                vehicleTypeModel.setNonac_hourly_min_rate(jsonObjectNonAC.optString("hourly_min_rate"));
                vehicleTypeModel.setNonac_hourly_max_rate(jsonObjectNonAC.optString("hourly_max_rate"));
                vehicleTypeModel.setNonac_hourly_overtime_min_rate(jsonObjectNonAC.optString("hourly_overtime_min_rate"));
                vehicleTypeModel.setNonac_hourly_overtime_max_rate(jsonObjectNonAC.optString("hourly_overtime_max_rate"));

                vehicleTypeModel.setNonac_min_night_rate(jsonObjectNonAC.optString("min_night_rate"));
                vehicleTypeModel.setNonac_max_night_rate(jsonObjectNonAC.optString("max_night_rate"));


                arrlistVehicleModel.add(vehicleTypeModel);
            }
            Log.d("Vehicle Model size:", "" + arrlistVehicleModel.size());
            Log.e("Vehicle Model", "" + arrlistVehicleModel);

        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                Ivehiclemodel_Interface.VehicleModelList(arrlistVehicleModel);
            }

        }

    }


}

package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleUnAvailabilityModel;

import java.util.ArrayList;

public interface VehicleUnAvailability_Interface {

     void unAvailableDate(ArrayList<VehicleUnAvailabilityModel> arrListVehicleUnAvailabilityModel);
}

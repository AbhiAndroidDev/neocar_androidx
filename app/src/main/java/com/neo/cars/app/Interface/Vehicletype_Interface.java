package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.StateListModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;

import java.util.ArrayList;

/**
 * Created by parna on 29/3/18.
 */

public interface Vehicletype_Interface {

    void VehicleTypeList(ArrayList<VehicleTypeModel> arrlistVehicleType);

}

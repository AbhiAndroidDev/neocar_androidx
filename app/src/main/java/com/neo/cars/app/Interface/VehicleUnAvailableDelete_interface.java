package com.neo.cars.app.Interface;

public interface VehicleUnAvailableDelete_interface {

    void vehicleUnAvailableDelete(String status, String msg);
}

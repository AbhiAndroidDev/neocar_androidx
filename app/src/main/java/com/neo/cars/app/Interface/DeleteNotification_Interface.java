package com.neo.cars.app.Interface;

/**
 * Created by joydeep on 23/7/18.
 */

public interface DeleteNotification_Interface {
    public void onDeleteNotification();
}

package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.OvertimeAvailabilityModel;

import java.util.ArrayList;

/**
 * Created by parna on 11/5/18.
 */

public interface OvertimeAvailability_Interface {

    public void onOvertimeAvailability(ArrayList<OvertimeAvailabilityModel> overtimeAvailabilityModel);
}

package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.BookingHours_Interface;
import com.neo.cars.app.SetGet.BookingHoursModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.OvertimeAvailability_Webservice;
import com.neo.cars.app.Webservice.OvertimeBookingHours_Webservice;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;

/**
 * Created by parna on 19/3/18.
 */

public class RequestOvertimeActivity extends AppCompatActivity implements BookingHours_Interface{

    private CustomTitilliumTextViewSemiBold tvChooseHours;
    private CustomTextviewTitilliumWebRegular tvSelectHrs;
    private CustomButtonTitilliumSemibold btnCheckAvailability;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;
    private ConnectionDetector cd;
    private Context context = RequestOvertimeActivity.this;
    private FrameLayout flSelectHours;
    private NoDefaultSpinner spnrSelectedHours;
    private ArrayList<String> listOfBookingHour;
    private ArrayAdapter<String> arrayAdapterBookingHour;
    private String strSelectedBookingHourLabel="", strSelectedBookingHourValue="", strBookingId="";
    private Bundle bundle;

    private int transitionflag = StaticClass.transitionflagNext;
    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_overtime);

        new AnalyticsClass(RequestOvertimeActivity.this);
        bundle = getIntent().getExtras();
        if(bundle != null){
            strBookingId = bundle.getString("bookingId");
        }

        Initialize();
        Listener();

        //Booking hours duration webservice
        if(cd.isConnectingToInternet()){
            new OvertimeBookingHours_Webservice().bookinghoursWebservice(RequestOvertimeActivity.this);

        }else {
            Intent i = new Intent(RequestOvertimeActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    private void Initialize(){
        context = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvHeaderRequestOvertime));

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        tvChooseHours = findViewById(R.id.tvChooseHours);
        btnCheckAvailability = findViewById(R.id.btnCheckAvailability);
        flSelectHours = findViewById(R.id.flSelectHours);
        spnrSelectedHours = findViewById(R.id.spnrSelectedHours);

        bottomview.BottomView(RequestOvertimeActivity.this,StaticClass.Menu_profile);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void  Listener(){

        btnCheckAvailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagNext;

                if (strSelectedBookingHourLabel != null && !strSelectedBookingHourLabel.equals("") &&
                        strSelectedBookingHourValue != null && !strSelectedBookingHourValue.equals("")) {

                    if (cd.isConnectingToInternet()) {
                        new OvertimeAvailability_Webservice().overtimeAvailability(RequestOvertimeActivity.this, strBookingId, strSelectedBookingHourValue, strSelectedBookingHourLabel);

                    } else {
                        Intent i = new Intent(RequestOvertimeActivity.this, NetworkNotAvailable.class);
                        startActivity(i);
                    }
                }else {
                    new CustomToast(RequestOvertimeActivity.this, "Select overtime hours");
                }
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(RequestOvertimeActivity.this, transitionflag);
    }



    @Override
    public void BookingHoursList(final ArrayList<BookingHoursModel> arrlistBookingHours) {
        if (arrlistBookingHours != null){
            listOfBookingHour = new ArrayList<String>();
            if(arrlistBookingHours.size() > 0){
                for (int x=0; x<arrlistBookingHours.size(); x++){
                    listOfBookingHour.add(arrlistBookingHours.get(x).getLabel());
                }
                Log.d("d", "Array list vehicle booking hour size::"+arrlistBookingHours.size());
                arrayAdapterBookingHour = new ArrayAdapter<String>(context, R.layout.countryitem, listOfBookingHour);
                arrayAdapterBookingHour.setDropDownViewResource(R.layout.simpledropdownitem);
                spnrSelectedHours.setPrompt(getResources().getString(R.string.tvSelectYourHoursSpinner));
                spnrSelectedHours.setAdapter(arrayAdapterBookingHour);

                spnrSelectedHours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        strSelectedBookingHourLabel = arrlistBookingHours.get(position).getLabel();
                        strSelectedBookingHourValue = arrlistBookingHours.get(position).getValue();
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });
            }
        }else{
            new CustomToast(this, getResources().getString(R.string.tvDurationNtFound));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }


        if(StaticClass.continueExploring){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) RequestOvertimeActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

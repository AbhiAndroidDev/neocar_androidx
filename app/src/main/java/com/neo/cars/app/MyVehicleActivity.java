package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.material.tabs.TabLayout;
import com.neo.cars.app.Interface.UserEmailMobileVerifyInterface;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.UserMobileEmailVerifyWebService;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.fragment.MyVehiclePager;

/**
 * Created by parna on 27/2/18.
 */

public class MyVehicleActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, UserEmailMobileVerifyInterface{

    private TabLayout tabs;
    private ViewPager viewpager;
    private Toolbar toolbar;
    private Activity activity;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private CustomTextviewTitilliumWebRegular rightTopBarText;
    private RelativeLayout rlAddLayout;
    private int transitionflag = StaticClass.transitionflagNext;
    private RelativeLayout rlBackLayout;
    private ConnectionDetector cd;

    private BottomView bottomview = new BottomView();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_vehicle);

        new AnalyticsClass(MyVehicleActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvMyVehicleHeader));

        cd = new ConnectionDetector(this);
        activity = MyVehicleActivity.this;

        rlBackLayout = findViewById(R.id.rlBackLayout);
        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);

        rlAddLayout = findViewById(R.id.rlAddLayout);
        rlAddLayout.setVisibility(View.VISIBLE);
        rightTopBarText= findViewById(R.id.rightTopBarText);
        rightTopBarText.setVisibility(View.GONE);

        viewpager = findViewById(R.id.viewpager);

        tabs = findViewById(R.id.tabs);
        //Adding the tabs using addTab() method
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.tvVehicleList)));
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.tvMyVehicleBooking)));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        //Creating our pager adapter
        final MyVehiclePager adapter = new MyVehiclePager(getSupportFragmentManager(), tabs.getTabCount(), rightTopBarText);

        //Adding adapter to pager
        viewpager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        tabs.setOnTabSelectedListener(this);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        bottomview.BottomView(MyVehicleActivity.this,StaticClass.Menu_profile);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        rlAddLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(StaticClass.canAddVehicle.equalsIgnoreCase("Y")){

                    if(cd.isConnectingToInternet()){
                        new UserMobileEmailVerifyWebService().UserMobileEmailVerification(MyVehicleActivity.this);

                    }else{
                        new CustomToast(activity, getResources().getString(R.string.Network_not_availabl));
                    }

                }else{
                    new CustomToast(MyVehicleActivity.this, getResources().getString(R.string.strCanAddVehicle));

                }
            }
        });
    }



    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
        if (tab.getPosition() == 0){
            rlAddLayout.setVisibility(View.VISIBLE);
            rightTopBarText.setVisibility(View.GONE);
        }else {
            rightTopBarText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(MyVehicleActivity.this, transitionflag);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(StaticClass.BottomProfile){
            finish();
        }

        if(StaticClass.MyVehicleAddUpdate) {
            StaticClass.MyVehicleAddUpdate=false;
            this.startActivity(new Intent(MyVehicleActivity.this, MyVehicleActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            finish();
        }

        if(StaticClass.MyVehicleIsDelete){
            StaticClass.MyVehicleIsDelete = false;
            this.startActivity(new Intent(MyVehicleActivity.this, MyVehicleActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            finish();
        }

        if (StaticClass.MyBookingListFlag){
            StaticClass.MyBookingListFlag = false;
            this.startActivity(new Intent(MyVehicleActivity.this, MyVehicleActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            finish();

        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }


    }



    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) MyVehicleActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    public void userEmailMobileVerify(String status) {
        if (status.equals(StaticClass.SuccessResult)){
            transitionflag=StaticClass.transitionflagNext;
            Intent addvehicledriverintent = new Intent(MyVehicleActivity.this, AddVehicleDriverInfoActivity.class);
            startActivity(addvehicledriverintent);
        }else if (status.equals(StaticClass.ErrorResult)){

        }
    }
}
package com.neo.cars.app.Utils;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.neo.cars.app.HomeProfileActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;

/**
 * Created by parna on 18/9/18.
 */

public class CustomToastRating {

    public CustomToastRating(final Context mcontext, String msg) {

        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                msg,
                mcontext.getResources().getString(R.string.btn_ok),
                "");
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                mcontext.startActivity(new Intent(mcontext, HomeProfileActivity.class));
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }
}

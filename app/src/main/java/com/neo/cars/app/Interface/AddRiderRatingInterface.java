package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.AddRiderRatingModel;

import java.util.ArrayList;

/**
 * Created by parna on 13/11/18.
 */

public interface AddRiderRatingInterface {

    public void onAddRiderRating(ArrayList<AddRiderRatingModel> arrListaddRatingModel);
}

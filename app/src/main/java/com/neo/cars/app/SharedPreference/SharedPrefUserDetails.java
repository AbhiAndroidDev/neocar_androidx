package com.neo.cars.app.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;

import com.neo.cars.app.Utils.StaticClass;


/**
 * Created by kamail on 16/11/16.
 */
public class SharedPrefUserDetails {

    private SharedPreferences userPref;
    private SharedPreferences.Editor userEditor;

    private Context context;

    int PRIVATE_MODE = 0;

    private String PREF_NAME = "user_details";

    static String isLoggedIn = "isLoggedIn";
    static String isNotificationOn = "isNotificationOn";
    static String isNotificationOnCompany = "isNotificationOnCompany";
    static String KEY_User_DETAILS = "KEY_User_DETAILS";
    static String KEY_Access_Token = "KEY_Access_Token";
    static String KEY_Bottom_View = "KEY_Bottom_View";
    static String KEY_Bottom_View_Company = "KEY_Bottom_View_Company";
    static String CURRENT_LAT = "lat";
    static String CURRENT_LONG = "long";
    static String CURRENT_ADDRESS = "address";
    static String START_TIME = "start_time";
    static String END_TIME = "END_TIME";

    public String  getMobile(){
        return userPref.getString(StaticClass.MOBILE, null);
    }

    public void setMobile(String mobile){
        userEditor.putString(StaticClass.MOBILE, mobile);
        userEditor.commit();
    }

    public String  getEmail(){
        return userPref.getString(StaticClass.EMAIL, null);
    }

    public void setEmail(String email){
        userEditor.putString(StaticClass.EMAIL, email);
        userEditor.commit();
    }


    public static String getStartTime() {
        return START_TIME;
    }

    public static void setStartTime(String startTime) {
        START_TIME = startTime;
    }


    public static String getEndTime() {
        return END_TIME;
    }

    public static void setEndTime(String endTime) {
        END_TIME = endTime;
    }

    public void setUserType(String value){
        userEditor.putString(StaticClass.USERTYPE, value);
        userEditor.commit();
    }

    public String getUserType(){

        return userPref.getString(StaticClass.USERTYPE, null);
    }

    public void setUserId(String userId){
        userEditor.putString(StaticClass.USERID, userId);
        userEditor.commit();
    }

    public String getUserid(){
        return userPref.getString(StaticClass.USERID, null);
    }


    // Constructor
    public SharedPrefUserDetails(Context context) {
        this.context = context;
        userPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        userEditor = userPref.edit();
    }

    public void putObjectToPreferenceUserDetails(String value) {
        userEditor.putString(KEY_User_DETAILS, value);
        userEditor.commit();
    }

    public String getObjectFromPreferenceUserDetails() {
        return userPref.getString(KEY_User_DETAILS, null);
    }


    public void setUserloginStatus() {
        userEditor.putBoolean(isLoggedIn, true);

        userEditor.commit();
    }

    // Get login status
    public boolean getLoginStatus() {
        return userPref.getBoolean(isLoggedIn, false);
    }


    public void putKEY_Access_Token(String value) {
        userEditor.putString(KEY_Access_Token, value);
        userEditor.commit();
    }

    public String getKEY_Access_Token() {
        return userPref.getString(KEY_Access_Token, "");
    }


    public void putBottomView(String value) {
        userEditor.putString(KEY_Bottom_View, value);
        userEditor.commit();
    }

    public String getBottomView() {
        return userPref.getString(KEY_Bottom_View, StaticClass.Menu_profile);
    }

    public void putBottomViewCompany(String value) {
        userEditor.putString(KEY_Bottom_View_Company, value);
        userEditor.commit();
    }

    public String getBottomViewCompany() {
        return userPref.getString(KEY_Bottom_View_Company, StaticClass.Menu_profile_company);
    }

    // Set Clear
    public void setClear() {
        userEditor.clear();
        userEditor.commit();
    }


    public void setNewNotificationStatus(boolean isNotification) {
        userEditor.putBoolean(isNotificationOn, isNotification);
        userEditor.commit();
    }

    public boolean getNewNotificationStatus() {
        return userPref.getBoolean(isNotificationOn, false);
    }

    public void setNewNotificationStatusCompany(boolean isNotification) {
        userEditor.putBoolean(isNotificationOnCompany, isNotification);
        userEditor.commit();
    }

    public boolean getNewNotificationStatusCompany() {
        return userPref.getBoolean(isNotificationOnCompany, false);
    }


    public void setCurrentLat(String value) {
        userEditor.putString(CURRENT_LAT, value);
        userEditor.commit();
    }

    public String getCurrentLat() {
        return userPref.getString(CURRENT_LAT, "");
    }

    public void setCurrentLong(String value) {
        userEditor.putString(CURRENT_LONG, value);
        userEditor.commit();
    }

    public String getCurrentLong() {
        return userPref.getString(CURRENT_LONG, "");
    }

    public void setCurrentAddress(String value) {
        userEditor.putString(CURRENT_ADDRESS, value);
        userEditor.commit();
    }

    public String getCurrentAddress() {
        return userPref.getString(CURRENT_ADDRESS, "");
    }



}

package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.CallBackOnDeleteUser;
import com.neo.cars.app.Interface.DeleteUserInterface;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.DeleteUser_Webservice;
import com.neo.cars.app.Webservice.LogoutUser_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

public class CompanySettingsActivity extends AppCompatActivity implements CallBackButtonClick, CallBackOnDeleteUser, DeleteUserInterface {

    private Toolbar toolbar;
    private ImageButton ibNavigateMenu;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile,
            tvNotificationSettings, tvTermsAndCond, tvPrivacyPolicy, tvLogout, tvDeleteUser;
    private RelativeLayout rlBackLayout;
    private ConnectionDetector cd;

    private int transitionflag = StaticClass.transitionflagNext;
    private SharedPrefUserDetails sharedPref;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_settings);

        new AnalyticsClass(CompanySettingsActivity.this);
        cd = new ConnectionDetector(this);
        Initialize();
        Listener();
    }

    private void Initialize(){
        sharedPref = new SharedPrefUserDetails(CompanySettingsActivity.this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvSettings));
        rlBackLayout = findViewById(R.id.rlBackLayout);

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        tvTermsAndCond = findViewById(R.id.tvTermsAndCond);
        tvPrivacyPolicy = findViewById(R.id.tvPrivacyPolicy);
        tvLogout = findViewById(R.id.tvLogout);
        tvNotificationSettings = findViewById(R.id.tvNotificationSettings);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        tvTermsAndCond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent termsIntent = new Intent(CompanySettingsActivity.this, CompanyCmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);

            }
        });

        tvPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent privacyIntent = new Intent(CompanySettingsActivity.this, CmsPageActivity.class);
                privacyIntent.putExtra("cms", StaticClass.PrivacyPolicy);

                startActivity(privacyIntent);

            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(CompanySettingsActivity.this,
                        CompanySettingsActivity.this,
                        CompanySettingsActivity.this.getResources().getString(R.string.DoyouWantToLogout),
                        CompanySettingsActivity.this.getResources().getString(R.string.yes),
                        CompanySettingsActivity.this.getResources().getString(R.string.no));
            }
        });

//        tvDeleteUser.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                BottomSheetDialogDeleteUser bsd = new BottomSheetDialogDeleteUser(CompanySettingsActivity.this,
//                        CompanySettingsActivity.this,
//                        CompanySettingsActivity.this.getResources().getString(R.string.deleteUserText),
//                        CompanySettingsActivity.this.getResources().getString(R.string.yes),
//                        CompanySettingsActivity.this.getResources().getString(R.string.no));
//            }
//        });

//        tvPaymentInfo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                // new CustomToast(SettingsActivity.this, getResources().getString(R.string.tvYetTobImplemented));
//
//                Intent paymentinfoIntent = new Intent(SettingsActivity.this, PaymentInformation.class);
//                startActivity(paymentinfoIntent);
//
//
//            }
//        });
//
//        tvEnlistVehicle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent enlistVehicleIntent = new Intent(SettingsActivity.this, EnlistVehicleActivity.class);
//                startActivity(enlistVehicleIntent);
//
//            }
//        });



        tvNotificationSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSettings=new Intent(CompanySettingsActivity.this, CompanyPushNotificationActivity.class);
                startActivity(intentSettings);

            }
        });
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanySettingsActivity.this, transitionflag);
    }


    @Override
    public void onButtonClick(String strButtonText) {
        if (strButtonText.equals(CompanySettingsActivity.this.getResources().getString(R.string.yes))) {

            if(cd.isConnectingToInternet()){
                new LogoutUser_Webservice().logoutUser(CompanySettingsActivity.this);

            }else {
                Intent i = new Intent(CompanySettingsActivity.this, NetworkNotAvailable.class);
                startActivity(i);
            }

//            StaticClass.isLoginFalg=true;
//            transitionflag = StaticClass.transitionflagBack;
//            finish();

        }
    }

    @Override
    public void onButtonCLickDelUser(String strButtonText) {
        if (strButtonText.equals(CompanySettingsActivity.this.getResources().getString(R.string.yes))) {
//            StaticClass.isLoginFalg=true;
//            transitionflag = StaticClass.transitionflagBack;
//            finish();
//            Toast.makeText(this, "Yes Clicked", Toast.LENGTH_SHORT).show();
            if(cd.isConnectingToInternet()){
                new DeleteUser_Webservice().deleteUser(CompanySettingsActivity.this);

            }else {
                Intent i = new Intent(CompanySettingsActivity.this, NetworkNotAvailable.class);
                startActivity(i);
            }

        }else {
            // Toast.makeText(this, "No Clicked", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        StaticClass.BottomProfileCompany = false;
//        if(StaticClass.BottomProfileCompany){
//            finish();
//        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        sharedPref.putBottomViewCompany(StaticClass.Menu_profile_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanySettingsActivity.this, StaticClass.Menu_profile_company);

    }

    @Override
    public void responseOnDelete(String msg) {
//        new CustomToast(SettingsActivity.this, msg);
        if (msg.equals(StaticClass.SuccessResult)) {
            Log.d("d", "***Status 34***"+StaticClass.SuccessResult);
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanySettingsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

package com.neo.cars.app.fragment;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.ImageFullActivity;
import com.neo.cars.app.Interface.MoreDriverInfo_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Webservice.MoreDriverInfo_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parna on 23/4/18.
 */

public class MoreDriverInfoFragment extends Fragment implements MoreDriverInfo_Interface {

    private View mView;
    private CustomTextviewTitilliumWebRegular tvName, tvDOB, tvSex, tvAddress, tvNationality, tvEmploymntSince, tvFathersname, tvMobNo,
            tvAddl, tvDrivingLicense, tvDrivingLicenseBack, tvAdhaarCard, tvAdhaarCardBack;
    // ImageView ivDrivingLicense, ivAdhaarCard;
    private CircularImageViewBorder civDriverPic;
    private Context context;
    private ConnectionDetector cd;
    private String strDriverId="";
    private CustomTextviewTitilliumWebRegular tvFirstName, tvLastName;
    private DriverDataModel driverDataModel;
    private String strDriverImageUrl="";
    private ImageView ivDriverTick, ivDriverTickBack, ivAdhaarTick, ivAdhaarTickBack;
    final int PERMISSION_REQUEST_CODE = 111;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_driver_information, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            strDriverId = getArguments().getString("driverId");
            Log.d("d", "Get strvehicleId: "+strDriverId);
        }

        Initialize();

        //fetch user vehicle list
        if(!strDriverId.equals("")) {
            if (NetWorkStatus.isNetworkAvailable(getActivity())) {
                new MoreDriverInfo_Webservice().driverInfo(getActivity(), MoreDriverInfoFragment.this, strDriverId);

            } else {
                Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
                startActivity(i);
            }
        }
        Listener();
        return mView;
    }

    private void Initialize(){

        context = getActivity();
        cd = new ConnectionDetector(getActivity());

        tvFirstName = mView.findViewById(R.id.tvFirstName);
        tvLastName = mView.findViewById(R.id.tvLastName);
        tvDOB = mView.findViewById(R.id.tvDOB);
        tvSex = mView.findViewById(R.id.tvSex);
        tvAddress = mView.findViewById(R.id.tvAddress);
        tvNationality = mView.findViewById(R.id.tvNationality);
        tvEmploymntSince = mView.findViewById(R.id.tvEmploymntSince);
        tvFathersname = mView.findViewById(R.id.tvFathersname);
        tvMobNo = mView.findViewById(R.id.tvMobNo);
        tvAddl = mView.findViewById(R.id.tvAddl);
        civDriverPic = mView.findViewById(R.id.civDriverPic);
        tvDrivingLicense = mView.findViewById(R.id.tvDrivingLicense);
        tvDrivingLicenseBack = mView.findViewById(R.id.tvDrivingLicenseBack);
        tvAdhaarCard = mView.findViewById(R.id.tvAdhaarCard);
        tvAdhaarCardBack = mView.findViewById(R.id.tvAdhaarCardBack);

        ivAdhaarTick = mView.findViewById(R.id.ivAdhaarTick);
        ivAdhaarTickBack = mView.findViewById(R.id.ivAdhaarTickBack);
        ivDriverTick = mView.findViewById(R.id.ivDriverTick);
        ivDriverTickBack = mView.findViewById(R.id.ivDriverTickBack);
    }

    private void Listener(){

        civDriverPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(strDriverImageUrl)) {

                    Intent i = new Intent(getActivity(), ImageFullActivity.class);
                    i.putExtra("imgUrl", strDriverImageUrl);
                    startActivity(i);
                }
            }
        });

        tvMobNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(driverDataModel.getContact_no())){

                    contactPermission(driverDataModel.getContact_no());
//                    callMethod(driverDataModel.getContact_no());
                }
            }
        });
    }

    private void contactPermission(final String contact_mobile){

        Dexter.withActivity(getActivity()).withPermissions( Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE)
                .withListener(new MultiplePermissionsListener()
                {
                    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
                    @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_mobile));
                            startActivity(intent);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            showAlert();
                        }
                    }
                    @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                    {/* ... */

                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void showAlert(){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getContext().getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }


    private void callMethod(String passenger_contact) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                requestPermission();
            } else {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
            startActivity(intent);
        }

    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!strDriverId.equals("")) {
            if (NetWorkStatus.isNetworkAvailable(getActivity())) {
                new MoreDriverInfo_Webservice().driverInfo(getActivity(), MoreDriverInfoFragment.this, strDriverId);

            } else {
                Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
                startActivity(i);
            }
        }
    }

    @Override
    public void MoreDriverInfo(DriverDataModel driverDataModel) {

        this.driverDataModel = driverDataModel;

        if(!TextUtils.isEmpty(driverDataModel.getFirst_name()) && !TextUtils.isEmpty(driverDataModel.getLast_name())){
            tvFirstName.setText(driverDataModel.getFirst_name()+" "+driverDataModel.getLast_name());
        }else{
            tvFirstName.setText("N/A");
        }


        if(! driverDataModel.getDob().equals("") && !driverDataModel.getDob().equals("null")){
            tvDOB.setText(driverDataModel.getDob());

        }else{
            tvDOB.setText("N/A");
        }

        if(! driverDataModel.getGender().equals("") && ! driverDataModel.getGender().equals("null")){
            if (driverDataModel.getGender().equalsIgnoreCase("M")){
                tvSex.setText("Male");
            }else if (driverDataModel.getGender().equalsIgnoreCase("F")){
                tvSex.setText("Female");
            }else {
                tvSex.setText("Others");
            }

        }else{
            tvSex.setText("N/A");
        }

        if(!driverDataModel.getAddress().equals("") && ! driverDataModel.getAddress().equals("null")){
            tvAddress.setText(driverDataModel.getAddress());

        }else{
            tvAddress.setText("N/A");
        }

        if(! driverDataModel.getNationality().equals("") && ! driverDataModel.getNationality().equals("null")){
            tvNationality.setText(driverDataModel.getNationality());

        }else {
            tvNationality.setText("N/A");
        }

        if(! driverDataModel.getIn_employment_since().equals("") && ! driverDataModel.getIn_employment_since().equals("null")){
            tvEmploymntSince.setText(driverDataModel.getIn_employment_since());
        }

        if(! driverDataModel.getFathers_name().equals("") && ! driverDataModel.getFathers_name().equals("null")){
            tvFathersname.setText(driverDataModel.getFathers_name());

        }else{
            tvFathersname.setText("N/A");
        }

        if(! driverDataModel.getContact_no().equals("") && ! driverDataModel.getContact_no().equals("null")){
            tvMobNo.setText(driverDataModel.getContact_no());
            tvMobNo.setTextColor(Color.parseColor("#1e65a6"));
            tvMobNo.setPaintFlags(tvMobNo.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        }else{
            tvMobNo.setText("N/A");
        }

        if(! driverDataModel.getAdditional_info().equals("") && !driverDataModel.getAdditional_info().equals("null")){
            tvAddl.setText(driverDataModel.getAdditional_info());

        }else{
            tvAddl.setText("N/A");
        }


        if(! driverDataModel.getRecent_photograph().equals("") && ! driverDataModel.getRecent_photograph().equals("null")){
            Picasso.get().load(driverDataModel.getRecent_photograph()).into(civDriverPic);
            strDriverImageUrl = driverDataModel.getRecent_photograph();
            Log.d("d", "**strDriverImageUrl**"+strDriverImageUrl);
        }

        ArrayList<UserDocumentModel> listOfDoc= driverDataModel.getArr_UserDocumentModel();
        if (listOfDoc != null){
            for (int i=0;i<listOfDoc.size();i++){
                if (listOfDoc.get(i).getDocument_type().equals("AC")){

                    if (listOfDoc.get(i).getDocument_side().equalsIgnoreCase("F")) {

                        String[] parts = listOfDoc.get(i).getDocument_file().split("/");
                        tvAdhaarCard.setText(parts[parts.length - 1]);
                        ivAdhaarTick.setVisibility(View.VISIBLE);

                    }else if (listOfDoc.get(i).getDocument_side().equalsIgnoreCase("B")){
                        String[] parts = listOfDoc.get(i).getDocument_file().split("/");
                        tvAdhaarCardBack.setText(parts[parts.length - 1]);
                        ivAdhaarTickBack.setVisibility(View.VISIBLE);
                    }

                }else if (listOfDoc.get(i).getDocument_type().equals("DL")){

                    if (listOfDoc.get(i).getDocument_side().equalsIgnoreCase("F")) {

                        String[] parts = listOfDoc.get(i).getDocument_file().split("/");
                        tvDrivingLicense.setText(parts[parts.length - 1]);
                        ivDriverTick.setVisibility(View.VISIBLE);

                    }else if (listOfDoc.get(i).getDocument_side().equalsIgnoreCase("B")){
                        String[] parts = listOfDoc.get(i).getDocument_file().split("/");
                        tvDrivingLicenseBack.setText(parts[parts.length - 1]);
                        ivDriverTickBack.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

    }
}

package com.neo.cars.app.Utils;

public class GetterShetter {
	
	public static String CategoryType="restaurant";
	public static String Categoryname="Restaurant";
	public static String gc_lat="22.572646";
	public static String gc_long="88.36389500000001";
	public static String LocationName="Kolkata";


	public static String getCategoryType() {
		return CategoryType;
	}

	public static void setCategoryType(String categoryType) {
		CategoryType = categoryType;
	}
	
	
	public static String getCategoryname() {
		return Categoryname;
	}

	public static void setCategoryname(String categoryname) {
		Categoryname = categoryname;
	}
	
	public static String getGc_lat() {
		return gc_lat;
	}

	public static void setGc_lat(String gc_lat) {
		GetterShetter.gc_lat = gc_lat;
	}
	
	public static String getGc_long() {
		return gc_long;
	}

	public static void setGc_long(String gc_long) {
		GetterShetter.gc_long = gc_long;
	}
	
	public static String getLocationName() {
		return LocationName;
	}

	public static void setLocationName(String locationName) {
		LocationName = locationName;
	}

}

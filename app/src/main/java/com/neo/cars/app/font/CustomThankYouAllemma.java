package com.neo.cars.app.font;

import android.content.Context;
import android.graphics.Typeface;

import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by parna on 11/5/18.
 */

public class CustomThankYouAllemma extends AppCompatTextView {

    public CustomThankYouAllemma(Context context) {
        super(context);
    }

    public CustomThankYouAllemma(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomThankYouAllemma(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if(!isInEditMode()){
            //------ Font------//
            Typeface typeface1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/AllemaFreeDemo.ttf");

            setTypeface(typeface1,   Typeface.NORMAL);
        }

    }
}

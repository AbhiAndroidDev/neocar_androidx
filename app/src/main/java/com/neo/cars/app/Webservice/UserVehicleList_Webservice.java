package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.InboxModalInterface;
import com.neo.cars.app.Interface.UserVehicleList_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.InboxModal;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 2/4/18.
 */

public class UserVehicleList_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strCanAddVehicle="", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private ArrayList<VehicleTypeModel> arrlistVehicle = new ArrayList<>();
    UserVehicleList_Interface IuserVehicleList_Interface;
    private int transitionflag = StaticClass.transitionflagNext;

    public void userVehicleList(Activity context, UserVehicleList_Interface muserVehicleList_Interface ){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        IuserVehicleList_Interface = muserVehicleList_Interface;

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest userVehicleListRequest  = new StringRequest(Request.Method.POST, Urlstring.user_vehicle_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response****", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPref.getUserid());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        userVehicleListRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(userVehicleListRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response){

        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("user_vehicle_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("user_vehicle_list").optString("message");
            Status= jobj_main.optJSONObject("user_vehicle_list").optString("status");
            strCanAddVehicle = jobj_main.optJSONObject("user_vehicle_list").optString("can_add_vehicle");

            JSONArray details=jobj_main.optJSONObject("user_vehicle_list").optJSONArray("details");
            if (Status.equals(StaticClass.SuccessResult)){
                arrlistVehicle = new ArrayList<>();
                for (int i=0; i<details.length(); i++){

                    VehicleTypeModel vehicleTypeModel = new VehicleTypeModel();

                    vehicleTypeModel.setVehicle_id(details.getJSONObject(i).optString("vehicle_id"));
                    vehicleTypeModel.setBrand(details.getJSONObject(i).optString("brand"));
                    vehicleTypeModel.setModel(details.getJSONObject(i).optString("model"));
                    vehicleTypeModel.setYear(details.getJSONObject(i).optString("year"));
                    vehicleTypeModel.setKm_travelled(details.getJSONObject(i).optString("km_travelled"));
                    vehicleTypeModel.setSpecial_info(details.getJSONObject(i).optString("special_info"));
                    vehicleTypeModel.setAc_available(details.getJSONObject(i).optString("ac_available"));
                    vehicleTypeModel.setState(details.getJSONObject(i).optString("state"));
                    vehicleTypeModel.setCity(details.getJSONObject(i).optString("city"));
                    vehicleTypeModel.setHourly_rate(details.getJSONObject(i).optString("hourly_rate"));
                    vehicleTypeModel.setMax_luggage(details.getJSONObject(i).optString("max_luggage"));
                    vehicleTypeModel.setMax_passenger(details.getJSONObject(i).optString("max_passenger"));
                    vehicleTypeModel.setLicense_plate_no(details.getJSONObject(i).optString("license_plate_no"));
                    vehicleTypeModel.setImage_url(details.getJSONObject(i).optString("image_url"));
                    vehicleTypeModel.setDriver_id(details.getJSONObject(i).optString("driver_id"));
                    vehicleTypeModel.setDriver_name(details.getJSONObject(i).optString("driver_name"));
                    vehicleTypeModel.setDriver_contact_no(details.getJSONObject(i).optString("driver_contact_no"));
                    vehicleTypeModel.setDriver_email(details.getJSONObject(i).optString("driver_email"));
                    vehicleTypeModel.setDriver_address(details.getJSONObject(i).optString("driver_address"));
                    vehicleTypeModel.setVehicle_status(details.getJSONObject(i).optString("vehicle_status"));
                    vehicleTypeModel.setIs_active_by_owner(details.getJSONObject(i).optString("is_active_by_owner"));

                    arrlistVehicle.add(vehicleTypeModel);

                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }



        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                // ((UserVehicleList_Interface)).UserVehicleListInterface(arrlistVehicle);
                IuserVehicleList_Interface.UserVehicleListInterface(arrlistVehicle, strCanAddVehicle);
            }
        }

    }

}

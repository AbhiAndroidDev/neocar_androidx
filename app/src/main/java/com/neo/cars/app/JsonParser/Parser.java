package com.neo.cars.app.JsonParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("deprecation")
public class Parser {
	
	public static HashMap<String, String> result=new HashMap<String, String>();

	public static HashMap<String, String> parseJsonObject(String url, String method,List<NameValuePair> params) {
		InputStream iStream = null;
		String data = "";

		System.out.println("url*************"+url);
		System.out.println("params*************"+params);
		
		DefaultHttpClient httpClient = null;
		try {

			// check for request method
			if (method == "POST") {
				// request method is POST 
				// defaultHttpClient
				if(httpClient==null)
				{
					
					httpClient = new DefaultHttpClient();
				}
				 
					HttpPost httpPost = new HttpPost(url);
					httpPost.setEntity(new UrlEncodedFormEntity(params));
					
					// set timeout 
					HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 20000);
					HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30000);
					
					HttpResponse httpResponse = httpClient.execute(httpPost);
					
					if (httpResponse.getStatusLine().getStatusCode() == 200) {
								HttpEntity httpEntity = httpResponse.getEntity();
								iStream = httpEntity.getContent();
								BufferedReader br = new BufferedReader(
										new InputStreamReader(iStream, "utf8"));
								StringBuffer sb = new StringBuffer();
								String line = "";
								while ((line = br.readLine()) != null) {
									sb.append(line);
								}
								data = sb.toString();
								br.close();
								iStream.close();
								httpClient.getConnectionManager().closeExpiredConnections();
								result.put("code", "200");
								result.put("value", data);
					} else {
								httpClient.getConnectionManager().closeExpiredConnections();
								result.put("code", String.valueOf(httpResponse.getStatusLine().getStatusCode()));
								result.put("value", "");
					}

			} else if (method == "GET") {
				// request method is GET
				if(httpClient==null)
				{
					httpClient = new DefaultHttpClient();
				}
					String paramString = URLEncodedUtils.format(params, "utf-8");
					url += "?" + paramString;
					HttpGet httpGet = new HttpGet(url);
					
					// set timeout 
					HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 20000);
					HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30000);
					
					HttpResponse httpResponse = httpClient.execute(httpGet);

						if (httpResponse.getStatusLine().getStatusCode() == 200) {
								HttpEntity httpEntity = httpResponse.getEntity();
								iStream = httpEntity.getContent();
								BufferedReader br = new BufferedReader(
										new InputStreamReader(iStream, "utf8"));
								StringBuffer sb = new StringBuffer();
								String line = "";
								while ((line = br.readLine()) != null) {
									sb.append(line);
								}
								data = sb.toString();
								br.close();
								iStream.close();
								httpClient.getConnectionManager().closeExpiredConnections();
								result.put("code", "200");
								result.put("value", data);
						} else {
								httpClient.getConnectionManager().closeExpiredConnections();
								result.put("code", String.valueOf(httpResponse.getStatusLine().getStatusCode()));
								result.put("value", "");
						}

			}

		} catch (Exception e) {
			result.put("code", "9999");
			result.put("value", "");
			e.printStackTrace();
			return result;
		} 
		return result;
	}


}

package com.neo.cars.app.SetGet;

public class VehicleListModel {

    private String vehicle_id;
    private String total_trip_cost;
    private String vehicle_make_id;
    private String vehicle_make_name;
    private String vehicle_model_id;
    private String vehicle_model_name;
    private String vehicle_type_id;
    private String vehicle_type_name;
    private String is_luxury;
    private String max_luggage;
    private String max_passenger;
    private String overtime_available;
    private String avg_vehicle_rating;
    private String image_file;

    public String getIs_luxury() {
        return is_luxury;
    }

    public void setIs_luxury(String is_luxury) {
        this.is_luxury = is_luxury;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getTotal_trip_cost() {
        return total_trip_cost;
    }

    public void setTotal_trip_cost(String total_trip_cost) {
        this.total_trip_cost = total_trip_cost;
    }

    public String getVehicle_make_id() {
        return vehicle_make_id;
    }

    public void setVehicle_make_id(String vehicle_make_id) {
        this.vehicle_make_id = vehicle_make_id;
    }

    public String getVehicle_make_name() {
        return vehicle_make_name;
    }

    public void setVehicle_make_name(String vehicle_make_name) {
        this.vehicle_make_name = vehicle_make_name;
    }

    public String getVehicle_model_id() {
        return vehicle_model_id;
    }

    public void setVehicle_model_id(String vehicle_model_id) {
        this.vehicle_model_id = vehicle_model_id;
    }

    public String getVehicle_model_name() {
        return vehicle_model_name;
    }

    public void setVehicle_model_name(String vehicle_model_name) {
        this.vehicle_model_name = vehicle_model_name;
    }

    public String getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public String getVehicle_type_name() {
        return vehicle_type_name;
    }

    public void setVehicle_type_name(String vehicle_type_name) {
        this.vehicle_type_name = vehicle_type_name;
    }

    public String getMax_luggage() {
        return max_luggage;
    }

    public void setMax_luggage(String max_luggage) {
        this.max_luggage = max_luggage;
    }

    public String getMax_passenger() {
        return max_passenger;
    }

    public void setMax_passenger(String max_passenger) {
        this.max_passenger = max_passenger;
    }

    public String getOvertime_available() {
        return overtime_available;
    }

    public void setOvertime_available(String overtime_available) {
        this.overtime_available = overtime_available;
    }

    public String getAvg_vehicle_rating() {
        return avg_vehicle_rating;
    }

    public void setAvg_vehicle_rating(String avg_vehicle_rating) {
        this.avg_vehicle_rating = avg_vehicle_rating;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }
}

package com.neo.cars.app.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.neo.cars.app.Interface.CallBackOnDeleteUser;
import com.neo.cars.app.R;


/**
 * Created by kamail on 15/11/17.
 */

public class BottomSheetDialogDeleteUser {

    private Context mContext;
    private Activity mActivity;
    private CallBackOnDeleteUser mClickCallBackButton;
    private String mStrMsg, mStrNegative, mStrPositive;
    private CallBackOnDeleteUser buttonClick=null;

    public BottomSheetDialogDeleteUser(Context context, Activity activity,
                                       String strMsg,
                                       String strNegative,
                                       String strPositive) {
        this.mContext = context;
        this.mActivity = activity;
        this.mStrMsg = strMsg;
        this.mStrNegative = strNegative;
        this.mStrPositive = strPositive;

        // call bottom sheet dialog
        callBottomSheetDialog();
    }

    public BottomSheetDialogDeleteUser(Context context, Activity activity,
                                       CallBackOnDeleteUser buttonClick,
                                       String strMsg,
                                       String strNegative,
                                       String strPositive) {
        this.mContext = context;
        this.mActivity = activity;
        this.buttonClick = buttonClick;
        this.mStrMsg = strMsg;
        this.mStrNegative = strNegative;
        this.mStrPositive = strPositive;

        // call bottom sheet dialog
        callBottomSheetDialog();
    }


    private void callBottomSheetDialog() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(mContext);
        View sheetView = mActivity.getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_positive_negative, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        TextView tvMsg;
        Button btNegative, btPositive;

        tvMsg = sheetView.findViewById(R.id.tvMsg);
        btNegative = sheetView.findViewById(R.id.btNegative);
        btPositive = sheetView.findViewById(R.id.btPositive);

        tvMsg.setText(mStrMsg);
        btNegative.setText(mStrNegative);
        btPositive.setText(mStrPositive);

        btNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (buttonClick == null) {
                    ((CallBackOnDeleteUser) mActivity).onButtonCLickDelUser(mStrNegative);

                }else {
                    buttonClick.onButtonCLickDelUser(mStrNegative);
                }
            }
        });

        btPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (buttonClick == null) {
                    ((CallBackOnDeleteUser) mActivity).onButtonCLickDelUser(mStrPositive);
                }else {
                    buttonClick.onButtonCLickDelUser(mStrPositive);
                }
            }
        });
    }
}
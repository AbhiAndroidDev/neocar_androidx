package com.neo.cars.app.Utils;

import android.os.Build;

/**
 * Created by parna on 6/3/18.
 */

public interface ConstantInterface {

    public static final boolean SHOWTAG = true;
    public static final int version = Build.VERSION.SDK_INT;
    public static int transitionFlagNext = 1;
    public static int transitionFlagBack = 2;

}

package com.neo.cars.app.Interface;

/**
 * Created by joydeep on 5/2/18.
 */

public interface CallBackButtonClick {
    void onButtonClick(String strButtonText);
}

package com.neo.cars.app.View;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.neo.cars.app.Interface.CallbackCancelButtonClick;
import com.neo.cars.app.Interface.CallbackCancelReasonButtonClick;
import com.neo.cars.app.R;
import com.neo.cars.app.Utils.CustomToast;

/**
 * Created by parna on 30/8/18.
 */

public class BottomSheetCancelView {
    private View v;
    private Context mContext;
    private Activity mActivity;
    private CallbackCancelButtonClick mClickCallBackButton;
    private String mStrMsg, mStrNegative, mStrPositive, mBookingFlag, mBookingType, mCancelReason = "";
    private CallbackCancelButtonClick buttonClick=null;
    private Button btNegative, btPositive;
    private LinearLayout linear_dialog_cancel;
    private EditText etCancelText;


    public View BottomSheetCancelView(Context context, Activity activity,
                                      CallbackCancelButtonClick buttonClick,
                                      String strMsg,
                                      String strNegative,
                                      String strPositive,
                                      String strBookingFlag,
                                      String strBookingType){
        this.mContext = context;
        this.mActivity = activity;
        this.buttonClick = buttonClick;
        this.mStrMsg = strMsg;
        this.mStrNegative = strNegative;
        this.mStrPositive = strPositive;
        this.mBookingFlag = strBookingFlag;
        this.mBookingType = strBookingType;

        v = LayoutInflater.from(mContext).inflate(R.layout.dialog_cancel, null);

        Initialize();
        Listener();
        return v;

    }

    private void Initialize() {

        btNegative = v.findViewById(R.id.btNegative);
        btPositive = v.findViewById(R.id.btPositive);
        linear_dialog_cancel= v.findViewById(R.id.linear_dialog_cancel);
        etCancelText = v.findViewById(R.id.etCancelText);

        btNegative.setText(mStrNegative);
        btPositive.setText(mStrPositive);

    }

    private void Listener(){
        btNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (buttonClick == null) {
                    ((CallbackCancelButtonClick) mActivity).onButtonClick(mStrNegative, mBookingFlag, mBookingType);
                }else {
                    buttonClick.onButtonClick(mStrNegative, mBookingFlag, mBookingType);
                }
            }
        });

        btPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCancelReason = etCancelText.getText().toString();

                if (!TextUtils.isEmpty(mCancelReason)) {

                        ((CallbackCancelReasonButtonClick) mActivity).onCancelReasonSubmitButtonClick(mStrPositive, mBookingFlag, mBookingType, mCancelReason);


//                    if (buttonClick == null) {
//                        ((CallbackCancelButtonClick) mActivity).onButtonClick(mStrPositive, mBookingFlag, mBookingType);
//                    } else {
//                        buttonClick.onButtonClick(mStrPositive, mBookingFlag, mBookingType);
//                    }
                }else {
                    new CustomToast(mActivity, "Please write reason for cancelling the booking");
                }
            }
        });

        linear_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("****Tap on cancel**");
                ((CallbackCancelButtonClick) mActivity).onButtonClick("Cancel", mBookingFlag, mBookingType);
            }
        });
    }
}

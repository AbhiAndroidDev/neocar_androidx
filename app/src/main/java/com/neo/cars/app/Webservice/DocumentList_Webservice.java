package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.DocumentListInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.DocumentListModel;
import com.neo.cars.app.SetGet.StateListModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 26/3/18.
 */

public class DocumentList_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private DocumentListModel documentListModel;

    private ArrayList<DocumentListModel> arrlistDocument;
    private JSONObject jsonObjectDocumentList;

    public void documentListWebservice(Activity context){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistDocument = new ArrayList<>();
        gson = new Gson();
        UserLoginDetails = new UserLoginDetailsModel();
        documentListModel = new DocumentListModel();

        showProgressDialog();

        StringRequest documentListRequest  = new StringRequest(Request.Method.POST, Urlstring.document_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        documentListRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(documentListRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;

        try{
            jobj_main = new JSONObject(response);

            Msg = jobj_main.optJSONObject("document_list").optString("message");
            Status = jobj_main.optJSONObject("document_list").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("document_list").optJSONArray("details");
            for (int i = 0; i < jsonArrayDetails.length(); i++) {
                jsonObjectDocumentList = jsonArrayDetails.optJSONObject(i);

               /* StateListModel stateListModel = new StateListModel();
                stateListModel.setId(jsonObjectState.optString("id"));
                stateListModel.setName(jsonObjectState.optString("name"));

                arrlistState.add(stateListModel);*/

               DocumentListModel documentListModel = new DocumentListModel();
               documentListModel.setDocument_code(jsonObjectDocumentList.optString("document_code"));
               documentListModel.setDocument_name(jsonObjectDocumentList.optString("document_name"));

               arrlistDocument.add(documentListModel);

            }
            Log.d("d", "Document listmodel size:" + arrlistDocument.size());
            Log.e("d", "Document listmodel" + arrlistDocument);


        }catch (Exception e) {
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {
            ((DocumentListInterface)mcontext).DocumentList(arrlistDocument);

        }

    }

}

package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.OwnerDeactive_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 14/6/18.
 */

public class VehicleSoftDelete_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    int int_position;
    OwnerDeactive_Interface ownerDeactiveInterfacefinal;
    //VehicleDelete_Interface vehicleDeleteInterface;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void VehicleSoftDelete (Activity context, final String vehicle_id){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
     //   ownerDeactiveInterfacefinal=OwnerDeactiveInterfacefinal;
        sharedPref = new SharedPrefUserDetails(mcontext);
       // int_position=position;

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();
        StringRequest ownerDeactivateVehicleStringRequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_soft_delete,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response::", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("vehicle_id", vehicle_id);
                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("params******getParams***" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        ownerDeactivateVehicleStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(ownerDeactivateVehicleStringRequest);


    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_soft_delete").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_soft_delete").optString("message");
            Status= jobj_main.optJSONObject("vehicle_soft_delete").optString("status");


        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if(Status.equals(StaticClass.SuccessResult)){
                // ownerDeactiveInterfacefinal.OnOwnerDeactive(int_position);

                StaticClass.MyVehicleIsDelete = true;

                mcontext.finish();

            }else{
                new CustomToast(mcontext, Msg);
            }
        }


    }


}

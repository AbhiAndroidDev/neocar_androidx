package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.CompanyHomeProfileActivity;
import com.neo.cars.app.HomeProfileActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SearchActivity;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.ShareFcmDetails;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.ViewCompanyprofile;
import com.neo.cars.app.ViewUserProfile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 15/3/18.
 */

public class MobileLogin_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "";

    private SharedPrefUserDetails sharedPref;
    private ShareFcmDetails shareFcm;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;

    private String userType="";
    private String st_mobile="",otp="", unique_id="";

    public void MobileLogin(Activity context, final String mobile, String sotp, final String user_type) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        st_mobile=mobile;
        otp=sotp;

        unique_id = Settings.Secure.getString(mcontext.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("d", "Device id::"+unique_id);

        sharedPref = new SharedPrefUserDetails(mcontext);
        shareFcm=new ShareFcmDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.mobile_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_no", st_mobile);
                params.put("otp", otp);
                params.put("device_type", "A");
                params.put("device_id",shareFcm.getFcmId());
//                params.put("user_type", sharedPref.getUserType());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);

    }




    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);


            Msg = jobj_main.optJSONObject("mobile_login").optString("message");
            Status= jobj_main.optJSONObject("mobile_login").optString("status");

            JSONObject details=jobj_main.optJSONObject("mobile_login").optJSONObject("details");

            if (Status.equals(StaticClass.SuccessResult)) {
                UserLoginDetails.setId(details.optString("id"));
                sharedPref.setUserId(details.optString("id"));
                UserLoginDetails.setEmail(details.optString("email"));
                UserLoginDetails.setMobile(details.optString("mobile_no"));
                Log.e("mobile_no", details.optString("mobile_no"));
                sharedPref.setMobile(details.optString("mobile_no"));
                userType = details.optString("user_type");
                sharedPref.setUserType(userType);
                UserLoginDetails.setIs_active(details.optString("is_active"));
                //UserLoginDetails.setAd_username(details.optString("profile_name"));
                UserLoginDetails.setProfile_name(details.optString("profile_name"));

                UserLoginDetails.setProfile_pic(details.optString("profile_pic"));
                UserLoginDetails.setReg_type(details.optString("reg_type"));
                UserLoginDetails.setReferral_code(details.optString("referral_code"));
                UserLoginDetails.setIs_notification_active(details.optString("is_notification_active"));
                UserLoginDetails.setIs_profile_complete(details.optString("is_profile_complete"));


                String strParentDetails = gson.toJson(UserLoginDetails);
                sharedPref.putObjectToPreferenceUserDetails(strParentDetails);
                sharedPref.setUserloginStatus();

            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("mobile_login").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);

            }
        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        //new CustomToast(mcontext, Msg);

        if (Status.equals(StaticClass.SuccessResult)) {

            if (mcontext.getClass().getName().equals(mcontext.getPackageName()+".ViewUserProfile")){
                Intent viewuserintent = new Intent(mcontext, ViewUserProfile.class);
                mcontext.startActivity(viewuserintent);
                mcontext.finish();

            }else if (mcontext.getClass().getName().equals(mcontext.getPackageName()+".ViewCompanyprofile")){
                Intent viewuserintent = new Intent(mcontext, ViewCompanyprofile.class);
                mcontext.startActivity(viewuserintent);
                mcontext.finish();

            }else if (userType.equalsIgnoreCase("U")) {
//                    Intent invitefriendsIntent = new Intent(mcontext, HomeProfileActivity.class);
                Intent invitefriendsIntent = new Intent(mcontext, SearchActivity.class);
                sharedPref.putBottomView(StaticClass.Menu_Explore);
                mcontext.startActivity(invitefriendsIntent);
                mcontext.finish();
            }else if (userType.equalsIgnoreCase("C")){
                Intent invitefriendsIntent = new Intent(mcontext, CompanyHomeProfileActivity.class);
                mcontext.startActivity(invitefriendsIntent);
                mcontext.finish();

            }
        }
    }
}

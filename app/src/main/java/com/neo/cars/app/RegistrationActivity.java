package com.neo.cars.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.Registration_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

//import me.iwf.photopicker.PhotoPicker;


/**
 * Created by parna on 2/3/18.
 */

public class RegistrationActivity extends RootActivity implements CallBackButtonClick {

    private static final String TAG = RegistrationActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_CONTACTS = 102;
    private ScrollView scvRegisterLayout;
    private RelativeLayout rlParentRelativeLayout;
    private LinearLayout llRegisterToLinearLayout;
    private CircularImageViewBorder civUserProfilePic;
    private CustomEditTextTitilliumWebRegular etRegName, etRegEmail, etRegMob, etRegFirstName, etRegLastName;
    //private CustomEditTextTitilliumWebRegular  etRegPassword, etRegConfPassword;
    private CustomButtonTitilliumSemibold btnRegister;
    //private CustomTextviewTitilliumWebRegular etRegDOB;
    private TextView tvTermsAndCondition;
    private ImageView ivChangeImage, ivContactIconReg;
    private String strRegFirstName = "", strRegLastName = "", strRegEmail = "", strRegPass = "", strRegConfPass = "", strRegMob = "",
            strRegDOB = "", userChoosenTask = "", imageFilePath = "", mSelectedGalleryImagePath = "", mGalleryImagePath = "";
    private ImageButton ibBackbutton;

    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int PICK_IMAGE_REQUEST = 904;

    private Context context;
    private File fileUserProfile;
    private ConnectionDetector cd;

    private Calendar mcalendar;
    private int day, month, year;
    private int transitionflag = StaticClass.transitionflagNext;

    private Uri uriContact;
    private String contactID;     // contacts unique ID

    private ProgressDialog dialog;
    private Uri imageUri;
    private String selectedNumber = "";
    private SharedPrefUserDetails prefs;

    private CheckBox cb_terms;
    private TextView tvByLoggingIntoText_privacypolicy;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        new AnalyticsClass(RegistrationActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize() {
        context = this;
        cd = new ConnectionDetector(this);
        prefs = new SharedPrefUserDetails(this);

        tvTermsAndCondition = findViewById(R.id.tvTermsAndCondition);

        scvRegisterLayout = findViewById(R.id.scvRegisterLayout);
        rlParentRelativeLayout = findViewById(R.id.rlParentRelativeLayout);
        llRegisterToLinearLayout = findViewById(R.id.llRegisterToLinearLayout);
        civUserProfilePic = findViewById(R.id.civUserProfilePic);
        // etRegName = findViewById(R.id.etRegName);
        etRegEmail = findViewById(R.id.etRegEmail);
        //etRegPassword = findViewById(R.id.etRegPassword);
        //etRegConfPassword = findViewById(R.id.etRegConfPassword);
        etRegMob = findViewById(R.id.etRegMob);
       // etRegDOB = findViewById(R.id.etRegDOB);
        btnRegister = findViewById(R.id.btnRegister);
        ivChangeImage = findViewById(R.id.ivChangeImage);
        ivContactIconReg = findViewById(R.id.ivContactIconReg);
        ibBackbutton = findViewById(R.id.ibBackbutton);

        etRegFirstName = findViewById(R.id.etRegFirstName);
        etRegLastName = findViewById(R.id.etRegLastName);

        cb_terms= findViewById(R.id.cb_terms);
        tvByLoggingIntoText_privacypolicy= findViewById(R.id.tvByLoggingIntoText_privacypolicy);

        mcalendar = Calendar.getInstance();

        day = mcalendar.get(Calendar.DAY_OF_MONTH);
        year = mcalendar.get(Calendar.YEAR);
        month = mcalendar.get(Calendar.MONTH);
    }

    private void Listener() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cb_terms.isChecked()) {
                    RegistrationChecking();
                }else{
                    new CustomToast(RegistrationActivity.this, getResources().getString(R.string.Pleaseaccepttheprivacypolicytoprocess));
                }

            }
        });

        civUserProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();

            }
        });

     /*   etRegDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoBDialog();
            }
        });*/

        ibBackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagBack;
                Intent i = new Intent(RegistrationActivity.this, LoginStepOneActivity.class);
                startActivity(i);
                finish();
            }
        });


        tvTermsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent termsIntent = new Intent(RegistrationActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);
            }
        });

        tvByLoggingIntoText_privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent termsIntent = new Intent(RegistrationActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.PrivacyPolicy);
                startActivity(termsIntent);
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getLoginStatus()){
            finish();
        }
    }

    private void RegistrationChecking() {

        etRegFirstName.setError(null);
        etRegLastName.setError(null);

        etRegEmail.setError(null);
        //etRegPassword.setError(null);
       // etRegConfPassword.setError(null);
        etRegMob.setError(null);

        boolean cancel = false;
        View focusView = null;

        strRegFirstName = etRegFirstName.getText().toString().trim();
        strRegLastName = etRegLastName.getText().toString().trim();

        strRegEmail = etRegEmail.getText().toString().trim();
        //strRegPass = etRegPassword.getText().toString().trim();
        //strRegConfPass = etRegConfPassword.getText().toString().trim();
        strRegMob = etRegMob.getText().toString().trim();
        //strRegDOB = etRegDOB.getText().toString().trim();

        if (TextUtils.isEmpty(strRegFirstName)) {
            etRegFirstName.setError(getString(R.string.error_field_required));
            etRegFirstName.requestFocus();

        } else if (TextUtils.isEmpty(strRegLastName)) {
            etRegLastName.setError(getString(R.string.error_field_required));
            etRegLastName.requestFocus();

        } else if (TextUtils.isEmpty(strRegEmail)) {
            etRegEmail.setError(getString(R.string.error_field_required));
            etRegEmail.requestFocus();

        } else if (!(new Emailvalidation().email_validation(strRegEmail))) {
            etRegEmail.setError(getString(R.string.error_email_format));
            etRegEmail.requestFocus();
        }

       /* else if (TextUtils.isEmpty(strRegPass)) {
            etRegPassword.setError(getString(R.string.error_field_required));
            etRegPassword.requestFocus();
        } else if (strRegPass.length() < 6) {
            etRegPassword.setError(getString(R.string.minimum_length_six));
            etRegPassword.requestFocus();
        } else if (TextUtils.isEmpty(strRegConfPass)) {
            etRegConfPassword.setError(getString(R.string.error_field_required));
            etRegConfPassword.requestFocus();
        } else if (!strRegPass.equals(strRegConfPass)) {
            etRegConfPassword.setError(getString(R.string.error_passwords_not_match));
            etRegConfPassword.requestFocus();
        }*/

        else if (TextUtils.isEmpty(strRegMob)) {
            etRegMob.setError(getString(R.string.error_field_required));
            etRegMob.requestFocus();

        } else if (!(new Emailvalidation().phone_validation(strRegMob))) {
            etRegMob.setError(getString(R.string.error_telphone_format));
            etRegMob.requestFocus();

        } /*else if (TextUtils.isEmpty(strRegDOB)) {
            etRegDOB.requestFocus();

        }*/  else {
            if (cd.isConnectingToInternet()) {
                new Registration_Webservice().Registration(RegistrationActivity.this, strRegFirstName, strRegLastName, strRegEmail,strRegMob,fileUserProfile);
               // new Registration_Webservice().Registration(RegistrationActivity.this, strRegFirstName, strRegLastName, strRegEmail, strRegPass, strRegConfPass, strRegMob, strRegDOB, fileUserProfile);

            } else {
                Intent i = new Intent(RegistrationActivity.this, NetworkNotAvailable.class);
                startActivity(i);
            }
        }
    }


    public void selectImage() {

        BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(context,
                RegistrationActivity.this,
                RegistrationActivity.this,
                getResources().getString(R.string.upload_user_photo),
                RegistrationActivity.this.getResources().getString(R.string.Camera),
                RegistrationActivity.this.getResources().getString(R.string.Gallery));
    }


    //take photo through camera
    private void takePhoto() {
        // set a image file path
        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

    }

    //select image from gallery
    private void mCallPhotoGallary() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

//        try {
//
//            PhotoPicker.builder()
//                    .setPhotoCount(1)
//                    .setShowCamera(true)
//                    .setShowGif(false)
//                    .setPreviewEnabled(false)
//                    .start(this, PICK_IMAGE_REQUEST);
//
//
//        } catch (Exception e) {
//            //To catch unknown Null pointer exception inside library
//            e.printStackTrace();
//        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_IMAGE_CAPTURE:

                    dialog = new ProgressDialog(context);
                    try {

                        dialog.setMessage("Image processing...");
                        dialog.show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Bitmap fullImage = MediaStore.Images.Media.getBitmap(RegistrationActivity.this.getContentResolver(), imageUri);

                                    InputStream input = context.getContentResolver().openInputStream(imageUri);
                                    ExifInterface ei;
                                    if (Build.VERSION.SDK_INT > 23)
                                        ei = new ExifInterface(input);
                                    else
                                        ei = new ExifInterface(imageUri.getPath());

                                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                    switch (orientation) {
                                        case ExifInterface.ORIENTATION_ROTATE_90:
                                            fullImage = rotateImage(fullImage, 90);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_180:
                                            fullImage = rotateImage(fullImage, 180);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_270:
                                            fullImage = rotateImage(fullImage, 270);
                                            break;
                                        default:
                                            break;
                                    }

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                    byte[] byteArray = bytes.toByteArray();
                                    Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
//                                    Bitmap scaledBitmap = (Bitmap) data.getExtras().get("data");
                                    fileUserProfile = ImageUtils.saveImage(scaledBitmap, context);

                                    if (fileUserProfile != null) {
                                        Log.d("d", "Camera image file:" + fileUserProfile);
                                        Picasso.get().load(fileUserProfile).into(civUserProfilePic);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                } finally {
                                    dialog.dismiss();
                                }
                            }
                        }, 7000);  // 7 sec to allow processing of image captured


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                    }

                    break;

                case PICK_IMAGE_REQUEST:

                    try {
//                        ArrayList<String> photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);


                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {
                            InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                            bufferedInputStream = new BufferedInputStream(inputStream);
                            bmp = BitmapFactory.decodeStream(bufferedInputStream);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                            byte[] byteArray = bytes.toByteArray();
                            Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                            fileUserProfile = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }


//                        mSelectedGalleryImagePath = "file://" + photos.get(0);
//                        Log.d("d", "Gallery image path : " + mSelectedGalleryImagePath);
                        Picasso.get().load(fileUserProfile).resize(200, 200).into(civUserProfilePic);
//                        mGalleryImagePath = photos.get(0);
//                        Log.d("d", "Gallery image to be uploaded: " + mGalleryImagePath);
//                        file = file;
                        Log.d("d", "Gallery Image File::" + fileUserProfile);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case REQUEST_CODE_PICK_CONTACTS:

                    Log.d(TAG, "Response: " + data.toString());
                    uriContact = data.getData();
                    getContactNumber(data, context);

                    break;
            }
        }
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public void getContactNumber(Intent data, final Context context) {
        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";
        etRegMob.setText("");

        List<String> allNumbers = new ArrayList<>();
        int contactIdColumnId, phoneColumnID, nameColumnID;
        try {
            Uri result = data.getData();
//            Log.e(TAG, result.toString());
            String id = result.getLastPathSegment();
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    String idContactBook = cursor.getString(contactIdColumnId);
                    String displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);
                    Log.e("Phone", phoneNumber);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                        primaryMobile = phoneNumber;
                    allNumbers.add(phoneNumber);
                    allNumbers.size();
                    cursor.moveToNext();

                }
            } else {
                // no results actions
            }
        } catch (Exception e) {
            // error actions
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (allNumbers.size() > 1 && !allNumbers.isEmpty()) {

                final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Choose a number");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        selectedNumber = items[item].toString().replaceAll("[^0-9\\+]", "");

                        Log.d("numberOnClick", selectedNumber);

                        if (selectedNumber != null && (new Emailvalidation()).phone_validation(selectedNumber)) {
                            etRegMob.setText(selectedNumber);
                            etRegMob.requestFocus();

                        } else {
                            new CustomToast(RegistrationActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                        }
                    }
                });
                AlertDialog alert = builder.create();

                alert.show();

            } else {

                selectedNumber = phoneNumber.replaceAll("[^0-9\\+]", "");

                Log.d("number", selectedNumber);

                if (selectedNumber != null && (new Emailvalidation()).phone_validation(selectedNumber)) {
                    etRegMob.setText(selectedNumber);
                    etRegMob.requestFocus();
                } else {
                    new CustomToast(RegistrationActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                }
            }
        }
    }

    public static String getMeMyNumber(String number, String countryCode) {
        String out = number.replaceAll("[^0-9\\+]", "")        //remove all the non numbers (brackets dashes spaces etc.) except the + signs
                .replaceAll("(^[1-9].+)", countryCode + "$1")         //if the number is starting with no zero and +, its a local number. prepend cc
                .replaceAll("(.)(\\++)(.)", "$1$3")         //if there are left out +'s in the middle by mistake, remove them
                .replaceAll("(^0{2}|^\\+)(.+)", "$2")       //make 00XXX... numbers and +XXXXX.. numbers into XXXX...
                .replaceAll("^0([1-9])", countryCode + "$1");         //make 0XXXXXXX numbers into CCXXXXXXXX numbers
        return out;
    }

 /*   public void DoBDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("d", "monthOfYear:" + monthOfYear);
                etRegDOB.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                Log.d("d", "String Dob::" + etRegDOB.getText().toString().trim());
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(context, listener, year, month, day);
        dpDialog.getDatePicker().setMaxDate(mcalendar.getTimeInMillis());
        dpDialog.show();
    }*/


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(RegistrationActivity.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        Intent loginIntent = new Intent(RegistrationActivity.this, LoginStepOneActivity.class);
        startActivity(loginIntent);
        finish();

    }

    @Override
    public void onButtonClick(String strButtonText) {

//        boolean result = CommonUtility.checkPermission(context);

        if (strButtonText.equals(RegistrationActivity.this.getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    takePhoto();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }

        } else if (strButtonText.equals(RegistrationActivity.this.getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    mCallPhotoGallary();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();

//            }
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) RegistrationActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

}

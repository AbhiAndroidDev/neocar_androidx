package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.neo.cars.app.BookACarActivity;
import com.neo.cars.app.BookNowActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleListModel;
import com.neo.cars.app.Utils.StaticClass;

import java.util.ArrayList;

public class SearchVehicleListAdapter extends RecyclerView.Adapter<SearchVehicleListAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<VehicleListModel> arr_listOfVehicle;
    private int transitionflag = StaticClass.transitionflagNext;
    private String start_date, duration, pickup_time;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_vehicle_name, tv_vehicle_type, tv_vehicle_cost, tv_passenger, tv_luggage;
        ImageView iv_vehicle_image, iv_ic_lux_;
        Button btn_book_now;
        RatingBar ratingBar;
        LinearLayout ll_vehicle_row;

        public MyViewHolder(View view) {
            super(view);
            tv_vehicle_name = view.findViewById(R.id.tv_vehicle_name);
            tv_vehicle_type = view.findViewById(R.id.tv_vehicle_type);
            tv_vehicle_cost = view.findViewById(R.id.tv_vehicle_cost);
            tv_passenger = view.findViewById(R.id.tv_passenger);
            tv_luggage = view.findViewById(R.id.tv_luggage);
            iv_vehicle_image = view.findViewById(R.id.iv_vehicle_image);
            iv_ic_lux_ = view.findViewById(R.id.iv_ic_lux_);
            btn_book_now = view.findViewById(R.id.btn_book_now);
            ratingBar = view.findViewById(R.id.ratingBar);

            ll_vehicle_row = view.findViewById(R.id.ll_vehicle_row);
        }
    }

    public SearchVehicleListAdapter(Context mContext, ArrayList<VehicleListModel> arr_listOfVehicle,
                                    String _start_date, String _duration, String _pickup_time) {
        this.mContext = mContext;
        this.arr_listOfVehicle = arr_listOfVehicle;
        this.start_date = _start_date;
        this.duration = _duration;
        this.pickup_time = _pickup_time;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.tv_vehicle_name.setText(arr_listOfVehicle.get(position).getVehicle_make_name()
                +" "+arr_listOfVehicle.get(position).getVehicle_model_name());
        holder.tv_vehicle_type.setText(arr_listOfVehicle.get(position).getVehicle_type_name());
        holder.tv_vehicle_cost.setText("₹"+arr_listOfVehicle.get(position).getTotal_trip_cost());

        holder.tv_passenger.setText(" x "+arr_listOfVehicle.get(position).getMax_passenger());
        holder.tv_passenger.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_passenger, 0, 0, 0);
//        holder.tv_passenger.setPadding(0, 5, 0, 0);
        holder.tv_luggage.setText(" x "+arr_listOfVehicle.get(position).getMax_luggage());
        holder.tv_luggage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_luggage, 0, 0, 0);
//        holder.tv_luggage.setPadding(0, 5, 0, 0);

        holder.ratingBar.setIsIndicator(true);

        holder.ratingBar.setRating(Float.parseFloat(arr_listOfVehicle.get(position).getAvg_vehicle_rating()));

        String vehicleType = arr_listOfVehicle.get(position).getVehicle_type_name();

        if(arr_listOfVehicle.get(position).getIs_luxury().equalsIgnoreCase("Y")){
            holder.iv_ic_lux_.setVisibility(View.VISIBLE);
        }else{
            holder.iv_ic_lux_.setVisibility(View.GONE);
        }

        holder.btn_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                transitionflag = StaticClass.transitionflagNext;
                Intent booknowintent = new Intent(mContext, BookNowActivity.class);
                booknowintent.putExtra("vehicleId", arr_listOfVehicle.get(position).getVehicle_id());
//                booknowintent.putExtra("driverId", strDriverId);
                booknowintent.putExtra("CityName", "Kolkata");
                booknowintent.putExtra("FromDate", start_date);
                booknowintent.putExtra("Duration", duration);
                booknowintent.putExtra("PickUpTime", pickup_time);
                booknowintent.putExtra("driverId", "");

//                                Log.d("d", "**Book now vehicle id send****"+strVehicleId);
//                                Log.d("d", "**Book now driver id send****"+strDriverId);
//                                Log.d("d", "**Book now location city send****"+strLocation);
                mContext.startActivity(booknowintent);
            }
        });


        holder.ll_vehicle_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bookacarintent = new Intent(mContext, BookACarActivity.class);
                bookacarintent.putExtra("VehicleId", arr_listOfVehicle.get(position).getVehicle_id());
                Log.d("d", "Vehicle id:: " + arr_listOfVehicle.get(position).getVehicle_id());
                bookacarintent.putExtra("IsComingFrom", StaticClass.ExploreCar);
//
                bookacarintent.putExtra("FromDate",  start_date);
                Log.d("strStartDate*", start_date);
                bookacarintent.putExtra("Duration", duration );
                Log.d("strDuration*", duration);
                bookacarintent.putExtra("PickUpTime", pickup_time );
                Log.d("strPickUpTime*", pickup_time);

                mContext.startActivity(bookacarintent);
            }
        });


        Glide.with(mContext)
                .load(arr_listOfVehicle.get(position).getImage_file())
                .into(holder.iv_vehicle_image);
    }

    @Override
    public int getItemCount() {
        return arr_listOfVehicle.size();
    }
}

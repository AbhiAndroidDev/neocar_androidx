package com.neo.cars.app.SetGet;

import java.util.ArrayList;

/**
 * Created by joydeep on 7/5/18.
 */

public class BookingInformationModel {



    String booking_id;
    String user_id;
    String vehicle_id;
    String vehicle_type_name;
    String driver_id;
    String booking_date;
    String start_time;
    String end_time;
    String total_passenger;
    String duration;
    String pickup_location;
    String drop_location;
    String total_amount;
    String addl_request;
    String addl_info;
    String expected_route;
    String booking_purpose;
    String customer_sex;
    String customer_dob;
    String customer_address;
    String customer_name;
    String customer_mail;
    String customer_contact;
    String cost;
    String cancel_reason;
    String cancel_by;
    String cancel_charge;
    String refundable_amount;
    String booking_status;
    String vehicle_city;
    String toll_parking_charge;

    public String getToll_parking_charge() {
        return toll_parking_charge;
    }

    public void setToll_parking_charge(String toll_parking_charge) {
        this.toll_parking_charge = toll_parking_charge;
    }

    public String getVehicle_type_name() {
        return vehicle_type_name;
    }

    public void setVehicle_type_name(String vehicle_type_name) {
        this.vehicle_type_name = vehicle_type_name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }



    ArrayList<BookingInfo_PassengerListModel> PassengerList;

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getTotal_passenger() {
        return total_passenger;
    }

    public void setTotal_passenger(String total_passenger) {
        this.total_passenger = total_passenger;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getDrop_location() {
        return drop_location;
    }

    public void setDrop_location(String drop_location) {
        this.drop_location = drop_location;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getAddl_request() {
        return addl_request;
    }

    public void setAddl_request(String addl_request) {
        this.addl_request = addl_request;
    }

    public String getAddl_info() {
        return addl_info;
    }

    public void setAddl_info(String addl_info) {
        this.addl_info = addl_info;
    }

    public String getExpected_route() {
        return expected_route;
    }

    public void setExpected_route(String expected_route) {
        this.expected_route = expected_route;
    }

    public String getBooking_purpose() {
        return booking_purpose;
    }

    public void setBooking_purpose(String booking_purpose) {
        this.booking_purpose = booking_purpose;
    }

    public String getCustomer_sex() {
        return customer_sex;
    }

    public void setCustomer_sex(String customer_sex) {
        this.customer_sex = customer_sex;
    }

    public String getCustomer_dob() {
        return customer_dob;
    }

    public void setCustomer_dob(String customer_dob) {
        this.customer_dob = customer_dob;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_mail() {
        return customer_mail;
    }

    public void setCustomer_mail(String customer_mail) {
        this.customer_mail = customer_mail;
    }

    public String getCustomer_contact() {
        return customer_contact;
    }

    public void setCustomer_contact(String customer_contact) {
        this.customer_contact = customer_contact;
    }

    public ArrayList<BookingInfo_PassengerListModel> getPassengerList() {
        return PassengerList;
    }

    public void setPassengerList(ArrayList<BookingInfo_PassengerListModel> passengerList) {
        PassengerList = passengerList;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public void setCancel_reason(String cancel_reason) {
        this.cancel_reason = cancel_reason;
    }

    public String getCancel_by() {
        return cancel_by;
    }

    public void setCancel_by(String cancel_by) {
        this.cancel_by = cancel_by;
    }

    public String getCancel_charge() {
        return cancel_charge;
    }

    public void setCancel_charge(String cancel_charge) {
        this.cancel_charge = cancel_charge;
    }

    public String getRefundable_amount() {
        return refundable_amount;
    }

    public void setRefundable_amount(String refundable_amount) {
        this.refundable_amount = refundable_amount;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }


    public String getVehicle_city() {
        return vehicle_city;
    }

    public void setVehicle_city(String vehicle_city) {
        this.vehicle_city = vehicle_city;
    }
}

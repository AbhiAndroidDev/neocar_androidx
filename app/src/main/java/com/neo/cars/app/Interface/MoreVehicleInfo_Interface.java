package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;

/**
 * Created by parna on 23/4/18.
 */

public interface MoreVehicleInfo_Interface {

     void MoreVehicleInfo(VehicleTypeModel vehicleTypeModel);
}

package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.CompanyAddVehicleActivity;
import com.neo.cars.app.Interface.OvertimeAvailability_Interface;
import com.neo.cars.app.OvertimeBookingActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.RequestOvertimeActivity;
import com.neo.cars.app.RequestOvertimeCheckActivity;
import com.neo.cars.app.SetGet.OvertimeAvailabilityModel;
import com.neo.cars.app.SetGet.OvertimeAvailabilityVehicleModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 11/5/18.
 */

public class OvertimeAvailability_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private ArrayList<OvertimeAvailabilityModel> overtimeAvailabilityList;
    private OvertimeAvailabilityModel overtimeAvailabilityModel;

    private String bookingid="",bookinhhours="",bookinghoursvalue="", strMaxPassengers = "";

    public void overtimeAvailability (Activity context, final String strBookingId, final String strSelectedBookingHourValue,String strSelectedBookingHourLabel){
        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        bookingid=strBookingId;
        bookinhhours=strSelectedBookingHourValue;
        bookinghoursvalue=strSelectedBookingHourLabel;

        UserLoginDetails=new UserLoginDetailsModel();

        gson = new Gson();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest overtimeAvailabilityStringRequest = new StringRequest(Request.Method.POST, Urlstring.overtime_availability,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response Booking hours", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("booking_id", strBookingId);
                params.put("duration", strSelectedBookingHourValue);
                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("overtime availability params******getParams***"+params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        overtimeAvailabilityStringRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(overtimeAvailabilityStringRequest);

    }


    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        overtimeAvailabilityList = new ArrayList<OvertimeAvailabilityModel>();
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("overtime_avalibility").optString("user_deleted");
            Msg = jobj_main.optJSONObject("overtime_avalibility").optString("message");
            Status= jobj_main.optJSONObject("overtime_avalibility").optString("status");

            details=jobj_main.optJSONObject("overtime_avalibility").optJSONObject("details");

            JSONArray alternate_vehicle_listArray = details.optJSONArray("alternate_vehicle_list");

            JSONObject jsonObject = new JSONObject();

            if (Status.equals("0") && alternate_vehicle_listArray != null) {

                for (int i = 0; i < alternate_vehicle_listArray.length(); i++) {
                    jsonObject = alternate_vehicle_listArray.optJSONObject(i);
                    overtimeAvailabilityModel = new OvertimeAvailabilityModel();

                    overtimeAvailabilityModel.setState_id(jsonObject.optString("state_id"));
                    overtimeAvailabilityModel.setState_name(jsonObject.optString("state_name"));
                    overtimeAvailabilityModel.setCity_id(jsonObject.optString("city_id"));
                    overtimeAvailabilityModel.setCity_name(jsonObject.optString("city_name"));
                    overtimeAvailabilityModel.setHas_more_vehicle(jsonObject.optString("has_more_vehicle"));

                    JSONArray jsonArrayVehicle = jsonObject.optJSONArray("vehicle");
                    JSONObject jsonObjectVehicle = new JSONObject();

                    ArrayList<OvertimeAvailabilityVehicleModel> OvertimeVehicleList = new ArrayList<OvertimeAvailabilityVehicleModel>();
                    for (int j = 0; j < jsonArrayVehicle.length(); j++) {
                        jsonObjectVehicle = jsonArrayVehicle.optJSONObject(j);
                        OvertimeAvailabilityVehicleModel availabilityVehicleModel = new OvertimeAvailabilityVehicleModel();
                        availabilityVehicleModel.setId(jsonObjectVehicle.optString("id"));
                        availabilityVehicleModel.setVehicle_type_id(jsonObjectVehicle.optString("vehicle_type_id"));
                        availabilityVehicleModel.setMake(jsonObjectVehicle.optString("make"));
                        availabilityVehicleModel.setModel(jsonObjectVehicle.optString("model"));
                        availabilityVehicleModel.setYear(jsonObjectVehicle.optString("year"));
                        availabilityVehicleModel.setKm_travelled(jsonObjectVehicle.optString("km_travelled"));
                        availabilityVehicleModel.setAc_available(jsonObjectVehicle.optString("ac_available"));
                        availabilityVehicleModel.setHourly_rate(jsonObjectVehicle.optString("hourly_rate"));
                        availabilityVehicleModel.setOvertime_available(jsonObjectVehicle.optString("overtime_available"));
                        availabilityVehicleModel.setHourly_overtime_rate(jsonObjectVehicle.optString("hourly_overtime_rate"));
                        availabilityVehicleModel.setMax_luggage(jsonObjectVehicle.optString("max_luggage"));
                        availabilityVehicleModel.setMax_passenger(jsonObjectVehicle.optString("max_passenger"));
                        availabilityVehicleModel.setMax_addl_passenger(jsonObjectVehicle.optString("max_addl_passenger"));
                        strMaxPassengers = jsonObjectVehicle.optString("max_addl_passenger");
                        availabilityVehicleModel.setDescription(jsonObjectVehicle.optString("description"));
                        availabilityVehicleModel.setVehicle_image(jsonObjectVehicle.optString("vehicle_image"));

                        OvertimeVehicleList.add(availabilityVehicleModel);
                    }

                    overtimeAvailabilityModel.setArrListOvertimeVehicle(OvertimeVehicleList);
                    overtimeAvailabilityList.add(overtimeAvailabilityModel);
                }
            }

            if (Status.equals(StaticClass.SuccessResult)){

                strMaxPassengers = details.optString("max_addl_passenger");

            }else if (Status.equals(StaticClass.ErrorResult)){

                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
//                    new CustomToast(mcontext, Msg);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {

            if (Status.equals(StaticClass.SuccessResult)) {

                final Dialog customdialog = new Dialog(mcontext);
                customdialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                customdialog.setContentView(R.layout.dialog_overtime);

                CustomTitilliumTextViewSemiBold tvMsg = customdialog.findViewById(R.id.tvReqOvertimeText);
                CustomButtonTitilliumSemibold btnOvertimeGo = customdialog.findViewById(R.id.btnOvertimeGo);

                tvMsg.setText(Msg);

                btnOvertimeGo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customdialog.dismiss();

                        Intent checkintent = new Intent(mcontext, OvertimeBookingActivity.class);
                        checkintent.putExtra("Duration", bookinhhours);
                        checkintent.putExtra("DurationLabel", bookinghoursvalue);
                        checkintent.putExtra("BookingId", bookingid);
                        checkintent.putExtra("strMaxPassengers", strMaxPassengers);
                        mcontext.startActivity(checkintent);
                        mcontext.finish();

                    }
                });
                customdialog.show();


            } else if (Status.equals(StaticClass.ErrorResult)){

                String overtimeAvailable = gson.toJson(overtimeAvailabilityModel);
                Intent intent = new Intent(mcontext, RequestOvertimeCheckActivity.class);
                intent.putExtra("overtimeAvailableModel", overtimeAvailable);
                intent.putExtra("msgOvertime", Msg);
                mcontext.startActivity(intent);

            }
        }
    }
}

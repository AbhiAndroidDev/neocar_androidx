package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.neo.cars.app.Adapter.PayoutInfoAdapter;
import com.neo.cars.app.Interface.PaymentInfo_Interface;
import com.neo.cars.app.SetGet.PaymentInfoModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.CompanyPayoutInfo_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;
import java.util.List;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_FLING;
import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;


/**
 * Created by Mithilesh on 20/2/19.
 */


public class CompanyPayoutInfoActivity extends AppCompatActivity implements PaymentInfo_Interface {

    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private RelativeLayout rlBackLayout;
    private RecyclerView rcv_PaymentInformation;
    private LinearLayoutManager layoutManagerVertical;
    private PayoutInfoAdapter payoutinfoAdapter;
    private List<PaymentInfoModel> myPayoutInfoList = new ArrayList<>();
    private BottomViewCompany bottomview = new BottomViewCompany();
    private CustomTitilliumTextViewSemiBold tvNoPayoutFound;
    private int pageCount=0, intOffset=12;
    private String strPageCount="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_payout_info);

        new AnalyticsClass(CompanyPayoutInfoActivity.this);

        Initialize();
        Listener();
    }


    private void Initialize() {

        rcv_PaymentInformation = findViewById(R.id.rcv_PaymentInformation);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.PayoutInformation));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        tvNoPayoutFound = findViewById(R.id.tvNoPayoutFound);

        bottomview.BottomViewCompany(CompanyPayoutInfoActivity.this, StaticClass.Menu_profile_company);

    }

    private void Listener() {
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });


        rcv_PaymentInformation.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == SCROLL_STATE_FLING) {

                } else if (newState == SCROLL_STATE_IDLE) {

                    if (myPayoutInfoList.size() < Integer.parseInt(strPageCount)) {

                        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rcv_PaymentInformation.getLayoutManager();
                        if (linearLayoutManager.findLastCompletelyVisibleItemPosition() >= (myPayoutInfoList.size() - 1)) {
                            System.out.println("**********findLastVisibleItemPosition************");
                            pageCount++;
                            dataFetching();
                        }
                    }

                }
            }
        });

    }


    private void dataFetching(){
        if (NetWorkStatus.isNetworkAvailable(CompanyPayoutInfoActivity.this)) {
            new CompanyPayoutInfo_Webservice().PayoutInfo(CompanyPayoutInfoActivity.this, pageCount);
        } else {
            Intent i = new Intent(CompanyPayoutInfoActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyPayoutInfoActivity.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }


    @Override
    protected void onResume() {
        super.onResume();

        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfileCompany){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        pageCount = 0;

        dataFetching();
    }

    @Override
    public void OnPaymentInfo(ArrayList<PaymentInfoModel> arr_PaymentInfo, String total_record) {

        strPageCount = total_record;

        if(arr_PaymentInfo.size() > 0){

            if(pageCount == 0) {

                myPayoutInfoList = arr_PaymentInfo;

                payoutinfoAdapter = new PayoutInfoAdapter(myPayoutInfoList, this);
                layoutManagerVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                rcv_PaymentInformation.setLayoutManager(layoutManagerVertical);
                rcv_PaymentInformation.setItemAnimator(new DefaultItemAnimator());
                rcv_PaymentInformation.setHasFixedSize(true);
                rcv_PaymentInformation.setAdapter(payoutinfoAdapter);

            }else{

                rcv_PaymentInformation.removeAllViews();

                for(int i = 0; i < arr_PaymentInfo.size(); i++) {
                    myPayoutInfoList.add(arr_PaymentInfo.get(i));
                }

                payoutinfoAdapter = new PayoutInfoAdapter(myPayoutInfoList,this);
                layoutManagerVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                rcv_PaymentInformation.setLayoutManager(layoutManagerVertical);
                rcv_PaymentInformation.setItemAnimator(new DefaultItemAnimator());
                rcv_PaymentInformation.setHasFixedSize(true);
                rcv_PaymentInformation.setAdapter(payoutinfoAdapter);

                if (pageCount > 0){
                    rcv_PaymentInformation.smoothScrollToPosition(pageCount*intOffset-1);
                }

            }

        }else{
            rcv_PaymentInformation.setVisibility(View.GONE);
            tvNoPayoutFound.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyPayoutInfoActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

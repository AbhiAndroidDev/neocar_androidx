package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.RatingReviewListModel;

/**
 * Created by parna on 12/9/18.
 */

public interface RatingReviewList_Interface {

    public void onRatingReviewList(RatingReviewListModel ratingReviewList);
}

package com.neo.cars.app.fragment;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.gson.Gson;
import com.neo.cars.app.SetGet.BookingInformationModel;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.VehicleInformationModel;
import com.neo.cars.app.Utils.StaticClass;

/**
 * Created by parna on 15/3/18.
 */

public class BookingInfoPager extends FragmentStatePagerAdapter {


    //integer to count number of tabs
    private int tabCount;
    private String MB_bookingInformationModel;
    private String MB_driverdataModel;
    private String MB_vehicleInformationModel;
    private String strShowDriver;
    private String strShowDriverMsg;

    private Gson gson;
    private String userType = "", bookingType = "";

    public BookingInfoPager(FragmentManager fm, int tabCount, BookingInformationModel bookingInformationModel,
                            DriverDataModel driverdataModel,
                            VehicleInformationModel vehicleInformationModel, String strShowDriver, String strShowDriverMsg, String userType, String bookingType) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        this.strShowDriver = strShowDriver;
        this.strShowDriverMsg = strShowDriverMsg;
        this.userType = userType;
        this.bookingType = bookingType;

        gson = new Gson();
        MB_bookingInformationModel =gson.toJson(bookingInformationModel);
        MB_driverdataModel=gson.toJson(driverdataModel);
        MB_vehicleInformationModel=gson.toJson(vehicleInformationModel);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                //MyInformationFragment is now BookingInformation as title of page
                MyInformationFragment tab1 = new MyInformationFragment();
                Bundle bundletab1 = new Bundle();
                bundletab1.putSerializable("bookingInformation", MB_bookingInformationModel);
                tab1.setArguments(bundletab1);
                return tab1;

            case 1:

                Log.d("userType**", userType);
                if (userType.equalsIgnoreCase(StaticClass.COMPANY) && bookingType.equalsIgnoreCase(StaticClass.MyVehicleUpcoming)){

                    CompanyDriverInformationFragment tab2 = new CompanyDriverInformationFragment();
                    Bundle bundletab2 = new Bundle();
                    bundletab2.putSerializable("driverdata", MB_driverdataModel);
                    bundletab2.putSerializable("bookingInformation", MB_bookingInformationModel);
                    tab2.setArguments(bundletab2);
                    return tab2;

                }else {
                    DriverInformationFragment tab2 = new DriverInformationFragment();
                    Bundle bundletab2 = new Bundle();
                    bundletab2.putSerializable("driverdata", MB_driverdataModel);
                    bundletab2.putString("strShowDriver", strShowDriver);
                    tab2.setArguments(bundletab2);
                    return tab2;
                }
            case 2:
                VehicleInformationFragment tab3 = new VehicleInformationFragment();
                Bundle bundletab3 = new Bundle();
                bundletab3.putSerializable("vehicleInformation", MB_vehicleInformationModel);
                tab3.setArguments(bundletab3);
                return tab3;

            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}

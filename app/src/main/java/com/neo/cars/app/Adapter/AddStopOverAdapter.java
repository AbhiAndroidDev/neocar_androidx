package com.neo.cars.app.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.neo.cars.app.R;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.StaticClass;

import java.util.ArrayList;


public class AddStopOverAdapter {
    @SuppressLint("InflateParams")

    private ArrayList<String> myLocation = new ArrayList<String>();

    private LayoutInflater inflater;
    private Activity context;
    private LinearLayout addstopover_listview;
    private boolean checkFiledFlag=true;

    public AddStopOverAdapter(Activity context, LinearLayout addstopoverlistview) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        addstopover_listview=addstopoverlistview;

        myLocation= StaticClass.getAl_Location();

        if(myLocation.size()==0){
            myLocation.add("");

            StaticClass.setAl_Location(myLocation);
        }else{
            for(int i=0;i<myLocation.size();i++){
                if(myLocation.get(i).equals("")){
                    checkFiledFlag=false;
                }
            }

            if(checkFiledFlag){
                myLocation.add("");
                StaticClass.setAl_Location(myLocation);
            }else{
                new CustomToast(context, MessageText.Enter_all_added_field);
            }
        }

        addstopover_listview.removeAllViews();


        Log.d("d", "my location size::"+myLocation.size());
        for(int i=0;i<myLocation.size();i++){
            if(myLocation.size()>1) {
                if (i > 0)
                    addstopover_listview.addView(addView2(i));
            }else{
                addstopover_listview.addView(addView2(i));
            }
        }
    }

    public View addView2(final int position) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.addmore_inflate, null);

        RelativeLayout delete_item_relative = v.findViewById(R.id.delete_item_relative);
        EditText et_tvpickuploc_inflate = v.findViewById(R.id.tvpickuploc_inflate);

        et_tvpickuploc_inflate.setText(myLocation.get(position));
        et_tvpickuploc_inflate.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                myLocation.set(position, s.toString());
            }
        });


        delete_item_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    myLocation.remove(position);
                    SetView();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return v;
    }

    private void SetView(){

        addstopover_listview.removeAllViews();
        Log.d("d", "my location size::"+myLocation.size());
        for(int i=0;i<myLocation.size();i++){
            if (i > 0)
            addstopover_listview.addView(addView2(i));
        }
        StaticClass.setAl_Location(myLocation);
    }
}
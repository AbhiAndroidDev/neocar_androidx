package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.neo.cars.app.Adapter.SearchVehicleListAdapter;
import com.neo.cars.app.Interface.VehicleListInterface;
import com.neo.cars.app.SetGet.VehicleDetailsModel;
import com.neo.cars.app.SetGet.VehicleListModel;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.SearchedVehicleListApi;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_FLING;
import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

public class SearchResultActivity extends AppCompatActivity implements VehicleListInterface {

    private Context context;
    private Toolbar toolbar;
    private ConnectionDetector cd;
    private RelativeLayout rlBackLayout, rel_filter;
    private RatingBar ratingBar;

    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private int transitionflag = StaticClass.transitionflagNext;
    private RelativeLayout rlAddLayout, rel_sort;
    private TextView tv_edit_search, tvPickuptime, tvDate, tvDuration, tvNoExploreCarFound;
    private ImageView iv_ic_sort, iv_ic_filter;
    private String start_date_to_send="", pickup_time_to_send = "";

    private String device_id = "", state_id = "", city_id = "", user_id = "", start_date = "", duration = "",
            pickup_time = "", trip_cost_range = "", trip_cost_range_position = "-1", vehicle_type = "",
            vehicle_type_position = "-1", vehicle_brand = "", vehicle_brand_position = "-1", passenger_capacity = "", passenger_capacity_position = "-1",
            luggage_capacity = "", luggage_capacity_position = "-1", overtime_available = "", sort_by = "FEATURED", search_flag = "Y", strPageCount = "0",
            is_luxury_status = "";

    private LinearLayout layoutBottomSheet;
    private BottomSheetBehavior mBottomSheetBehavior;
    private RecyclerView rcv_sort_list;
    private NestedScrollView mNestedScrollView;
    private LinearLayoutManager layoutManager;
    private ArrayList<String> sortOptionList;
    private int selectedPosition = 0;

    private RecyclerView rcvExploreCar;
    private SearchVehicleListAdapter vehicleListAdapter;
    private LinearLayoutManager layoutManagerVertical;

    private int pageCount=0, intOffset=5;
    private ArrayList<VehicleDetailsModel> arrlistExplore;
    private ArrayList<VehicleListModel> arr_ExploreVehicleListModel=new ArrayList<>();
    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        initialize();
        listener();
        getData();

        getVehicleList();

    }

    private void getData() {

        Intent intent = getIntent();
        if (intent != null) {
            start_date = intent.getStringExtra("FromDate");
            pickup_time = intent.getStringExtra("PickUpTime");
            duration = intent.getStringExtra("Duration");
            city_id = intent.getStringExtra("strCityId");
            state_id = intent.getStringExtra("strStateId");

            start_date_to_send = convertDateFormat(start_date);
            pickup_time_to_send = convertTimeFormat(pickup_time);

            tvDate.setText(start_date);
            tvDuration.setText(duration + " HOURS");

            if (!TextUtils.isEmpty(pickup_time)){
//                convert12(strPickUpTime);
                tvPickuptime.setText(pickup_time);
            }
        }

        device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);


    }

    private String convertTimeFormat(String pickup_time) {

        String timeTOSend = "";
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
            final Date dateObj = sdf.parse(pickup_time);
            timeTOSend = new SimpleDateFormat("HH:mm", Locale.US).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return timeTOSend;
    }

    private String convertDateFormat(String startDate) {
        String dateTOSend = "";
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
            final Date dateObj = sdf.parse(startDate);
            dateTOSend = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return dateTOSend;
    }

    private void initialize() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText("BOOK NEOCARS");

        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlAddLayout = findViewById(R.id.rlAddLayout);
        rel_sort = findViewById(R.id.rel_sort);
        rel_filter = findViewById(R.id.rel_filter);
        rlAddLayout.setVisibility(View.VISIBLE);
        tv_edit_search = findViewById(R.id.tv_edit_search);
        tvPickuptime = findViewById(R.id.tvPickuptime);
        tvDate = findViewById(R.id.tvDate);
        tvDuration = findViewById(R.id.tvDuration);
        rcvExploreCar = findViewById(R.id.rcvExploreCar);
        tvNoExploreCarFound = findViewById(R.id.tvNoExploreCarFound);
        iv_ic_sort = findViewById(R.id.iv_ic_sort);
        iv_ic_filter = findViewById(R.id.iv_ic_filter);

        tv_edit_search.setVisibility(View.VISIBLE);

        sortOptionList = new ArrayList<>();
        sortOptionList.add("FEATURED");
        sortOptionList.add("PRICE HIGH TO LOW");
        sortOptionList.add("PRICE LOW TO HIGH");
        sortOptionList.add("NEWER CAR FIRST");
        sortOptionList.add("OLDER CAR FIRST");
        sortOptionList.add("RATING HIGH TO LOW");
        sortOptionList.add("RATING LOW TO HIGH");

    }

    private void listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(SearchResultActivity.this,
                        SearchResultActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        SearchResultActivity.this.getResources().getString(R.string.yes),
                        SearchResultActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        tv_edit_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rel_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setBottomSheetSort();
            }
        });


        rel_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent applyintent = new Intent(SearchResultActivity.this, FilterNewActivity.class);
                applyintent.putExtra("FromDate", start_date );
                applyintent.putExtra("Duration", duration );
                applyintent.putExtra("PickUpTime", pickup_time );
                applyintent.putExtra("trip_cost_range", trip_cost_range );
                applyintent.putExtra("trip_cost_range_position", trip_cost_range_position );
                applyintent.putExtra("vehicle_type", vehicle_type );
                applyintent.putExtra("vehicle_type_position", vehicle_type_position );
                applyintent.putExtra("vehicle_brand", vehicle_brand );
                applyintent.putExtra("vehicle_brand_position", vehicle_brand_position );
                applyintent.putExtra("is_luxury_status", is_luxury_status );
                applyintent.putExtra("passenger_capacity", passenger_capacity );
                applyintent.putExtra("passenger_capacity_position", passenger_capacity_position );
                applyintent.putExtra("luggage_capacity", luggage_capacity );
                applyintent.putExtra("luggage_capacity_position", luggage_capacity_position );
                applyintent.putExtra("overtime_available", overtime_available );

                startActivityForResult(applyintent, StaticClass.SearchFilterRequestCode);


            }
        });


        rcvExploreCar.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == SCROLL_STATE_FLING) {

                } else if (newState == SCROLL_STATE_IDLE) {
//                    System.out.println("**********onScrolled************");
//                    System.out.println("**********strCityId************" + strCityId);
//                    System.out.println("**********strPageCount************" + strPageCount);

                    if(TextUtils.isEmpty(city_id)) {
                        if (arrlistExplore.size() < Integer.parseInt(strPageCount)) {
                            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rcvExploreCar.getLayoutManager();
                            if (linearLayoutManager.findLastCompletelyVisibleItemPosition() >= (arrlistExplore.size() - 1)) {
                                System.out.println("**********findLastVisibleItemPosition************");
                                pageCount++;
                                search_flag = "N";
                                getVehicleList();
                            }
                        }
                    }else {

                        Log.d("arrListExploreModelSize", ""+arr_ExploreVehicleListModel.size());
                        Log.d("strPageCount", strPageCount);

                        if (arr_ExploreVehicleListModel.size() < Integer.parseInt(strPageCount)) {
                            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rcvExploreCar.getLayoutManager();
                            if (linearLayoutManager.findLastCompletelyVisibleItemPosition() >= (arr_ExploreVehicleListModel.size() - 1)) {
                                System.out.println("**********findLastVisibleItemPosition************");
                                pageCount++;
                                search_flag = "N";
                                getVehicleList();
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("d", "requestCode::  "+requestCode);
        Log.d("d", "resultCode::  "+resultCode);
        pageCount = 0;
        if (resultCode == StaticClass.SearchFilterRequestCode) {
            if(data!=null) {

                if(StaticClass.resetClicked){
                    iv_ic_filter.setBackgroundResource(R.drawable.ic_filter_grey);
                    StaticClass.resetClicked = false;
                }else {
                    iv_ic_filter.setBackgroundResource(R.drawable.ic_filter_yellow);
                }
                trip_cost_range = Objects.requireNonNull(data.getExtras()).getString("trip_cost_range");
                trip_cost_range_position = data.getExtras().getString("trip_cost_range_position");
                is_luxury_status = data.getExtras().getString("is_luxury_status");
                vehicle_type = data.getExtras().getString("vehicle_type");
                vehicle_type_position = data.getExtras().getString("vehicle_type_position");
                vehicle_brand = data.getExtras().getString("vehicle_brand");
                vehicle_brand_position = data.getExtras().getString("vehicle_brand_position");
                passenger_capacity = data.getExtras().getString("passenger_capacity");
                passenger_capacity_position = data.getExtras().getString("passenger_capacity_position");
                luggage_capacity = data.getExtras().getString("luggage_capacity");
                luggage_capacity_position = data.getExtras().getString("luggage_capacity_position");
                overtime_available = data.getExtras().getString("overtime_available");

                search_flag = "N";

                getVehicleList();
            }
        }
    }

    //setting BottomSheet
    private void setBottomSheetSort() {
        View view = LayoutInflater.from(this).inflate(R.layout.bottomsheet_sort_list, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        dialog.setContentView(view);

        LinearLayout llSort = view.findViewById(R.id.ll_sort_option);
        llSort.removeAllViews();

        for(int i=0;i<sortOptionList.size();i++){
            llSort.addView(addViewSort(i, dialog));
        }
        dialog.show();
    }

    public View addViewSort(final int position, final BottomSheetDialog dialog) {

        View v;
        v = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_sort, null);

        final TextView tvCost = v.findViewById(R.id.ctv_sort_option);

        tvCost.setText(sortOptionList.get(position));

        tvCost.setGravity(Gravity.START);


        if(selectedPosition == position){
            tvCost.setBackgroundResource(R.drawable.base_1);
            tvCost.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_select,0);
            tvCost.setGravity(Gravity.CENTER_VERTICAL);

            sort_by = sortOptionList.get(position);
            pageCount = 0;
        }

        tvCost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tvCost.setBackgroundResource(R.drawable.base_1);
                tvCost.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_select,0);
                tvCost.setGravity(Gravity.CENTER_VERTICAL);
                selectedPosition = position;

                if(position == 0){
                    sort_by = "FEATURED";
                }else if(position == 1){
                    sort_by = "PRICE_HIGH_TO_LOW";
                }else if(position == 2){
                    sort_by = "PRICE_LOW_TO_HIGH";
                }else if(position == 3){
                    sort_by = "NEWER_CAR";
                }else  if(position == 4){
                    sort_by = "OLDER_CAR";
                }else if(position == 5){
                    sort_by = "RATING_HIGH_TO_LOW";
                }else if(position == 6){
                    sort_by = "RATING_LOW_TO_HIGH";
                }
//                sort_by = sortOptionList.get(position);
                pageCount = 0;
                search_flag = "N";
                if(!sort_by.equals("FEATURED")){
                    iv_ic_sort.setBackgroundResource(R.drawable.ic_sort_yellow);
                }else{
                    iv_ic_sort.setBackgroundResource(R.drawable.ic_sort_grey);
                }
                getVehicleList();

                dialog.dismiss();
            }
        });

        return v;
    }


    private void getVehicleList(){
        //  rcvExploreCar.setVisibility(View.GONE);
        if (NetWorkStatus.isNetworkAvailable(SearchResultActivity.this)) {
//            Log.d("d", "** page count**"+pageCount);


            new SearchedVehicleListApi().SearchedVehicleList(SearchResultActivity.this, device_id, state_id, city_id, "",
                    start_date_to_send, duration, pickup_time_to_send, trip_cost_range, vehicle_type, vehicle_brand, passenger_capacity,
                    luggage_capacity, overtime_available, sort_by, search_flag, String.valueOf(pageCount), is_luxury_status);

//            new ExploreVehicleList_Webservice().exploreVehicleList(ExploreActivity.this, strMinPrice,
//                    strMaxPrice, strStartDate, strEndDate, strDuration, strVehicleTypeId, strVehicleBrandId, strNoOfPassenger,
//                    strNoOfBaggage, strNeedOvertime, strNeedAc, strCityId, strStateId, strPickUpTime , strModelId, pageCount);

        } else {
            Intent i = new Intent(SearchResultActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(SearchResultActivity.this,
                SearchResultActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                SearchResultActivity.this.getResources().getString(R.string.yes),
                SearchResultActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();

            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    public void VehicleList(String strPageLimit, String strTotalRecord, ArrayList<VehicleDetailsModel> arr_vehicleDetailsModel) {

//        Log.d("d", "ArrlistExplore size::  "+arr_vehicleDetailsModel.size());
        Log.d("d", "strPageLimit::  " + strPageLimit);
        Log.d("d", "strTotalRecord::  " + strTotalRecord);

        if (arr_vehicleDetailsModel != null && !strTotalRecord.equals("0")) {
            if (pageCount == 0) {
                arrlistExplore = new ArrayList<>();
                arrlistExplore = arr_vehicleDetailsModel;
                if (arr_vehicleDetailsModel.size() > 0)
                    arr_ExploreVehicleListModel = new ArrayList<>();
                    arr_ExploreVehicleListModel = arrlistExplore.get(pageCount).getArr_exploreVehicleListModel();

            } else {
                for (int i = 0; i < arr_vehicleDetailsModel.size(); i++) {
                    arrlistExplore.add(arr_vehicleDetailsModel.get(i));
                    arr_ExploreVehicleListModel.addAll(arr_vehicleDetailsModel.get(i).getArr_exploreVehicleListModel());
                }
            }
            strPageCount = strTotalRecord;
            intOffset = Integer.parseInt(strPageLimit);

            if (arr_ExploreVehicleListModel.size() > 0) {

                rcvExploreCar.setVisibility(View.VISIBLE);
                tvNoExploreCarFound.setVisibility(View.GONE);
                rcvExploreCar.removeAllViews();
                vehicleListAdapter = new SearchVehicleListAdapter(this, arr_ExploreVehicleListModel,
                        start_date_to_send, duration, pickup_time_to_send);
                layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                rcvExploreCar.setLayoutManager(layoutManagerVertical);
                rcvExploreCar.setItemAnimator(new DefaultItemAnimator());
                rcvExploreCar.setHasFixedSize(true);
                rcvExploreCar.setAdapter(vehicleListAdapter);

                if (pageCount > 0) {
                    rcvExploreCar.smoothScrollToPosition(pageCount * intOffset - 1);
                }

            } else {
                rcvExploreCar.setVisibility(View.GONE);
                tvNoExploreCarFound.setVisibility(View.VISIBLE);
            }

            vehicleListAdapter.notifyDataSetChanged();

        } else {
            rcvExploreCar.setVisibility(View.GONE);
            tvNoExploreCarFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(StaticClass.BottomExplore){
            StaticClass.BottomExplore = false;
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

        bottomview = new BottomView();
        bottomview.BottomView(SearchResultActivity.this, StaticClass.Menu_Explore);
    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(SearchResultActivity.this, transitionflag);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) SearchResultActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}

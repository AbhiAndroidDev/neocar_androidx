package com.neo.cars.app.VolleyParser;

import android.content.Context;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by joydeep on 3/10/16.
 */
public class ParevalueClass {

    public static String Parevalue(Context context,String mUrl,HashMap<String, String> params) {


        System.out.println("mUrl*************" + mUrl);
        System.out.println("params*************" + params);


        StringBuilder stringBuilder = new StringBuilder(mUrl);
        Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
        int i = 1;
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if(i == 1) {
                stringBuilder.append("?" + entry.getKey() + "=" + entry.getValue());
            } else {
                stringBuilder.append("&" + entry.getKey() + "=" + entry.getValue());
            }
            iterator.remove(); // avoids a ConcurrentModificationException
            i++;
        }

        System.out.println("Final path*************" + stringBuilder.toString());

        return stringBuilder.toString();
    }
}

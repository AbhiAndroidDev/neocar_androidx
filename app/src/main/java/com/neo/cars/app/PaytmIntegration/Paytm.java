package com.neo.cars.app.PaytmIntegration;


import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.Random;


public class Paytm {

    @SerializedName("MID")
    String mId;

    @SerializedName("ORDER_ID")
    String orderId;

    @SerializedName("CUST_ID")
    String custId;

    @SerializedName("CHANNEL_ID")
    String channelId;

    @SerializedName("TXN_AMOUNT")
    String txnAmount;

    @SerializedName("WEBSITE")
    String website;

    @SerializedName("CALLBACK_URL")
    String callBackUrl;

    @SerializedName("INDUSTRY_TYPE_ID")
    String industryTypeId;

    public Paytm(String mId, String channelId, String txnAmount, String website, String callBackUrl, String industryTypeId,String custId) {
        this.mId = mId;
        this.orderId = generateString();
        this.custId = custId;
        this.channelId = channelId;
        this.txnAmount = txnAmount;
        this.website = website;
        this.callBackUrl = callBackUrl;
        this.industryTypeId = industryTypeId;

        Log.d("orderId", orderId);
        Log.d("customerId", custId);
    }

    public String getmId() {
        return mId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getCustId() {
        return custId;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public String getWebsite() {
        return website;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public String getIndustryTypeId() {
        return industryTypeId;
    }

    /*
    * The following method we are using to generate a random string everytime
    * As we need a unique customer id and order id everytime
    * For real scenario you can implement it with your own application logic
    * */
    private String generateString() {
       // String uuid = UUID.randomUUID().toString();
        //return uuid.replaceAll("-", "");
        return randInt(0,999999999);
    }


    public String randInt(int min, int max) {
// Usually this should be a field rather than a method variable so
// that it is not re-seeded every call.
        Random rand = new Random();

// nextInt is normally exclusive of the top value,
// so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return String.valueOf(randomNum);
    }
}

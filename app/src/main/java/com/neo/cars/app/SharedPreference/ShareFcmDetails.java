package com.neo.cars.app.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by joydeep on 17/7/17.
 */

public class ShareFcmDetails {

    private SharedPreferences userPref;
    private SharedPreferences.Editor userEditor;
    private Context context;

    int PRIVATE_MODE = 0;

    private String PREF_NAME = "fcm_details";
    public static String FCM_ID = "fcm_id";

    // Constructor
    public ShareFcmDetails(Context context) {
        this.context = context;
        userPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        userEditor = userPref.edit();
    }

    // Set fcm id
    public void setFcmId(String fcmId) {
        userEditor.putString(FCM_ID, fcmId);
        userEditor.commit();
    }

    // Get fcm id
    public String getFcmId() {
        return userPref.getString(FCM_ID, "");
    }
}

package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.DriverUnAvailabilityDetailsModel;

public interface DriverAddEditAvailability_Interface {

    void driverAddEditUnAvailability(DriverUnAvailabilityDetailsModel driverUnAvailabilityModel, String msg);
}

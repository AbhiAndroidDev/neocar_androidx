package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleTypeModel;

import java.util.ArrayList;

/**
 * Created by parna on 7/5/18.
 */

public interface FilterVehicleType_Interface {

    void VehicleTypeList(ArrayList<VehicleTypeModel> arrlistVehicleType, String max_slider, String min_slider);

}

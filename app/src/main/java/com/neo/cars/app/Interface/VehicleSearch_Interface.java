package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleSearchModel;

import java.util.ArrayList;

/**
 * Created by parna on 23/10/18.
 */

public interface VehicleSearch_Interface {

    public void vehicleSearch(ArrayList<VehicleSearchModel> arrlistVehicle);
}
